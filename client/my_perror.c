/*
** my_perror.c for my_perror in /home/taiebi_e//workdir/zappy/client
**
** Made by eddy taiebi
** Login   <taiebi_e@epitech.net>
**
** Started on  Thu May 23 09:58:51 2013 eddy taiebi
** Last update Thu May 23 10:08:44 2013 eddy taiebi
*/

#define		_GNU_SOURCE

#include	<stdio.h>
#include	<netdb.h>
#include	<string.h>
#include	<errno.h>

int		my_perror(char *str)
{
  if (errno != 0)
    perror(str);
  else
    herror(str);
  return (-1);
}
