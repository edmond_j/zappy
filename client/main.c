/*
** main.c for main in /home/taiebi_e//workdir/zappy
**
** Made by eddy taiebi
** Login   <taiebi_e@epitech.net>
**
** Started on  Tue May 21 11:22:48 2013 eddy taiebi
** Last update Thu May 23 10:11:22 2013 eddy taiebi
*/

#define		_GNU_SOURCE

#include	<netdb.h>
#include	<sys/types.h>
#include	<sys/stat.h>
#include	<fcntl.h>
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<arpa/inet.h>
#include	<unistd.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<sys/select.h>
#include	<string.h>
#include	"socks.h"

int		client(char *addr, int port, char *team)
{
  t_socket	sock;
  t_sockaddr_in	sin;
  t_hostent	*host;

  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
    return (my_perror("socket"));
  if ((host = gethostbyname(addr)) == NULL)
    return (my_perror("gethostbyname"));
  sin.sin_addr = *(t_in_addr *)host->h_addr;
  sin.sin_family = AF_INET;
  sin.sin_port = htons(port);
  if (connect(sock, (t_sockaddr *)&sin, sizeof(sin)) == SOCKET_ERROR)
    return (my_perror("connect"));
  if (dprintf(sock, "%s\n", team) < 0)
    return (dprintf(2, "Write on socket failed.\n"));
  to_do(sock, team);
  return (closesocket(sock));
}

int		get_opt(int ac, char **av)
{
  char		*opt[] = {"-h", "-p", "-n"};
  char		*tab[] = {NULL, NULL, NULL};
  int		i;
  int		j;

  i = 1;
  while (i < ac)
    {
      j = 0;
      while (j < 3)
	{
	  if (strcmp(av[i], opt[j]) == 0 && ac > i)
	    tab[j] = av[i + 1];
	  ++j;
	}
      ++i;
    }
  if (!tab[1] || !tab[2])
    return (dprintf(2, "Usage: ./client -n team -p port [-h hostname]\n"));
  return (client(tab[0] ? tab[0] : "127.0.0.1", atoi(tab[1]), tab[2]));
}

int		main(int ac, char **av)
{
  return (get_opt(ac, av));
}
