/*
** to_do.c for to_do in /home/taiebi_e//workdir/ftp
**
** Made by eddy taiebi
** Login   <taiebi_e@epitech.net>
**
** Started on  Sun Apr 14 20:46:09 2013 eddy taiebi
** Last update Thu May 23 10:20:07 2013 eddy taiebi
*/

#define		_GNU_SOURCE

#include	<errno.h>
#include	<netdb.h>
#include	<sys/types.h>
#include	<sys/stat.h>
#include	<fcntl.h>
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<arpa/inet.h>
#include	<unistd.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<sys/select.h>
#include	<string.h>
#include	"socks.h"

int		to_do_stdin(t_socket sock, char *buffer)
{
  int		ret;

  (void)sock;
  if ((ret = read(0, buffer, 4096)) <= 0)
    return (42);
  buffer[ret - 1] = '\0';
  return (0);
}

int		to_do_read(t_socket sock, char *buffer, fd_set *writefs)
{
  int		ret;

  if ((ret = read(sock, buffer, 4096)) <= 0)
    return (-1);
  buffer[ret] = '\0';
  FD_SET(sock, writefs);
  return (0);
}

int		to_do(t_socket sock, char *team)
{
  int		ret;
  fd_set	readfs;
  fd_set	writefs;
  char		buffer[4096];
  t_timeval	time;

  (void)team;
  time.tv_sec = 0;
  ret = 0;
  while (ret != 42)
    {
      FD_ZERO(&readfs);
      FD_SET(sock, &readfs);
      FD_SET(STDIN_FILENO, &readfs);
      time.tv_usec = 500000;
      if (select(sock + 1, &readfs, &writefs, NULL, &time) < 0)
      	return (my_perror("select"));
      if (FD_ISSET(STDIN_FILENO, &readfs))
	ret = to_do_stdin(sock, buffer);
      else if (FD_ISSET(sock, &readfs))
	if (to_do_read(sock, buffer, &writefs) == -1)
	  return (-1);
    }
  return (0);
}
