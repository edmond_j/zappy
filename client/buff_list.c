/*
** buff_list.c for buff_list in /home/taiebi_e//workdir/zappy/client
**
** Made by eddy taiebi
** Login   <taiebi_e@epitech.net>
**
** Started on  Thu May 23 09:45:29 2013 eddy taiebi
** Last update Thu May 23 09:45:51 2013 eddy taiebi
*/

#include	<stdlib.h>
#include	<string.h>
#include	"buffer.h"

int		put_in_buffer_list(t_buff_list **list, t_buff *buff)
{
  t_buff_list	*elem;
  t_buff_list	*tmp;

  if ((elem = malloc(sizeof(t_buff_list))) == NULL)
    return (-1);
  elem->buff = buff;
  elem->next = NULL;
  tmp = *list;
  if (!tmp)
    *list = elem;
  else
    {
      while (tmp->next)
	tmp = tmp->next;
      tmp->next = elem;
    }
  return (0);
}

int		free_buff_list(t_buff_list *list)
{
  t_buff_list	*elem;

  while (list)
    {
      elem = list;
      list = list->next;
      destroy_buff(elem->buff);
      free(elem);
    }
  return (0);
}
