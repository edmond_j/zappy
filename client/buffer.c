/*
** buffer.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue May 21 10:46:05 2013 julien edmond
** Last update Tue May 21 16:39:16 2013 eddy taiebi
*/

#include	<string.h>
#include	<stdlib.h>
#include	"buffer.h"

t_buff		*new_buff(int size)
{
  t_buff	*elem;

  if ((elem = malloc(sizeof(t_buff)))
      && (elem->buf = malloc(sizeof(char) * size)))
    {
      elem->size = size;
      elem->start = 0;
      elem->end = 0;
    }
  else
    {
      free(elem);
      elem = NULL;
    }
  return (elem);
}

void	destroy_buff(t_buff *buff)
{
  if (buff)
    {
      free(buff->buf);
      free(buff);
    }
}

void	buff_write(t_buff *buff, char *data, int size)
{
  int	i;

  i = -1;
  while (++i < size)
    {
      buff->buf[buff->end] = data[i];
      buff->end = (buff->end + 1) % buff->size;
      if (buff->end == buff->start)
	buff->start = (buff->start + 1) % buff->size;
    }
}

char	*buff_readline(t_buff *buff)
{
  char	*sent;
  int	i;

  sent = 0;
  if (((buff->start <= buff->end
	&& memchr(&buff->buf[buff->start], '\n', buff->end - buff->start))
       || (buff->start > buff->end
	   && (memchr(&buff->buf[buff->start], '\n', buff->size - buff->start)
	       || memchr(&buff->buf, '\n', buff->end))))
      && (sent = malloc(sizeof(char) * (buff->size + 1))))
    {
      i = buff->start;
      while (buff->buf[i] != '\n')
	{
	  sent[i - buff->start] = buff->buf[i];
	  i = (i + 1) % buff->size;
	}
      sent[i - buff->start] = 0;
      buff->start = (i + 1) % buff->size;
    }
  return (sent);
}

int	buff_size(t_buff *buff)
{
  int	size;

  if ((size = buff->end - buff->start) < 0)
    size += buff->size;
  return (size);
}
