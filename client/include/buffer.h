/*
** buffer.h for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue May 21 10:42:37 2013 julien edmond
** Last update Thu May 23 09:44:32 2013 eddy taiebi
*/

#ifndef		BUFFER_H_
# define	BUFFER_H_

# define	BUFF_SIZE		1024

typedef struct		s_buff
{
  int			size;
  int			start;
  int			end;
  char			*buf;
}			t_buff;

typedef struct		s_buff_list
{
  t_buff		*buff;
  struct s_buff_list	*next;
}			t_buff_list;

t_buff		*new_buff(int);
void		destroy_buff(t_buff *);

char		*buff_readline(t_buff *);
void		buff_write(t_buff *, char *, int);

int		buff_size(t_buff *);

int		read_to_buff(t_buff *, int);
int		write_from_buff(t_buff *, int);

#endif		/* !BUFFER_H_ */
