/*
** socks.h for socks in /home/taiebi_e//afs/util/socket
**
** Made by eddy taiebi
** Login   <taiebi_e@epitech.net>
**
** Started on  Mon Jun 11 15:33:20 2012 eddy taiebi
** Last update Thu May 23 10:21:30 2013 eddy taiebi
*/

#ifndef		__SOCKS_H__
# define	__SOCKS_H__

# define	MIN(a, b)	(a < b ? a : b)

# define	closesocket(param)	close(param)
# define	INVALID_SOCKET		-1
# define	SOCKET_ERROR		-1

typedef	int			t_socket;
typedef	struct	sockaddr_in	t_sockaddr_in;
typedef	struct	sockaddr	t_sockaddr;
typedef	struct	in_addr		t_in_addr;
typedef	struct	hostent		t_hostent;
typedef	struct	timeval		t_timeval;

typedef	struct		s_sock
{
  t_socket		sock;
  struct s_sock		*next;
}			t_sock;

int		my_perror(char *);
int		to_do(t_socket, char *);

#endif
