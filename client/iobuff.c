/*
** iobuff.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue May 21 11:41:38 2013 julien edmond
** Last update Tue May 21 15:08:38 2013 julien edmond
*/

#include	<sys/types.h>
#include	<sys/socket.h>
#include	"buffer.h"

int		read_to_buff(t_buff *buff, int fd)
{
  int		size;
  int		ret;

  if ((size = buff->start - buff->end) <= 0)
    size = buff->size - buff->end;
  ret = recv(fd, &buff->buf[buff->end], size, 0);
  buff->end = (buff->end + ret) % buff->size;
  return (ret);
}

int		write_from_buff(t_buff *buff, int fd)
{
  int		size;
  int		ret;

  if ((size = buff->end - buff->start) < 0)
    size = buff->size - buff->start;
  ret = send(fd, &buff->buf[buff->start], size, MSG_NOSIGNAL);
  buff->start = (buff->start + ret) % buff->size;
  return (ret);
}
