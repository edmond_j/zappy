/*
** error_funcs.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d/utils
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Wed Apr 24 16:22:03 2013 david olivier
** Last update Wed Apr 24 18:56:03 2013 david olivier
*/

#include		<stdio.h>
#include		<string.h>
#include		<unistd.h>
#include		"server.h"

int			error(char *str)
{
  write(2, str, strlen(str));
  perror("\nDetail");
  return (-1);
}

int			errorcs(char *str, int socket)
{
  close(socket);
  write(2, str, strlen(str));
  perror("\nDetail");
  return (-1);
}

int			errorf(char *str, FILE *file)
{
  fclose(file);
  write(2, str, strlen(str));
  perror("\nDetail");
  return (-1);
}
