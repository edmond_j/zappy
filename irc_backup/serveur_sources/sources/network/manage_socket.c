/*
** manage_socket.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Wed Apr 24 18:06:52 2013 david olivier
** Last update Thu Apr 25 18:47:02 2013 david olivier
*/

#include		"server.h"

void			reset_socket(t_settings *settings)
{
  t_client		*tmp;

  FD_ZERO(&settings->readfs);
  FD_SET(settings->socket, &settings->readfs);
  FD_SET(0, &settings->readfs);
  tmp = settings->clients;
  while (tmp)
    {
      FD_SET(tmp->socket, &settings->readfs);
      tmp = tmp->next;
    }
}

int			get_max_socket(t_settings *settings)
{
  int			max;
  t_client		*tmp;

  max = settings->socket;
  tmp = settings->clients;
  while (tmp)
    {
      if (max < tmp->socket)
	max = tmp->socket;
      tmp = tmp->next;
    }
  return (max);
}
