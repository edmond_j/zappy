/*
** init_network.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Wed Apr 24 15:23:04 2013 david olivier
** Last update Thu Apr 25 12:30:04 2013 david olivier
*/

#include		<sys/socket.h>
#include		<unistd.h>
#include		<string.h>
#include		<netdb.h>
#include		"server.h"

int			init_network(t_settings *settings)
{
  int			no_block;

  no_block = 1;
  if ((settings->std_cin = fdopen(STD_CIN, "r")) == NULL)
    return (error("Error: open standard output failed."));
  if ((settings->socket = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    return (error("Error: create socket server failed."));
  settings->inf_net.sin_addr.s_addr = htonl(INADDR_ANY);
  settings->inf_net.sin_family = AF_INET;
  settings->inf_net.sin_port = htons(settings->port);
  if (setsockopt(settings->socket, SOL_SOCKET, SO_REUSEADDR,
		 &no_block, sizeof(int)) == -1)
    return (errorcs("Error: setsockopt server failed.", settings->socket));
  if (bind(settings->socket, (struct sockaddr *)&settings->inf_net,
	   (sizeof(struct sockaddr_in))) == -1)
    return (errorcs("Error: bind failed.", settings->socket));
  if (listen(settings->socket, MAX_CLIENTS) == - 1)
    return (errorcs("Error: listen failed.", settings->socket));
  return (0);
}
