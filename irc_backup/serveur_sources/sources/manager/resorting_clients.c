/*
** resorting_clients.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Sun Apr 28 02:30:34 2013 david olivier
** Last update Sun Apr 28 13:29:16 2013 david olivier
*/

#include		<string.h>
#include		"server.h"

int			sort_channel(t_channel **channel,
				     t_client_connected *client)
{
  t_client_connected	*tmp;
  t_client_connected	*prev;

  prev = NULL;
  tmp = (*channel)->clients_connected;
  while ((tmp) && (strcmp(client->client->nick_name,
			  tmp->client->nick_name) >= 0))
    {
      prev = tmp;
      tmp = tmp->next;
    }
  client->next = tmp;
  if (prev)
    prev->next = client;
  else
    (*channel)->clients_connected = client;
  if (!(*channel)->clients_connected)
    (*channel)->clients_connected = client;
  return (0);
}

int			resort_in_channel(t_channel **channel, char *str)
{
  t_client_connected	*tmp;
  t_client_connected	*prev;

  prev = NULL;
  tmp = (*channel)->clients_connected;
  while ((tmp) && (strcmp(str, tmp->client->nick_name) != 0))
    {
      prev = tmp;
      tmp = tmp->next;
    }
  if (!tmp)
    return (0);
  if (prev)
    prev->next = tmp->next;
  if (tmp == (*channel)->clients_connected)
    (*channel)->clients_connected = tmp->next;
  if (sort_channel(channel, tmp) == -1)
    return (-1);
  return (0);
}

int			resort_users(t_settings *settings, t_client *current)
{
  t_channel		*tmp;

  tmp = settings->channels;
  while (tmp)
    {
      if (resort_in_channel(&tmp, current->nick_name) == -1)
	return (-1);
      tmp = tmp->next;
    }
  return (0);
}
