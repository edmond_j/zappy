/*
** server.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Wed Apr 24 10:44:08 2013 david olivier
** Last update Wed Apr 24 20:25:18 2013 david olivier
*/

#include		<unistd.h>
#include		<stdlib.h>
#include		<stdio.h>
#include		"server.h"

int			start_server(t_settings *settings)
{
  if (write(1, "Server started !\n", 18) == -1)
    return (error("Error: write server started."));
  FD_ZERO(&settings->writefs);
  while (settings->quit != true)
    if (manage_transmission(settings) == -1)
      return (-1);
  return (0);
}

int			launch_server(int port)
{
  t_settings		settings;
  int			ret;

  ret = -1;
  settings.port = port;
  settings.clients = NULL;
  settings.channels = NULL;
  settings.quit = false;
  if (init_network(&settings) == -1)
    return (-1);
  ret = start_server(&settings);
  quit_server(&settings);
  return (ret);
}
