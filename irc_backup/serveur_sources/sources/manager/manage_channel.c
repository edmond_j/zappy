/*
** manage_channel.c for my_irc in /home/olivie_d//afs/fileslinux/depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Fri Apr 26 11:40:15 2013 david olivier
** Last update Sun Apr 28 02:18:42 2013 david olivier
*/

#include		<unistd.h>
#include		<stdlib.h>
#include		<string.h>
#include		"server.h"

t_client_connected	*get_prev_client_co(t_client_connected *client,
					    t_client *current)
{
  t_client_connected	*tmp;
  t_client_connected	*prev;

  prev = NULL;
  tmp = client;
  while ((tmp) && (tmp->client != current) &&
	 (strcmp(current->nick_name, tmp->client->nick_name) >= 0))
    {
      prev = tmp;
      tmp = tmp->next;
    }
  if ((tmp) && (tmp->client == current))
    prev = tmp;
  return (prev);
}

t_client_connected	*init_new_client(t_client *current, t_channel *channel)
{
  t_client_connected	*new;

  if (add_reference_ch_connected(channel, current) == -1)
    return (NULL);
  if ((new = malloc(sizeof(t_client_connected))) == NULL)
    {
      write(2, "Error: Malloc add client in channel failed.\n", 44);
      return (NULL);
    }
  new->client = current;
  new->next = NULL;
  return (new);
}

int			add_client_in_channel(t_channel *channel,
					      t_client *current)
{
  t_client_connected		*new;
  t_client_connected		*prev;

  prev = get_prev_client_co(channel->clients_connected, current);
  if ((prev) && (prev->client == current))
    return (0);
  if ((new = init_new_client(current, channel)) == NULL)
    return (-1);
  if (prev)
    {
      new->next = prev->next;
      prev->next = new;
    }
  else if ((channel->clients_connected) &&
	   (strcmp(current->nick_name,
		   channel->clients_connected->client->nick_name) < 0))
    {
      new->next = channel->clients_connected;
      channel->clients_connected = new;
    }
  if (!channel->clients_connected)
    channel->clients_connected = new;
  return (0);
}

t_channel		*get_prev_channel(t_channel *channels, char *str)
{
  t_channel		*tmp;
  t_channel		*prev;

  prev = NULL;
  tmp = channels;
  while ((tmp) && (strcmp(str, tmp->name) >= 0))
    {
      prev = tmp;
      tmp = tmp->next;
    }
  return (prev);
}

int			create_channel(t_settings *settings, t_client *current)
{
  t_channel		*new;
  t_channel		*prev;

  prev = get_prev_channel(settings->channels, current->packet->channel);
  if ((new = malloc(sizeof(t_channel))) == NULL)
    return (error("Error: Malloc new channel failed."));
  new->clients_connected = NULL;
  strcpy(new->name, current->packet->channel);
  new->next = NULL;
  if (prev)
    {
      new->next = prev->next;
      prev->next = new;
    }
  else if ((settings->channels) &&
	   (strcmp(current->packet->channel, settings->channels->name) < 0))
    {
      new->next = settings->channels;
      settings->channels = new;
    }
  if (!settings->channels)
    settings->channels = new;
  return (add_client_in_channel(new, current));
}
