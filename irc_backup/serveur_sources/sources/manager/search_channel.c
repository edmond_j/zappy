/*
** search_channel.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Sun Apr 28 02:12:57 2013 david olivier
** Last update Sun Apr 28 02:19:03 2013 david olivier
*/

#include		<string.h>
#include		"server.h"

t_channel		*search_channel(t_settings *settings,
					char *name_channel)
{
  t_channel		*tmp;

  tmp = settings->channels;
  while ((tmp) && (strcmp(tmp->name, name_channel) != 0))
    tmp = tmp->next;
  return (tmp);
}
