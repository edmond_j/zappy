/*
** quit_server.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Wed Apr 24 15:10:40 2013 david olivier
** Last update Fri Apr 26 22:04:09 2013 david olivier
*/

#include		<stdio.h>
#include		<string.h>
#include		<stdlib.h>
#include		<unistd.h>
#include		"server.h"

void			free_channels(t_settings *settings)
{
  t_channel			*tmp_ch;
  t_channel			*prev_ch;
  t_client_connected		*tmp_client;
  t_client_connected		*prev_client;

  tmp_ch = settings->channels;
  while (tmp_ch)
    {
      prev_ch = tmp_ch;
      tmp_ch = tmp_ch->next;
      tmp_client = prev_ch->clients_connected;
      while (tmp_client)
	{
	  prev_client = tmp_client;
	  tmp_client = tmp_client->next;
	  free(prev_client);
	}
      free(prev_ch);
    }
}

void			free_clients(t_settings *settings)
{
  t_client		*tmp;
  t_client		*prev;
  t_channel_connected	*tmp_ch_co;
  t_channel_connected	*prev_ch_co;

  tmp = settings->clients;
  while (tmp)
    {
      prev = tmp;
      tmp = tmp->next;
      if (close(prev->socket) == -1)
	write(2, "Warning: closesocket clients failed.\n", 37);
      tmp_ch_co = prev->channels_connected;
      while (tmp_ch_co)
	{
	  prev_ch_co = tmp_ch_co;
	  tmp_ch_co = tmp_ch_co->next;
	  free(prev_ch_co);
	}
      free(prev);
    }
}

void			quit_server(t_settings *settings)
{
  free_channels(settings);
  free_clients(settings);
  if (fclose(settings->std_cin) == -1)
    write(2, "Warning: closeFile std input server failed.\n", 44);
  if (close(settings->socket) == -1)
    write(2, "Warning: closesocket server failed.\n", 37);
}

bool			is_not_invalid_char(char *str)
{
  bool			ret;
  bool			is_char;
  int			cpt;

  ret = true;
  is_char = false;
  if (str[0] == '\n')
    ret = false;
  if (ret != false)
    {
      cpt = -1;
      while ((str) && (str[++cpt]))
	{
	  if ((str[cpt] != ' ') && (str[cpt] != '\t') && (str[cpt] != '\n'))
	    is_char = true;
	}
      ret = is_char;
    }
  return (ret);
}

int			check_is_quit(t_settings *settings)
{
  char			*tmp;
  size_t		len;
  int			ret;

  len = 0;
  tmp = NULL;
  if ((ret = getline(&tmp, &len, settings->std_cin)) == 0)
    return (error("Error: read standard output failed."));
  if ((ret == -1) || ((tmp) && (strcmp(tmp, COMMAND_QUIT) == 0)))
    {
      write(1, "Server shutdown...\n", 19);
      settings->quit = true;
    }
  else if ((is_not_invalid_char(tmp) == true) &&
	   (write(2, "Command not found.\n", 19) == -1))
    return (error("Error: write failed."));
  free(tmp);
  return (0);
}
