/*
** manage_client.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Wed Apr 24 21:15:22 2013 david olivier
** Last update Sun Apr 28 11:08:44 2013 david olivier
*/

#include		<string.h>
#include		<stdlib.h>
#include		<unistd.h>
#include		"server.h"

t_client		*get_prev_client(t_client *clients)
{
  t_client		*tmp;

  tmp = clients;
  while (tmp && tmp->next)
    tmp = tmp->next;
  return (tmp);
}

int			delete_client(t_client *current, t_client *prev,
				      t_settings *settings)
{
  if (prev)
    prev->next = current->next;
  else
    settings->clients = current->next;
  if (remove_client_channel(settings, current) == -1)
    return (-1);
  free(current);
  return (0);
}

int			disconnect_client(t_settings *settings,
					  int tmp_sock)
{
  int			ret;
  t_client		*tmp;
  t_client		*prev;

  prev = NULL;
  tmp = settings->clients;
  while ((tmp) && (tmp->socket != tmp_sock))
    {
      prev = tmp;
      tmp = tmp->next;
    }
  if (!tmp)
    {
      if (write(2, "Error: disconnect client failed.\n", 33) == -1)
	return (-1);
      return (0);
    }
  FD_CLR(tmp->socket, &settings->readfs);
  FD_CLR(tmp->socket, &settings->writefs);
  ret = dprintf(1, "Client %s disconnected.\n", (tmp->status == UNIDENTIFIED) ?
		("unknown") : (tmp->nick_name));
  ret = close(tmp->socket);
  ret = (ret == -1) ? (-1) : (delete_client(tmp, prev, settings));
  return ((ret == -1) ? (error("Error: close socket client.")) : (0));
}

int			add_client(t_settings *settings, int tmp)
{
  t_client		*new;
  t_client		*prev;

  prev = get_prev_client(settings->clients);
  if ((new = malloc(sizeof(t_client))) == NULL)
    return (error("Error: Malloc new client failed."));
  new->socket = tmp;
  memset(new->nick_name, 0, NAME_LEN);
  new->read = 0;
  new->status = UNIDENTIFIED;
  new->bf_read.end = 0;
  new->bf_write.start = 0;
  new->bf_write.end = 0;
  new->packet = NULL;
  new->channels_connected = NULL;
  new->next = NULL;
  if (prev)
    prev->next = new;
  if (!settings->clients)
    settings->clients = new;
  return (0);
}

int			connect_client(t_settings *settings)
{
  unsigned int		len;
  int			tmp;

  len = SOCKADDR_LEN;
  if ((tmp = accept(settings->socket,
		    (struct sockaddr *)&settings->inf_net, &len)) == -1)
    return (error("Error: accept client failed."));
  if (add_client(settings, tmp) == -1)
    return (-1);
  if (write(1, "Client connected.\n", 18) == -1)
    return (error("Error: write in connect client."));
  return (0);
}
