/*
** is_port.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Wed Apr 24 11:00:42 2013 david olivier
** Last update Sun Apr 28 09:54:00 2013 david olivier
*/

#include		<unistd.h>
#include		"server.h"

bool			is_port(char *str)
{
  int			cpt;
  bool			ret;

  ret = true;
  cpt = -1;
  while ((str) && (str[++cpt]))
    {
      if ((str[cpt] < '0') || (str[cpt] > '9'))
	ret = false;
    }
  if (cpt <= 0)
    ret = false;
  if (ret == false)
    write(2, "USAGE: ./server [PORT]\nError: Port entered is incorrect.\n",
	  57);
  return (ret);
}
