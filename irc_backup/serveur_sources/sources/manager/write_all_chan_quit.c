/*
** write_all_chan_quit.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Sun Apr 28 12:05:07 2013 david olivier
** Last update Sun Apr 28 14:04:41 2013 david olivier
*/

#include		<string.h>
#include		"server.h"
int			write_circular_buff_part_all(t_client *dest,
						     t_client *src,
						     char *buffer)
{
  t_packet		packet;
  char			packet_tmp[PACKET_SIZE];
  int			len_write;

  memset(&packet, 0, PACKET_SIZE);
  packet.type = PART;
  packet.size = PACKET_SIZE;
  if (buffer)
    strcpy(packet.request, buffer);
  strcpy(packet.nick_name, src->nick_name);
  memcpy(&packet_tmp[0], &packet, PACKET_SIZE);
  len_write = (((dest->bf_write.end + PACKET_SIZE) >= SIZE_BUFFER) ?
	       (SIZE_BUFFER - dest->bf_write.end) : (PACKET_SIZE));
  memcpy(&dest->bf_write.buffer[dest->bf_write.end], &packet_tmp[0],
	 len_write);
  dest->bf_write.end += len_write;
  if (dest->bf_write.end >= (int)SIZE_BUFFER)
    dest->bf_write.end = 0;
  if (len_write != PACKET_SIZE)
    write_second_part_in_buffer(len_write, dest, packet_tmp);
  return (0);
}

void			write_all_chan_quit(char *name_chan, int *len_write,
					    char *buffer)
{
  int			len_tmp;

  if (strcmp(STRING_EMPTY, name_chan) == 0)
    return ;
  len_tmp = strlen(name_chan) + 1;
  if ((*len_write + len_tmp) < (int)SIZE_BUFFER)
    {
      *len_write += len_tmp;
      strcat(buffer, name_chan);
      buffer[*len_write - 1] = '\n';
    }
}
