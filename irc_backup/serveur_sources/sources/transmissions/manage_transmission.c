/*
** manage_transmission.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Wed Apr 24 17:08:16 2013 david olivier
** Last update Fri Apr 26 11:19:38 2013 david olivier
*/

#include		<unistd.h>
#include		<stdlib.h>
#include		<string.h>
#include		"server.h"

int			manage_transmission_client_read(t_settings *settings)
{
  t_client		*tmp;
  t_client		*prev;
  int			ret;

  if ((FD_ISSET(settings->socket, &settings->readfs) == 1) &&
      (connect_client(settings) == -1))
    return (-1);
  else
    {
      tmp = settings->clients;
      while (tmp)
	{
	  ret = 0;
	  if (FD_ISSET(tmp->socket, &settings->readfs) == 1)
	    ret = transmission_client_read(settings, tmp);
	  prev = tmp;
	  tmp = tmp->next;
	  if (ret == -1)
	    return (-1);
	  else if (ret == 1)
	    return (disconnect_client(settings, prev->socket));
	}
    }
  return (0);
}

int			manage_transmission_client_write(t_settings *settings)
{
  t_client		*tmp;
  t_client		*prev;
  int			ret;

  tmp = settings->clients;
  while (tmp)
    {
      ret = 0;
      if (FD_ISSET(tmp->socket, &settings->writefs) == 1)
      	ret = transmission_client_write(settings, tmp);
      prev = tmp;
      tmp = tmp->next;
      if (ret == -1)
	return (-1);
      else if (ret == 1)
	return (disconnect_client(settings, prev->socket));
    }
  return (0);
}

int			manage_transmission(t_settings *settings)
{
  struct timeval	wait_select;

  wait_select.tv_sec = 0;
  wait_select.tv_usec = TIME_OUT;
  reset_socket(settings);
  if (select(get_max_socket(settings) + 1, &settings->readfs,
	     &settings->writefs, NULL, &wait_select) < 0)
    return (error("Error: select failed."));
  if ((FD_ISSET(0, &settings->readfs)) == 1)
    {
      if (check_is_quit(settings) == -1)
	return (-1);
    }
  if (settings->quit == false)
    {
      if (manage_transmission_client_write(settings) == -1)
  	return (-1);
      if (manage_transmission_client_read(settings) == -1)
  	return (-1);
    }
  return (0);
}
