/*
** transmission_client_read.c for my_irc in /home/olivie_d//afs/fileslinux/depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Thu Apr 25 12:36:02 2013 david olivier
** Last update Sun Apr 28 09:53:05 2013 david olivier
*/

#include		<stdio.h>
#include		<unistd.h>
#include		<string.h>
#include		"server.h"

int			write_in_packet(t_client *current)
{
  int			len_write;
  int			pos_end;

  len_write = (((current->bf_read.start + PACKET_SIZE) >= SIZE_BUFFER) ?
	       (SIZE_BUFFER - current->bf_read.start) : (PACKET_SIZE));
  memcpy(&current->buffer_tmp[0],
	 &current->bf_read.buffer[current->bf_read.start], len_write);
  if (len_write != PACKET_SIZE)
    {
      pos_end = len_write;
      len_write = PACKET_SIZE - pos_end;
      memcpy(&current->buffer_tmp[pos_end + 1], \
	     &current->bf_read.buffer[0], len_write);
    }
  current->packet = (t_packet *)&current->buffer_tmp[0];
  return (0);
}

int			read_socket(t_client *current)
{
  int			len_read;
  int			end;

  end = current->bf_read.end;
  if (current->read == 0)
    {
      current->packet = NULL;
      current->read = PACKET_SIZE;
      current->bf_read.start = current->bf_read.end;
    }
  if ((len_read =
       read(current->socket,
	    &current->bf_read.buffer[(current->bf_read.start == end) ?
				     (current->bf_read.start) : (end)],
	    ((current->bf_read.end + current->read) >= SIZE_BUFFER) ?
	    (SIZE_BUFFER - current->bf_read.end) :
	    (current->read))) <= 0)
    return (-1);
  current->read -= len_read;
  current->bf_read.end += len_read;
  current->bf_read.end %= SIZE_BUFFER;
  if ((current->read == 0) && (write_in_packet(current) == -1))
    return (error("Error: write in packet failed."));
  return (0);
}

int			transmission_client_read(t_settings *settings,
						 t_client *current)
{
  if (read_socket(current) == -1)
    return (1);
  if (current->packet)
    {
      if (manage_commands(settings, current) == -1)
	return (1);
    }
  return (0);
}
