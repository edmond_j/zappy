/*
** write_circular_buffer.c for my_irc in /home/olivie_d//afs/fileslinux/depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Fri Apr 26 01:08:02 2013 david olivier
** Last update Fri Apr 26 03:57:01 2013 david olivier
*/

#include		<unistd.h>
#include		<string.h>
#include		"server.h"

void			write_second_part_in_buffer(int len_write,
						    t_client *client,
						    char *packet_tmp)
{
  int			pos_tmp;

  pos_tmp = len_write;
  len_write = PACKET_SIZE - pos_tmp;
  memcpy(&client->bf_write.buffer[client->bf_write.end],
	 &packet_tmp[pos_tmp + 1],
	 len_write);
  client->bf_write.end += len_write;
}

int			write_circular_buffer(t_client *client, char *str,
					      t_type type, char *nick_name)
{
  t_packet		packet;
  char			packet_tmp[PACKET_SIZE];
  int			len_write;

  memset(&packet, 0, PACKET_SIZE);
  packet.type = type;
  packet.size = PACKET_SIZE;
  strcpy(packet.nick_name, nick_name);
  strcpy(packet.request, str);
  memcpy(&packet_tmp[0], &packet, PACKET_SIZE);
  len_write = (((client->bf_write.end + PACKET_SIZE) >= SIZE_BUFFER) ?
	       (SIZE_BUFFER - client->bf_write.end) : (PACKET_SIZE));
  memcpy(&client->bf_write.buffer[client->bf_write.end], &packet_tmp[0],
	 len_write);
  client->bf_write.end += len_write;
  if (client->bf_write.end >= (int)SIZE_BUFFER)
    client->bf_write.end = 0;
  if (len_write != PACKET_SIZE)
    write_second_part_in_buffer(len_write, client, packet_tmp);
  return (0);
}
