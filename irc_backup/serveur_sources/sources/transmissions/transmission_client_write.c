/*
** transmission_client_write.c for my_irc in /home/olivie_d//afs/fileslinux/depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Fri Apr 26 01:53:34 2013 david olivier
** Last update Fri Apr 26 23:00:30 2013 david olivier
*/

#include		<stdio.h>
#include		<string.h>
#include		<unistd.h>
#include		"server.h"

int			sending_one_packet(t_client *current)
{
  char			buffer_tmp[PACKET_SIZE];
  int			len_write;
  int			pos_tmp;

  len_write = SIZE_BUFFER - current->bf_write.start;
  memcpy(&buffer_tmp[0], &current->bf_write.buffer[current->bf_write.start],
	 len_write);
  current->bf_write.start = 0;
  pos_tmp = len_write;
  len_write = PACKET_SIZE - pos_tmp;
  memcpy(&buffer_tmp[pos_tmp + 1],
	 &current->bf_write.buffer[current->bf_write.start],
	 len_write);
  current->bf_write.start += len_write;
  if (write(current->socket, &buffer_tmp[0], PACKET_SIZE) == -1)
    return (-1);
  return (0);
}

int			transmission_client_write(t_settings *settings,
						  t_client *current)
{
  int			ret;

  ret = 0;
  while (current->bf_write.start != current->bf_write.end)
    {
      if ((current->bf_write.start + PACKET_SIZE) >= SIZE_BUFFER)
	ret = sending_one_packet(current);
      else
	{
	  ret = write(current->socket,
		      &current->bf_write.buffer[current->bf_write.start],
		      PACKET_SIZE);
	  current->bf_write.start += PACKET_SIZE;
	}
      if (ret == -1)
	{
	  dprintf(2, "Error: sending client %s failed.\n",
		  current->nick_name);
	  return (1);
	}
    }
  FD_CLR(current->socket, &settings->writefs);
  return (0);
}
