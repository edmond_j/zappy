/*
** manage_commands.c for my_irc in /home/olivie_d//afs/fileslinux/depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Fri Apr 26 00:10:53 2013 david olivier
** Last update Sun Apr 28 15:34:29 2013 david olivier
*/

#include		"server.h"

int			manage_commands(t_settings *settings, t_client *current)
{
  t_ptrfunc		func_cmd[MAX_READ_VALUE] = {&protocol, &join_cmd,
						    &part_cmd, &msg_cmd,
						    &nick_cmd, &list_cmd,
						    &users_cmd, &private_cmd,
						    &send_cmd,
						    &accept_cmd, &refuse_cmd};

  if (current->packet->type >= MAX_READ_VALUE)
    return (0);
  return (func_cmd[current->packet->type](settings, current));
}
