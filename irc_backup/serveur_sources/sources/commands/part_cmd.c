/*
** part_cmd.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Fri Apr 26 17:06:48 2013 david olivier
** Last update Sun Apr 28 14:07:01 2013 david olivier
*/

#include		<string.h>
#include		<stdlib.h>
#include		"server.h"

int			update_channels(t_settings *settings)
{
  t_channel		*tmp;
  t_channel		*prev;

  prev = NULL;
  tmp = settings->channels;
  while ((tmp) && (tmp->clients_connected != NULL))
    {
      prev = tmp;
      tmp = tmp->next;
    }
  if (!tmp)
    return (0);
  if (prev)
    prev->next = tmp->next;
  else
    settings->channels = tmp->next;
  free(tmp);
  return (0);
}

int			remove_client(t_channel *channel,
				      t_settings *settings, t_client *current)
{
  t_client_connected	*tmp;
  t_client_connected	*prev;

  prev = NULL;
  tmp = channel->clients_connected;
  while ((tmp) && (tmp->client != current))
    {
      prev = tmp;
      tmp = tmp->next;
    }
  if (!tmp)
    return (0);
  if (prev)
    prev->next = tmp->next;
  else
    channel->clients_connected = tmp->next;
  free(tmp);
  return (update_channels(settings));
}

int			sending_part_clients(char *buffer,
					     t_settings *settings,
					     t_client *current)
{
  t_client		*tmp;

  tmp = settings->clients;
  while (tmp)
    {
      if (tmp != current)
	{
	  if (write_circular_buff_part_all(tmp, current, buffer) == -1)
	    return (error("Error: write part client in channel."));
	  FD_SET(tmp->socket, &settings->writefs);
	}
      tmp = tmp->next;
    }
  return (0);
}

int			remove_client_channel(t_settings *settings,
					      t_client *current)
{
  t_channel_connected	*tmp;
  t_channel_connected	*prev;
  char			buffer[NAME_LEN];
  int			len_write;
  char			name_ch_tmp[CHANNEL_LEN];

  prev = NULL;
  memset(buffer, 0, NAME_LEN);
  len_write = 0;
  tmp = current->channels_connected;
  while (tmp)
    {
      reset_name_ch_tmp(name_ch_tmp, tmp->channel);
      if (remove_client(tmp->channel, settings, current) == -1)
	return (-1);
      write_all_chan_quit(name_ch_tmp, &len_write, buffer);
      prev = tmp;
      tmp = tmp->next;
      free(prev);
    }
  if (sending_part_clients(buffer, settings, current) == -1)
    return (error("Error: sending part clients failed."));
  return (0);
}

int			part_cmd(t_settings *settings, t_client *current)
{
  t_channel_connected	*tmp;
  t_channel_connected	*prev;

  prev = NULL;
  tmp = current->channels_connected;
  while ((tmp) && (strcmp(tmp->channel->name, current->packet->channel) != 0))
    {
      prev = tmp;
      tmp = tmp->next;
    }
  if (!tmp)
    return (0);
  if (remove_client(tmp->channel, settings, current) == -1)
    return (error("Error: remove client failed in part command."));
  if (write_status_in_channel(tmp->channel, current, settings, PART) == -1)
    return (error("Error: write part client in channel."));
  if (prev)
    prev->next = tmp->next;
  else
    current->channels_connected = tmp->next;
  free(tmp);
  return (0);
}
