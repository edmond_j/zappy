/*
** refuse_cmd.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/serveur_sources/sources
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Sun Apr 28 15:39:25 2013 david olivier
** Last update Sun Apr 28 15:40:00 2013 david olivier
*/

#include		<string.h>
#include		"server.h"

int			refuse_cmd(t_settings *settings, t_client *current)
{
  t_client		*tmp;

  tmp = settings->clients;
  while ((tmp) && (strcmp(tmp->nick_name, current->packet->nick_name) != 0))
    tmp = tmp->next;
  if (!tmp)
    {
      if (write_circular_buff_file(current, current,
				   current->packet->nick_name,
				   ABORT_FILE) == -1)
	return (error("Error: write refuse fail file failed."));
      FD_SET(current->socket, &settings->writefs);
      return (0);
    }
  if (write_circular_buff_file(tmp, current, current->nick_name,
			       REFUSE_FILE) == -1)
    return (error("Error: write resfuse file failed."));
  FD_SET(tmp->socket, &settings->writefs);
  return (0);
}
