/*
** join_cmd.c for my_irc in /home/olivie_d//afs/fileslinux/depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Fri Apr 26 11:37:58 2013 david olivier
** Last update Fri Apr 26 23:34:36 2013 david olivier
*/

#include		<unistd.h>
#include		<string.h>
#include		<stdlib.h>
#include		"server.h"

int			write_circular_buff_status(t_client *dest,
						   t_client *src,
						   t_type type,
						   t_channel *channel)
{
  t_packet		packet;
  char			packet_tmp[PACKET_SIZE];
  int			len_write;

  memset(&packet, 0, PACKET_SIZE);
  packet.type = type;
  packet.size = PACKET_SIZE;
  strcpy(packet.nick_name, src->nick_name);
  strcpy(packet.channel, channel->name);
  memcpy(&packet_tmp[0], &packet, PACKET_SIZE);
  len_write = (((dest->bf_write.end + PACKET_SIZE) >= SIZE_BUFFER) ?
	       (SIZE_BUFFER - dest->bf_write.end) : (PACKET_SIZE));
  memcpy(&dest->bf_write.buffer[dest->bf_write.end], &packet_tmp[0],
	 len_write);
  dest->bf_write.end += len_write;
  if (dest->bf_write.end >= (int)SIZE_BUFFER)
    dest->bf_write.end = 0;
  if (len_write != PACKET_SIZE)
    write_second_part_in_buffer(len_write, dest, packet_tmp);
  return (0);
}

int			write_status_in_channel(t_channel *channel,
						t_client *client,
						t_settings *settings,
						t_type type)
{
  t_client_connected	*tmp;

  tmp = channel->clients_connected;
  while (tmp)
    {
      if (write_circular_buff_status(tmp->client, client, type,
				     channel) == -1)
	return (error("Error: write in buffer client for write failed."));
      FD_SET(tmp->client->socket, &settings->writefs);
      tmp = tmp->next;
    }
  return (0);
}

  t_channel_connected	*get_prev_ch_co(t_channel_connected *channels)
{
  t_channel_connected	*tmp;

  tmp = channels;
  while ((tmp) && (tmp->next))
    tmp = tmp->next;
  return (tmp);
}

int			add_reference_ch_connected(t_channel *channel,
						   t_client *client)
{
  t_channel_connected		*new;
  t_channel_connected		*prev;

  prev = get_prev_ch_co(client->channels_connected);
  if ((new = malloc(sizeof(t_client_connected))) == NULL)
    return (error("Error: Malloc add client in channel failed."));
  new->channel = channel;
  new->next = NULL;
  if (prev)
    prev->next = new;
  if (!client->channels_connected)
    client->channels_connected = new;
  return (0);
}

int			join_cmd(t_settings *settings,
				 t_client *current)
{
  t_channel		*found;

  if ((found = search_channel(settings, current->packet->channel)) == NULL)
    {
      if (create_channel(settings, current) == -1)
	return (error("Error: create channel failed."));
    }
  else if (add_client_in_channel(found, current) == -1)
    return (error("Error: adding client in channel."));
  if ((found = search_channel(settings, current->packet->channel)) == NULL)
    return (error("Error: adding client in channel."));
  if (write_status_in_channel(found, current, settings, JOIN) == -1)
    return (error("Error: write join client in channel."));
  return (0);
}
