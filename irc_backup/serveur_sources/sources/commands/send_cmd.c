/*
** send_cmd.c for my_irc in /home/olivie_d//depotsvn/saveserver/toto/serveur/sources
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Sun Apr 28 15:13:59 2013 david olivier
** Last update Sun Apr 28 15:35:30 2013 david olivier
*/

#include		<string.h>
#include		"server.h"

int			write_circular_buff_file(t_client *dest,
						 t_client *src,
						 char *str,
						 t_type type)
{
  t_packet		packet;
  char			packet_tmp[PACKET_SIZE];
  int			len_write;

  memset(&packet, 0, PACKET_SIZE);
  packet.type = type;
  packet.size = src->packet->size;
  strcpy(packet.nick_name, str);
  memcpy(packet.request, src->packet->request, packet.size);
  memcpy(&packet_tmp[0], &packet, PACKET_SIZE);
  len_write = (((dest->bf_write.end + PACKET_SIZE) >= SIZE_BUFFER) ?
	       (SIZE_BUFFER - dest->bf_write.end) : (PACKET_SIZE));
  memcpy(&dest->bf_write.buffer[dest->bf_write.end], &packet_tmp[0],
	 len_write);
  dest->bf_write.end += len_write;
  if (dest->bf_write.end >= (int)SIZE_BUFFER)
    dest->bf_write.end = 0;
  if (len_write != PACKET_SIZE)
    write_second_part_in_buffer(len_write, dest, packet_tmp);
  return (0);
}

int			send_cmd(t_settings *settings, t_client *current)
{
  t_client		*tmp;

  tmp = settings->clients;
  while ((tmp) && (strcmp(tmp->nick_name, current->packet->nick_name) != 0))
    tmp = tmp->next;
  if (!tmp)
    {
      if (write_circular_buff_file(current, current,
				   current->packet->nick_name,
				   ABORT_FILE) == -1)
	return (error("Error: write abort file failed."));
      FD_SET(current->socket, &settings->writefs);
      return (0);
    }
  if (write_circular_buff_file(tmp, current, current->nick_name,
			       SEND_FILE) == -1)
    return (error("Error: write file failed."));
  FD_SET(tmp->socket, &settings->writefs);
  return (0);
}
