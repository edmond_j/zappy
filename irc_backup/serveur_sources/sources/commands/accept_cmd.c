/*
** accept_cmd.c for my_irc in /home/olivie_d//depotsvn/saveserver/toto/serveur/sources
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Sun Apr 28 15:31:00 2013 david olivier
** Last update Sun Apr 28 15:39:08 2013 david olivier
*/

#include		<string.h>
#include		"server.h"

int			accept_cmd(t_settings *settings, t_client *current)
{
  t_client		*tmp;

  tmp = settings->clients;
  while ((tmp) && (strcmp(tmp->nick_name, current->packet->nick_name) != 0))
    tmp = tmp->next;
  if (!tmp)
    {
      if (write_circular_buff_file(current, current,
				   current->packet->nick_name,
				   (current->packet->size == 0) ?
				   (ABORT_FILE) : (REFUSE_FILE)) == -1)
	return (error("Error: write accept fail file failed."));
      FD_SET(current->socket, &settings->writefs);
      return (0);
    }
  if (write_circular_buff_file(tmp, current, current->nick_name,
			       ACCEPT_FILE) == -1)
    return (error("Error: write accept file failed."));
  FD_SET(tmp->socket, &settings->writefs);
  return (0);
}
