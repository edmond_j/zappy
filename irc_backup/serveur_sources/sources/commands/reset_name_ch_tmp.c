/*
** reset_name_ch_tmp.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Sun Apr 28 14:01:18 2013 david olivier
** Last update Sun Apr 28 14:06:10 2013 david olivier
*/

#include		<string.h>
#include		"server.h"

void			reset_name_ch_tmp(char *buffer, t_channel *channel)
{
  memset(buffer, 0, CHANNEL_LEN);
  if (channel)
    strcpy(buffer, channel->name);
}
