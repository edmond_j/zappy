/*
** protocol.c for my_irc in /home/olivie_d//afs/fileslinux/depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Fri Apr 26 00:19:36 2013 david olivier
** Last update Fri Apr 26 09:07:04 2013 david olivier
*/

#include		<limits.h>
#include		<unistd.h>
#include		<stdio.h>
#include		<stdlib.h>
#include		<string.h>
#include		"server.h"

bool			is_exist(char *new_name, t_settings *settings)
{
  t_client		*tmp;

  tmp = settings->clients;
  while (tmp)
    {
      if (strcmp(new_name, tmp->nick_name) == 0)
	return (true);
      tmp = tmp->next;
    }
  return (false);
}

int			get_new_name_for_client(t_settings *settings,
						t_client *current)
{
  char			buffer_tmp[NAME_LEN];
  int			cpt;

  if (is_exist(current->packet->nick_name, settings) == false)
    {
      strcpy(current->nick_name, current->packet->nick_name);
      return (0);
    }
  if (sprintf(buffer_tmp, "%s%d", BASE_NICK_NAME, 1) <= 0)
    return (-1);
  cpt = 2;
  while (is_exist(buffer_tmp, settings) == true)
    {
      if (sprintf(buffer_tmp, "%s%d", BASE_NICK_NAME, cpt) <= 0)
	return (-1);
      ++cpt;
      if (cpt >= (INT_MAX - 1))
	return (error("Error: too much clients."));
    }
  strcpy(current->nick_name, buffer_tmp);
  return (0);
}

int			protocol(t_settings *settings, t_client *current)
{
  if ((get_new_name_for_client(settings, current)) == -1)
    return (error("Error: get new name for client failed."));
  current->status = IDENTIFIED;
  if (strcmp(current->packet->request, SECU_ONE) != 0)
    {
      dprintf(2, "Error: Procotol is invalid for %s client.\n",
	      current->nick_name);
      return (-1);
    }
  if (write_circular_buffer(current, SECU_TWO, PROTOCOL,
			    current->nick_name) == -1)
    return (error("Error: write in buffer client for write failed."));
  FD_SET(current->socket, &settings->writefs);
  return (0);
}
