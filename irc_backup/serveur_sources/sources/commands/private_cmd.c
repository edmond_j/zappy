/*
** private_cmd.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Sat Apr 27 15:00:17 2013 david olivier
** Last update Sat Apr 27 18:20:14 2013 david olivier
*/

#include		<string.h>
#include		"server.h"

int			write_circular_buff_private(t_client *dest,
						    t_client *src,
						    t_type type)
{
  t_packet		packet;
  char			packet_tmp[PACKET_SIZE];
  int			len_write;

  memset(&packet, 0, PACKET_SIZE);
  packet.type = type;
  packet.size = PACKET_SIZE;
  if (type == PRIVATE)
    strcpy(packet.nick_name, src->nick_name);
  else
    strcpy(packet.nick_name, dest->packet->nick_name);
  if (type == PRIVATE)
    strcpy(packet.request, src->packet->request);
  memcpy(&packet_tmp[0], &packet, PACKET_SIZE);
  len_write = (((dest->bf_write.end + PACKET_SIZE) >= SIZE_BUFFER) ?
	       (SIZE_BUFFER - dest->bf_write.end) : (PACKET_SIZE));
  memcpy(&dest->bf_write.buffer[dest->bf_write.end], &packet_tmp[0],
	 len_write);
  dest->bf_write.end += len_write;
  if (dest->bf_write.end >= (int)SIZE_BUFFER)
    dest->bf_write.end = 0;
  if (len_write != PACKET_SIZE)
    write_second_part_in_buffer(len_write, dest, packet_tmp);
  return (0);
}

int			private_cmd(t_settings *settings, t_client *current)
{
  t_client		*tmp;

  tmp = settings->clients;
  while ((tmp) && (strcmp(tmp->nick_name, current->packet->nick_name) != 0))
    tmp = tmp->next;
  if (!tmp)
    {
      if (write_circular_buff_private(current, NULL, PRIVATE_ERR) == -1)
	return (error("Error: sending private message failed."));
      FD_SET(current->socket, &settings->writefs);
      return (0);
    }
  if (write_circular_buff_private(tmp, current, PRIVATE) == -1)
    return (error("Error: sending private message failed."));
  FD_SET(tmp->socket, &settings->writefs);
  return (0);
}
