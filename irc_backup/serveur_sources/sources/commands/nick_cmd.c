/*
** nick_cmd.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Sat Apr 27 13:38:44 2013 david olivier
** Last update Sun Apr 28 02:31:59 2013 david olivier
*/

#include		<stdio.h>
#include		<string.h>
#include		"server.h"

int			write_circular_buff_name(t_client *dest, t_client *src,
						 char *old_name,
						 t_channel *channel)
{
  t_packet		packet;
  char			packet_tmp[PACKET_SIZE];
  int			len_write;

  memset(&packet, 0, PACKET_SIZE);
  packet.type = NICK;
  packet.size = PACKET_SIZE;
  strcpy(packet.nick_name, old_name);
  if (channel)
    strcpy(packet.channel, channel->name);
  strcpy(packet.request, src->nick_name);
  memcpy(&packet_tmp[0], &packet, PACKET_SIZE);
  len_write = (((dest->bf_write.end + PACKET_SIZE) >= SIZE_BUFFER) ?
	       (SIZE_BUFFER - dest->bf_write.end) : (PACKET_SIZE));
  memcpy(&dest->bf_write.buffer[dest->bf_write.end], &packet_tmp[0],
	 len_write);
  dest->bf_write.end += len_write;
  if (dest->bf_write.end >= (int)SIZE_BUFFER)
    dest->bf_write.end = 0;
  if (len_write != PACKET_SIZE)
    write_second_part_in_buffer(len_write, dest, packet_tmp);
  return (0);
}

int			send_without_channel(t_settings *settings,
					     t_client *current,
					     char *old_name)
{
  if (write_circular_buff_name(current, current, old_name, NULL) == -1)
    return (-1);
  FD_SET(current->socket, &settings->writefs);
  return (0);
}

int			send_new_name_in_channels(t_settings *settings,
						  t_client *current,
						  char *old_name)
{
  t_channel_connected	*tmp_ch;
  t_client_connected	*tmp_client;

  tmp_ch = current->channels_connected;
  if (!tmp_ch)
    {
      if (send_without_channel(settings, current, old_name) == -1)
	return (-1);
      return (0);
    }
  while (tmp_ch)
    {
      tmp_client = tmp_ch->channel->clients_connected;
      while (tmp_client)
	{
	  if (write_circular_buff_name(tmp_client->client, current, old_name,
				       tmp_ch->channel) == -1)
	    return (-1);
	  FD_SET(tmp_client->client->socket, &settings->writefs);
	  tmp_client = tmp_client->next;
	}
      tmp_ch = tmp_ch->next;
    }
  return (0);
}

int			nick_cmd(t_settings *settings, t_client *current)
{
  char			buffer_tmp[NAME_LEN];

  if (strcmp(current->packet->request, current->nick_name) == 0)
    return (0);
  strcpy(buffer_tmp, current->nick_name);
  strcpy(current->packet->nick_name, current->packet->request);
  if ((get_new_name_for_client(settings, current)) == -1)
    return (error("Error: get new name for client failed in nick command."));
  if (send_new_name_in_channels(settings, current, buffer_tmp) == -1)
    return (error("Error: sending new name in channels failed."));
  if (resort_users(settings, current) == -1)
    return (error("Error: resorting users failed."));
  return (0);
}
