/*
** list_cmd.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Sat Apr 27 14:50:56 2013 david olivier
** Last update Sun Apr 28 03:09:22 2013 david olivier
*/

#include		<unistd.h>
#include		<string.h>
#include		"server.h"

int			write_circular_buff_list(t_client *dest,
						 t_type type,
						 char *buffer)
{
  t_packet		packet;
  char			packet_tmp[PACKET_SIZE];
  int			len_write;

  memset(&packet, 0, PACKET_SIZE);
  packet.type = type;
  packet.size = PACKET_SIZE;
  if (buffer)
    strcpy(packet.request, buffer);
  memcpy(&packet_tmp[0], &packet, PACKET_SIZE);
  len_write = (((dest->bf_write.end + PACKET_SIZE) >= SIZE_BUFFER) ?
	       (SIZE_BUFFER - dest->bf_write.end) : (PACKET_SIZE));
  memcpy(&dest->bf_write.buffer[dest->bf_write.end], &packet_tmp[0],
	 len_write);
  dest->bf_write.end += len_write;
  if (dest->bf_write.end >= (int)SIZE_BUFFER)
    dest->bf_write.end = 0;
  if (len_write != PACKET_SIZE)
    write_second_part_in_buffer(len_write, dest, packet_tmp);
  return (0);
}

int			write_list_in_buff(t_client *client,
					   t_channel *channel,
					   char *buffer)
{
  int			len_write;
  int			len_tmp;

  len_write = 0;
  while (channel)
    {
      len_tmp = 0;
      if (strstr(channel->name, client->packet->request) != NULL)
	{
	  len_tmp = strlen(channel->name) + 1;
	  if ((len_write + len_tmp) < REQUEST_LEN)
	    {
	      len_write += len_tmp;
	      strcat(buffer, channel->name);
	      buffer[len_write - 1] = '\n';
	    }
	  else
	    channel = NULL;
	}
      if (channel)
	channel = channel->next;
    }
  return (0);
}

t_channel		*get_current_channel(t_settings *settings, char *str,
					     t_client *current)
{
  t_channel		*tmp;

  tmp = settings->channels;
  if (!tmp)
    {
      if (write_circular_buff_list(current, END_LIST, NULL) == -1)
	write(2, "Error: write circular buffer in end lis failed.\n", 48);
      FD_SET(current->socket, &settings->writefs);
    }
  if (strcmp(STRING_EMPTY, str) == 0)
    return (tmp);
  while ((tmp) && (strcmp(tmp->name, str) != 0))
    tmp = tmp->next;
  return (tmp);
}

int			list_cmd(t_settings *settings, t_client *current)
{
  t_channel		*tmp;
  char			buffer[REQUEST_LEN];

  memset(buffer, 0, REQUEST_LEN);
  if ((tmp = get_current_channel(settings, current->packet->channel,
				 current)) == NULL)
    return (0);
  if (strcmp(STRING_EMPTY, current->packet->channel) != 0)
    tmp = tmp->next;
  if ((!tmp) && (write_circular_buff_list(current, END_LIST, NULL) == -1))
    return (error("Error: write circular buffer in end lis failed."));
  if (!tmp)
    {
      FD_SET(current->socket, &settings->writefs);
      return (0);
    }
  if (write_list_in_buff(current, tmp, buffer) == -1)
    return (error("Error: write list in buffer."));
  if (write_circular_buff_list(current,
			       (strcmp(STRING_EMPTY, buffer) == 0) ?
			       (END_LIST) : (LIST), buffer) == -1)
    return (error("Error: write circular buffer in list failed."));
  FD_SET(current->socket, &settings->writefs);
  return (0);
}
