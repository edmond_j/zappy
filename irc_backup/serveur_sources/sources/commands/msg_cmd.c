/*
** msg_cmd.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Fri Apr 26 22:11:10 2013 david olivier
** Last update Sun Apr 28 09:53:23 2013 david olivier
*/

#include		<unistd.h>
#include		<string.h>
#include		"server.h"

int			write_circular_buffer_msg(t_client *dest, t_client *src)
{
  t_packet		packet;
  char			packet_tmp[PACKET_SIZE];
  int			len_write;

  memset(&packet, 0, PACKET_SIZE);
  packet.type = MSG;
  packet.size = PACKET_SIZE;
  strcpy(packet.nick_name, src->nick_name);
  strcpy(packet.channel, src->packet->channel);
  strcpy(packet.request, src->packet->request);
  memcpy(&packet_tmp[0], &packet, PACKET_SIZE);
  len_write = (((dest->bf_write.end + PACKET_SIZE) >= SIZE_BUFFER) ?
	       (SIZE_BUFFER - dest->bf_write.end) : (PACKET_SIZE));
  memcpy(&dest->bf_write.buffer[dest->bf_write.end], &packet_tmp[0],
	 len_write);
  dest->bf_write.end += len_write;
  if (dest->bf_write.end >= (int)SIZE_BUFFER)
    dest->bf_write.end = 0;
  if (len_write != PACKET_SIZE)
    write_second_part_in_buffer(len_write, dest, packet_tmp);
  return (0);
}

int			msg_cmd(t_settings *settings, t_client *current)
{
  t_channel_connected	*tmp_ch;
  t_client_connected	*tmp;

  tmp_ch = current->channels_connected;
  while ((tmp_ch) && (strcmp(tmp_ch->channel->name,
			     current->packet->channel) != 0))
    tmp_ch = tmp_ch->next;
  if (!tmp_ch)
    return (error("Error: channel send not found."));
  tmp = tmp_ch->channel->clients_connected;
  while (tmp)
    {
      if (write_circular_buffer_msg(tmp->client, current) == -1)
	return (error("Error: write in buffer client for write failed."));
      FD_SET(tmp->client->socket, &settings->writefs);
      tmp = tmp->next;
    }
  return (0);
}
