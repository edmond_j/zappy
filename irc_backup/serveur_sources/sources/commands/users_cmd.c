/*
** users_cmd.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Sat Apr 27 14:56:08 2013 david olivier
** Last update Sun Apr 28 09:56:02 2013 david olivier
*/

#include		<string.h>
#include		"server.h"

int			write_circular_buff_users(t_client *dest,
						  t_type type,
						  char *buffer)
{
  t_packet		packet;
  char			packet_tmp[PACKET_SIZE];
  int			len_write;

  memset(&packet, 0, PACKET_SIZE);
  packet.type = type;
  packet.size = PACKET_SIZE;
  if (buffer)
    strcpy(packet.request, buffer);
  strcpy(packet.channel, dest->packet->channel);
  memcpy(&packet_tmp[0], &packet, PACKET_SIZE);
  len_write = (((dest->bf_write.end + PACKET_SIZE) >= SIZE_BUFFER) ?
	       (SIZE_BUFFER - dest->bf_write.end) : (PACKET_SIZE));
  memcpy(&dest->bf_write.buffer[dest->bf_write.end], &packet_tmp[0],
	 len_write);
  dest->bf_write.end += len_write;
  if (dest->bf_write.end >= (int)SIZE_BUFFER)
    dest->bf_write.end = 0;
  if (len_write != PACKET_SIZE)
    write_second_part_in_buffer(len_write, dest, packet_tmp);
  return (0);
}

t_client_connected	*get_client_current(t_channel *channel,
					    char *str)
{
  t_client_connected	*tmp;

  tmp = channel->clients_connected;
   if (strcmp(STRING_EMPTY, str) == 0)
    return (tmp);
  while ((tmp) && (strcmp(tmp->client->nick_name, str) != 0))
    tmp = tmp->next;
  if ((tmp) && (strcmp(tmp->client->nick_name, str) == 0))
    return (NULL);
  if ((!tmp) || (channel->clients_connected == tmp))
    return (tmp);
  return (tmp->next);
}

int			write_users_in_buff(t_client *current,
					    t_channel *channel,
					    char *buffer)
{
  int			len_tmp;
  int			len_write;
  t_client_connected	*tmp;

  if ((tmp = get_client_current(channel, current->packet->nick_name)) == NULL)
    return (0);
  len_write = 0;
  while (tmp)
    {
      len_tmp = strlen(tmp->client->nick_name) + 1;
      if ((len_write + len_tmp) < REQUEST_LEN)
	{
	  len_write += len_tmp;
	  strcat(buffer, tmp->client->nick_name);
	  buffer[len_write - 1] = '\n';
	}
      else
	tmp = NULL;
      if (tmp)
	tmp = tmp->next;
    }
  return (0);
}

int			users_cmd(t_settings *settings, t_client *current)
{
  char			buffer[REQUEST_LEN];
  t_channel		*channel;

  memset(buffer, 0, REQUEST_LEN);
  if ((channel = search_channel(settings, current->packet->channel)) == NULL)
    {
      if (write_circular_buff_users(current, USER_END, NULL) == -1)
	return (error("Error: write in circular buffer end user failed."));
      FD_SET(current->socket, &settings->writefs);
      return (0);
    }
  if (write_users_in_buff(current, channel, buffer) == -1)
    return (error("Error: write users in buffer."));
  if (write_circular_buff_users(current, (strcmp(STRING_EMPTY, buffer) == 0) ?
				(USER_END) : (USERS), buffer) == -1)
    return (error("Error: write in circular buffer user failed."));
  FD_SET(current->socket, &settings->writefs);
  return (0);
}
