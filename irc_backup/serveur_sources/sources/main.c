/*
** main.c for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/olivie_d
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Wed Apr 24 10:36:19 2013 david olivier
** Last update Wed Apr 24 10:57:44 2013 david olivier
*/

#include		<stdlib.h>
#include		<unistd.h>
#include		"server.h"

int			main(int ac, char **av)
{
  if (ac <= 2)
    {
      if ((ac == 2) && (is_port(av[1]) == false))
	return (-1);
      return (launch_server((ac == 2) ? (atoi(av[1])) : (PORT_DEFAULT)));
    }
  write(2, "USAGE: ./server [PORT]\n", 25);
  return (-1);
}
