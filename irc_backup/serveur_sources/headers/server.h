/*
** server.h for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j/headers/server
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Wed Apr 24 08:51:58 2013 david olivier
** Last update Sun Apr 28 15:29:17 2013 david olivier
*/

#ifndef				SERVER_H_
# define			SERVER_H_

# include			<stdio.h>
# include			<netdb.h>

# define			MAX_CLIENTS	10
# define			BASE_NICK_NAME	"client_"
# define			COMMAND_QUIT	"quit\n"
# define			STRING_EMPTY	""
# define			STD_CIN		0
# define			SOCKADDR_LEN	sizeof(struct sockaddr_in)
# define			SIZE_BUFFER	(PACKET_SIZE * 3)
# define			TIME_OUT	500000

# include			"common.h"

typedef enum
  {
    false,
    true
  }				bool;

typedef	struct s_channel_connected	t_channel_connected;

typedef struct			s_circular_buffer
{
  char				buffer[SIZE_BUFFER];
  short				start;
  short				end;
  size_t			read;
}				t_circular_buffer;

typedef struct			s_client
{
  char				nick_name[NAME_LEN];
  int				socket;
  char				buffer_tmp[PACKET_SIZE];
  t_circular_buffer		bf_read;
  t_circular_buffer		bf_write;
  size_t			read;
  t_status			status;
  struct s_channel_connected	*channels_connected;
  t_packet			*packet;
  struct s_client		*next;
}				t_client;

typedef struct			s_client_connected
{
  t_client			*client;
  struct s_client_connected	*next;
}				t_client_connected;

typedef struct			s_channel
{
  char				name[CHANNEL_LEN];
  t_client_connected		*clients_connected;
  struct s_channel		*next;
}				t_channel;

typedef struct			s_channel_connected
{
  t_channel			*channel;
  struct s_channel_connected	*next;
}				t_channel_connected;

typedef struct			s_settings
{
  FILE				*std_cin;
  int				port;
  bool				quit;
  int				socket;
  fd_set			readfs;
  fd_set			writefs;
  struct sockaddr_in		inf_net;
  t_client			*clients;
  t_channel			*channels;
}				t_settings;

typedef				int(*t_ptrfunc)(t_settings *, t_client *);

/* sources/manager/server.c */
int				start_server(t_settings *settings);
int				launch_server(int port);

/* sources/manager/quit_server.c */
void				free_channels(t_settings *settings);
void				free_clients(t_settings *settings);
void				quit_server(t_settings *settings);
bool				is_not_invalid_char(char *str);
int				check_is_quit(t_settings *settings);

/* sources/manager/is_port.c */
bool				is_port(char *str);

/* sources/manager/search_channel.c */
t_channel			*search_channel(t_settings *settings,
						char *name_channel);

/* sources/manager/write_all_chan_quit.c */
int				write_circular_buff_part_all(t_client *dest,
							     t_client *src,
							     char *buffer);
void				write_all_chan_quit(char *name_chan,
						    int *len_write,
						    char *buffer);

/* sources/manager/resorting_clients.c */
int				sort_channel(t_channel **channel,
					     t_client_connected *client);
int				resort_in_channel(t_channel **channel,
						  char *str);
int				resort_users(t_settings *settings,
					     t_client *current);

/* sources/manager/reset_name_ch_tmp.c */
void				reset_name_ch_tmp(char *buffer,
						  t_channel *channel);

/* sources/manager/manage_client.c */
t_client			*get_prev_client(t_client *clients);
int				delete_client(t_client *current,
					      t_client *prev,
					      t_settings *settings);
int				disconnect_client(t_settings *settings,
						  int tmp_sock);
int				add_client(t_settings *settings, int tmp);
int				connect_client(t_settings *settings);

/* sources/manager/manage_channel.c */
t_client_connected		*get_prev_client_co(t_client_connected *client,
						    t_client *current);
t_client_connected		*init_new_client(t_client *current,
						 t_channel *channel);
int				add_client_in_channel(t_channel *channel,
						      t_client *current);
t_channel			*get_prev_channel(t_channel *channels,
						  char *str);
int				create_channel(t_settings *settings,
					       t_client *current);

/* sources/transmissions/manage_transmission.c */
int				manage_transmission_client_read(t_settings
								*settings);
int				manage_transmission_client_write(t_settings
								 *settings);
int				manage_transmission(t_settings *settings);

/* sources/transmissions/transmission_client_read.c */
int				write_in_packet(t_client *current);
int				read_socket(t_client *current);
int				transmission_client_read(t_settings *settings,
							 t_client *current);

/* sources/transmissions/write_circular_buffer.c */
void				write_second_part_in_buffer(int len_write,
							    t_client *client,
							    char *packet_tmp);
int				write_circular_buffer(t_client *client,
						      char *str,
						      t_type type,
						      char *nick_name);

/* sources/transmissions/transmission_client_write.c */
int				sending_one_packet(t_client *current);
int				transmission_client_write(t_settings *settings,
							  t_client *current);

/* sources/network/init_network.c */
int				init_network(t_settings *settings);

/* sources/network/manage_socket.c */
void				reset_socket(t_settings *settings);
int				get_max_socket(t_settings *settings);

/* sources/utils/error_funcs.c */
int				error(char *str);
int				errorcs(char *str, int socket);
int				errorf(char *str, FILE *file);

/* sources/commands/manage_commands.c */
int				manage_commands(t_settings *settings,
						t_client *current);

/* sources/commands/protocol.c */
bool				is_exist(char *new_name, t_settings *settings);
int				get_new_name_for_client(t_settings *settings,
							t_client *current);
int				protocol(t_settings *settings,
					 t_client *current);

/* sources/commands/join_cmd.c */
int				write_circular_buff_status(t_client *dest,
							   t_client *src,
							   t_type type,
							   t_channel *channel);
int				write_status_in_channel(t_channel *channel,
							t_client *client,
							t_settings *settings,
							t_type type);
t_channel_connected		*get_prev_ch_co(t_channel_connected *channels);
int				add_reference_ch_connected(t_channel *channel,
							   t_client *client);
int				join_cmd(t_settings *settings,
					 t_client *current);

/* sources/commands/part_cmd.c */
int				update_channels(t_settings *settings);
int				remove_client(t_channel *channel,
					      t_settings *settings,
					      t_client *current);
int				sending_part_clients(char *buffer,
						     t_settings *settings,
						     t_client *current);
int				remove_client_channel(t_settings *settings,
						      t_client *current);
int				part_cmd(t_settings *settings,
					 t_client *current);

/* sources/commands/msg_cmd.c */
int				write_circular_buffer_msg(t_client *dest,
							  t_client *src);
int				msg_cmd(t_settings *settings,
					t_client *current);

/* sources/commands/nick_cmd.c */
int				write_circular_buff_name(t_client *dest,
							 t_client *src,
							 char *old_name,
							 t_channel *channel);
int				send_without_channel(t_settings *settings,
						     t_client *current,
						     char *old_name);
int				send_new_name_in_channels(t_settings *settings,
							  t_client *current,
							  char *old_name);
int				nick_cmd(t_settings *settings,
					 t_client *current);

/* sources/commands/list_cmd.c */
int				write_circular_buff_list(t_client *dest,
							 t_type type,
							 char *buffer);
int				write_list_in_buff(t_client *client,
						   t_channel *channel,
						   char *buffer);
t_channel			*get_current_channel(t_settings *settings,
						     char *str,
						     t_client *current);
int				list_cmd(t_settings *settings,
					 t_client *current);

/* sources/commands/users_cmd.c */
int				write_circular_buff_users(t_client *dest,
							  t_type type,
							  char *buffer);
t_client_connected		*get_client_current(t_channel *channel,
						    char *str);
int				write_users_in_buff(t_client *current,
						    t_channel *channel,
						    char *buffer);
int				users_cmd(t_settings *settings,
					  t_client *current);

/* sources/commands/private_cmd.c */
int				write_circular_buff_private(t_client *dest,
							    t_client *src,
							    t_type type);
int				private_cmd(t_settings *settings,
					    t_client *current);

/* sources/commands/send_cmd.c */
int				write_circular_buff_file(t_client *dest,
							 t_client *src,
							 char *str,
							 t_type type);
int				send_cmd(t_settings *settings,
					 t_client *current);

/* sources/commands/accept_cmd.c */
int				accept_cmd(t_settings *settings,
					    t_client *current);

/* sources/commands/refuse_cmd.c */
int				refuse_cmd(t_settings *settings,
					    t_client *current);

#endif				/* !SERVER_H_ */
