/*
** common.h for my_irc in /home/olivie_d//depotsvn/myirc-2016-edmond_j
**
** Made by david olivier
** Login   <olivie_d@epitech.net>
**
** Started on  Wed Apr 24 08:26:54 2013 david olivier
** Last update Sun Apr 28 15:30:33 2013 david olivier
*/

#ifndef			COMMON_H_
# define		COMMON_H_

# define		CHANNEL_LEN	32
# define		NAME_LEN	256
# define		REQUEST_LEN	1024
# define		PORT_DEFAULT	6665
# define		SECU_ONE	"Parce que"
# define		SECU_TWO	"Toulon"

typedef enum
  {
    UNIDENTIFIED,
    IDENTIFIED
  }			t_status;

typedef enum
  {
    PROTOCOL,
    JOIN,
    PART,
    MSG,
    NICK,
    LIST,
    USERS,
    PRIVATE,
    SEND_FILE,
    ACCEPT_FILE,
    REFUSE_FILE,
    MAX_READ_VALUE,
    END_LIST,
    USER_END,
    PRIVATE_ERR,
    ABORT_FILE
  }			t_type;

typedef struct		s_packet
{
  t_type		type;
  char			channel[CHANNEL_LEN];
  char			nick_name[NAME_LEN];
  size_t		size;
  char			request[REQUEST_LEN];
}			t_packet;

# define		PACKET_SIZE	sizeof(t_packet)

#endif			/* !COMMON_H_ */
