//
// IrcClient.cpp for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j/my_irc
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Wed Apr 24 15:09:57 2013 julien edmond
// Last update Thu Apr 25 14:02:25 2013 julien edmond
//

#include	"IrcClient.hh"
#include	"MainWindow.hh"

IrcClient::IrcClient(int ac, char **av)
  : app(ac, av)
{}

IrcClient::~IrcClient()
{}

int		IrcClient::start()
{
  MainWindow	win;

  win.show();
  while (win.isVisible())
    {
      win.communicate();
      app.processEvents();
    }
  return (0);
}
