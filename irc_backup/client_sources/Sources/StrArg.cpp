//
// StrArg.cpp<2> for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sat Apr 27 15:08:36 2013 julien edmond
// Last update Sat Apr 27 15:59:52 2013 julien edmond
//

#include	<algorithm>
#include	"StrArg.hh"

StrArg::StrArg()
{}

StrArg::StrArg(const StrArg& str)
  : std::string(str)
{}

StrArg::StrArg(const StrArg& str, size_t pos, size_t len)
  : std::string(str, pos, len)
{}

StrArg::StrArg(const char* s)
  : std::string(s)
{}

StrArg::StrArg(const char* s, size_t n)
  : std::string(s, n)
{}

StrArg::StrArg(size_t n, char c)
  : std::string(n, c)
{}

StrArg&	StrArg::operator=(const StrArg& other)
{
  if (this != &other)
    std::string::operator=(other);
  return (*this);
}

StrArg::~StrArg()
{}

std::istream&	operator>>(std::istream& stream, StrArg& str)
{
  std::string	tmp;
  char		c(0);
  bool		isOpen(false);
  int		nbQuotes;

  str.clear();
  while (stream && (isOpen || tmp.empty()))
    {
      stream >> tmp;
      str += tmp;
      if (std::count(tmp.begin(), tmp.end(), '"') % 2)
	isOpen = !isOpen;
      if (isOpen)
	{
	  while (stream.get(c) && (c == ' ' || c == '\t'))
	    {
	      str += c;
	      c = 0;
	    }
	  if (c)
	    stream.unget();
	}
    }
  nbQuotes = std::count(str.begin(), str.end(), '"');
  std::remove(str.begin(), str.end(), '"');
  str.resize(str.size() - nbQuotes);
  return (stream);
}
