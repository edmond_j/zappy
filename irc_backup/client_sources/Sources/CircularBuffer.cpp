//
// CircularBuffer.cpp for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Wed Apr 24 21:17:07 2013 julien edmond
// Last update Wed Apr 24 21:57:11 2013 julien edmond
//

#include	"CircularBuffer.hpp"

CircularBuffer::CircularBuffer(int size)
  : _size(size), _start(0), _end(0), _buff(new char [size])
{}

CircularBuffer::CircularBuffer(const CircularBuffer& other)
  : _size(other._size), _start(other._start), _end(other._end),
    _buff(new char [_size])
{
  memcpy(_buff, other._buff, sizeof(char) * _size);
}

CircularBuffer& CircularBuffer::operator=(const CircularBuffer& other)
{
  if (this != &other)
    {
      delete _buff;
      _size = other._size;
      _start = other._start;
      _end = other._end;
      _buff = new char [_size];
      memcpy(_buff, other._buff, sizeof(char) * _size);
    }
  return (*this);
}

CircularBuffer::~CircularBuffer()
{
  delete _buff;
}

int	CircularBuffer::getReadable() const
{
  int	ret(_end - _start);

  if (ret < 0)
    ret += _size;
  return (ret);
}

int	CircularBuffer::getSize() const
{
  return (_size);
}

