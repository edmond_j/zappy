//
// main.cpp for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j/my_irc
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Wed Apr 24 15:03:40 2013 julien edmond
// Last update Wed Apr 24 15:09:38 2013 julien edmond
//

#include    "IrcClient.hh"

int		main(int ac, char **av)
{
  IrcClient	client(ac, av);

  return (client.start());
}
