//
// TCPSocket.cpp for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Wed Apr 24 21:40:35 2013 julien edmond
// Last update Sun Apr 28 09:57:25 2013 julien edmond
//

#include	<sys/select.h>
#include	<sys/time.h>
#include	<sys/types.h>
#include	<unistd.h>
#include	<iostream>
#include	<netdb.h>
#include	<sys/socket.h>
#include	"Errno/Errno.hh"
#include	"TCPSocket.hpp"
#include	"common.hh"

TCPSocket::TCPSocket(const char* host, int port)
  : _isConnected(false), _isReadable(false), _isWritable(false),
    _socket(-1), _host(host), _port(port)
{
  struct hostent*	hostinfo;
  struct sockaddr_in	sin;

  memset(&sin, 0, sizeof(sin));
  if ((_socket = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    _connectError = LibC::Errno::getStrError();
  else if (!(hostinfo = gethostbyname(host)))
    _connectError = hstrerror(h_errno);
  else
    {
      sin.sin_addr = *reinterpret_cast<struct in_addr*>(hostinfo->h_addr);
      sin.sin_port = htons(port);
      sin.sin_family = AF_INET;
      if (connect(_socket, reinterpret_cast<struct sockaddr*>(&sin),
		  sizeof(struct sockaddr)) == -1)
	_connectError = LibC::Errno::getStrError();
      else
	{
	  _isConnected = true;
	  _connectError = "Success";
	}
    }
}

TCPSocket::~TCPSocket()
{
  if (_socket != -1 && close(_socket) == -1)
    std::cerr << "close: " << LibC::Errno::getStrError() << std::endl;
}

void			TCPSocket::refresh()
{
  fd_set		rfds;
  fd_set		wfds;
  struct timeval	timeout;

  if (_isConnected)
    {
      timeout.tv_sec = 0;
      timeout.tv_usec = 500;
      FD_ZERO(&rfds);
      FD_ZERO(&wfds);
      FD_SET(_socket, &rfds);
      if (this->_output.getReadable())
	FD_SET(_socket, &wfds);
      if (select(_socket + 1, &rfds, &wfds, NULL, &timeout) == -1)
	std::cerr << "Select: " << LibC::Errno::getStrError() << std::endl;
      if ((_isReadable = FD_ISSET(_socket, &rfds)))
	this->readFromServ();
      if ((_isWritable = FD_ISSET(_socket, &wfds)))
	this->writeToServ();
    }
}

bool	TCPSocket::isConnected() const
{
  return (_isConnected);
}

const char*	TCPSocket::getConnectError() const
{
  return (_connectError.c_str());
}

int	TCPSocket::getPort() const
{
  return (_port);
}

const char*	TCPSocket::getHost() const
{
  return (_host.c_str());
}

int	TCPSocket::readFromServ()
{
  int	ret(0);
  char	buff[PACKET_SIZE];

  if (_isConnected && _isReadable)
    {
      if (!(ret = read(_socket, buff, sizeof(buff))))
	_isConnected = false;
      else if (ret == -1)
	{
	  _isConnected = false;
	  LibC::Errno::printError("Read from serv");
	}
      else
	_input.write(buff, ret);
    }
  return (ret);
}

int	TCPSocket::writeToServ()
{
  char	buff[PACKET_SIZE];
  int	ret(0);
  int	size;

  if (_isConnected && _isWritable)
    {
      size = std::min<unsigned>(sizeof(buff), _output.getReadable());
      _output.read(buff, size);
      if ((ret = write(_socket, buff, size)) == -1)
	{
	  _isConnected = false;
	  LibC::Errno::printError("Write to serv");
	}
    }
  return (ret);
}
