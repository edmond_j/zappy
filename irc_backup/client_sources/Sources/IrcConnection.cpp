//
// IrcConnection.cpp for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j/Sources
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Wed Apr 24 20:10:40 2013 julien edmond
// Last update Sun Apr 28 14:15:42 2013 julien edmond
//

#include	<sys/socket.h>
#include	<sstream>
#include	"IrcConnection.hh"

IrcConnection::IrcConnection(const char* hostname, int port,
			     const char* nickname)
  : TCPSocket(hostname, port), _nickname(nickname),
    _isLogged(false)
{
  this->sendCommand(PROTOCOL, "", SECU_ONE, sizeof(SECU_ONE));
  this->_actions[PROTOCOL] = &IrcConnection::checkProto;
  this->_actions[MSG] = &IrcConnection::receiveMsg;
  this->_actions[JOIN] = &IrcConnection::someoneConnected;
  this->_actions[PART] = &IrcConnection::someoneDisconnected;
  this->_actions[NICK] = &IrcConnection::nickChanged;
  this->_actions[PRIVATE] = &IrcConnection::privateMsg;
  this->_actions[PRIVATE_ERR] = &IrcConnection::notConnected;
  this->_actions[LIST] = &IrcConnection::listChan;
  this->_actions[END_LIST] = &IrcConnection::endList;
  this->_actions[USERS] = &IrcConnection::user;
  this->_actions[USER_END] = &IrcConnection::endUser;
  this->_timer.start(2000);
  QObject::connect(&this->_timer, SIGNAL(timeout()),
		   this, SLOT(checkLogged()));
}

IrcConnection::~IrcConnection()
{}

bool	IrcConnection::isLogged() const
{
  return (_isLogged);
}

const char*	IrcConnection::getNickname() const
{
  return (this->_nickname.c_str());
}

void			IrcConnection::dealWithInputs()
{
  t_packet		packet;
  ActionMap::iterator	it;

  while (*this >> packet)
    {
      if ((it = this->_actions.find(packet.type)) != this->_actions.end())
	(this->*(it->second))(&packet);
    }
}

void	IrcConnection::sendCommand(t_type type, const char* channel,
				   const char* data, int size,
				   const char* nickname)
{
  t_packet	packet;

  if (!nickname)
    nickname = _nickname.c_str();
  bzero(&packet, sizeof(packet));
  packet.type = type;
  strncpy(packet.channel, channel, CHANNEL_LEN - 1);
  strncpy(packet.nick_name, nickname, NAME_LEN - 1);
  packet.size = size;
  memcpy(packet.request, data, std::min(size, REQUEST_LEN - 1));
  *this << packet;
}

void	IrcConnection::askList(const char* pattern)
{
  _pattern = pattern;
  this->sendCommand(LIST, "", pattern, _pattern.size());
}

void	IrcConnection::askUser(const char* channel)
{
  this->sendCommand(USERS, channel, "", 0,
		    "");
}

void	IrcConnection::checkLogged()
{
  if (!this->_isLogged)
    {
      this->_isConnected = false;
    }
}

void	IrcConnection::checkProto(const t_packet* packet)
{
  if (std::string(packet->request) == SECU_TWO)
    {
      _isLogged = true;
      _nickname = packet->nick_name;
      emit nickNameChanged(packet->nick_name);
    }
}

void	IrcConnection::receiveMsg(const t_packet* packet)
{
  emit msgReceived(packet->channel, packet->nick_name, packet->request);
}

void	IrcConnection::someoneConnected(const t_packet* packet)
{
  emit connection(packet->channel, packet->nick_name);
}

void			IrcConnection::someoneDisconnected(const t_packet* p)
{
  std::istringstream	stream(p->request);
  std::string		channel;

  emit disconnection(p->channel, p->nick_name);
  while (std::getline(stream, channel))
    emit disconnection(channel.c_str(), p->nick_name);
}

void	IrcConnection::nickChanged(const t_packet* packet)
{
  if (_nickname == packet->nick_name)
    {
      _nickname = packet->request;
      emit nickNameChanged(packet->request);
    }
  emit nickChanged(packet->channel, packet->nick_name, packet->request);
}

void	IrcConnection::privateMsg(const t_packet* packet)
{
  emit mpReceived(packet->nick_name, packet->request);
}

void	IrcConnection::notConnected(const t_packet* packet)
{
  emit notConnected(packet->nick_name);
}

void	IrcConnection::listChan(const t_packet* packet)
{
  std::istringstream	stream(packet->request);
  std::string		channel;
  std::string		lastChannel;

  while (std::getline(stream, channel))
    {
      lastChannel = channel;
      emit listChan(channel.c_str());
    }
  if (!lastChannel.empty())
    this->sendCommand(LIST, lastChannel.c_str(), _pattern.c_str(),
		      _pattern.size());
}

void	IrcConnection::endList(const t_packet*)
{
  emit endList();
}

void	IrcConnection::user(const t_packet* packet)
{
  std::istringstream	stream(packet->request);
  std::string		nick;
  std::string		lastNick;

  while (std::getline(stream, nick))
    {
      lastNick = nick;
      emit user(packet->channel, nick.c_str());
    }
  if (!lastNick.empty())
    this->sendCommand(USERS, packet->channel, "", 0, lastNick.c_str());
}

void	IrcConnection::endUser(const t_packet*)
{
  emit endUser();
}
