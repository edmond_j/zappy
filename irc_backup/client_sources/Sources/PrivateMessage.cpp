//
// PrivateMessage.cpp for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Fri Apr 26 01:54:29 2013 julien edmond
// Last update Sun Apr 28 14:44:43 2013 julien edmond
//

#include	<QTime>
#include	"PrivateMessage.hh"
#include	"ui_PrivateMessage.h"
#include	"MainWindow.hh"

PrivateMessage::PrivateMessage(MainWindow *parent)
  : ui(new Ui::PrivateMessage)
{
  ui->setupUi(this);
  if (parent)
    QObject::connect(this, SIGNAL(sendMsg(const QString&, const QString&)),
		     parent, SLOT(sendMP(const QString&, const QString&)));
  QObject::connect(this->ui->InputLine, SIGNAL(returnPressed()),
		   this, SLOT(msgSender()));
  QObject::connect(this->ui->SendButton, SIGNAL(clicked()),
		   this, SLOT(msgSender()));
}

PrivateMessage::PrivateMessage(const QString& to, MainWindow *parent)
  : ui(new Ui::PrivateMessage), _to(to)
{
  ui->setupUi(this);
  this->setWindowTitle(to);
  this->ui->NickLabel->setText(to);
  if (parent)
    QObject::connect(this, SIGNAL(sendMsg(const QString&, const QString&)),
		     parent, SLOT(sendMP(const QString&, const QString&)));
  QObject::connect(this->ui->InputLine, SIGNAL(returnPressed()),
		   this, SLOT(msgSender()));
  QObject::connect(this->ui->SendButton, SIGNAL(clicked()),
		   this, SLOT(msgSender()));
}

PrivateMessage::~PrivateMessage()
{
  delete ui;
}

void	PrivateMessage::msgSender()
{
  if (this->ui->InputLine->text().size())
    emit sendMsg(_to, this->ui->InputLine->text());
  this->ui->InputLine->clear();
}

void	PrivateMessage::notConnected()
{
  MainWindow::appendColorText(this->ui->Messages, _to + " is not online",
			      "red");
}

void	PrivateMessage::receiveMessage(const QString& from,
				       const QString& msg)
{
  this->show();
  this->setFocus();
  ui->Messages->appendPlainText(QTime::currentTime().toString("[hh:mm] ")
				+ from + ": " + msg);
}
