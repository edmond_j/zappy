//
// ConnectWindow.cpp for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j/my_irc
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Wed Apr 24 15:03:50 2013 julien edmond
// Last update Wed Apr 24 16:35:04 2013 julien edmond
//

#include	"ConnectWindow.hh"
#include	"ui_ConnectWindow.h"
#include	"common.hh"

ConnectWindow::ConnectWindow(QWidget *parent)
  : QDialog(parent),
    ui(new Ui::ConnectWindow)
{
  setModal(true);
  ui->setupUi(this);
  ui->PortSelector->setValue(PORT_DEFAULT);
  ui->NickNameEdit->insert(getenv("USER"));
  if (parent)
    connectToParent(parent);
}

void	ConnectWindow::connectToParent(const QWidget *parent) const
{
  QObject::connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(connect()));
  QObject::connect(this, SIGNAL(connect(const QString&, int, const QString&)),
		   parent,
		   SLOT(connectTo(const QString&, int, const QString&)));
}

void	ConnectWindow::connect()
{
  emit connect(ui->Hostname->text(), ui->PortSelector->value(),
	       ui->NickNameEdit->text());
}

ConnectWindow::~ConnectWindow()
{
  delete ui;
}
