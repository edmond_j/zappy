//
// PrivateMsgContainer.cpp for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sat Apr 27 16:38:36 2013 julien edmond
// Last update Sat Apr 27 18:22:33 2013 julien edmond
//

#include	<algorithm>
#include	"PrivateMsgContainer.hh"

PrivateMsgContainer::PrivateMsgContainer(MainWindow* parent)
  : _parent(parent)
{}

PrivateMsgContainer::~PrivateMsgContainer()
{
  std::for_each(this->begin(), this->end(), &this->deleter);
}

PrivateMessage*&	PrivateMsgContainer::operator[](const QString& name)
{
  PrivateMsgContainer::iterator	it(this->find(name));

  if (it == this->end())
    std::map<const QString, PrivateMessage*>::operator[](name)
      = new PrivateMessage(name, _parent);
  std::map<const QString, PrivateMessage*>::operator[](name)->show();
  return (std::map<const QString, PrivateMessage*>::operator[](name));
}

void	PrivateMsgContainer::clear()
{
  std::for_each(this->begin(), this->end(), &this->deleter);
  std::map<const QString, PrivateMessage*>::clear();
}

void	PrivateMsgContainer::deleter(std::pair<const QString,
					       PrivateMessage*>& p)
{
  delete p.second;
}
