//
// MainWindow.cpp for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j/my_irc
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Wed Apr 24 15:03:24 2013 julien edmond
// Last update Sun Apr 28 15:01:45 2013 julien edmond
//

#include	<algorithm>
#include	<cctype>
#include	<QInputDialog>
#include	"ChannelWidget.hh"
#include	"Tools.hpp"
#include	"MainWindow.hh"
#include	"ui_MainWindow.h"
#include	"Sleep/Sleep.hh"
#include	"StrArg.hh"

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent),
    ui(new Ui::MainWindow),
    _isLogged(false),
    _connectWin(this),
    _connection(NULL),
    _privateMsg(this),
    _askingList(false),
    _channelList(NULL),
    _refreshList(false),
    _askingUser(false),
    _refreshUser(false)
{
  ui->setupUi(this);
  this->ui->tabWidget->setTabsClosable(true);
  connector();
  _commands["/server"] = &MainWindow::server;
  _commands["/join"] = &MainWindow::join;
  _commands["/part"] = &MainWindow::part;
  _commands["/nick"] = &MainWindow::nick;
  _commands["/msg"] = &MainWindow::privateMsg;
  _commands["/list"] = &MainWindow::list;
  _commands["/users"] = &MainWindow::user;
  this->ui->ChannelList->setSelectionMode(QListWidget::NoSelection);
  this->ui->userList->setSelectionMode(QListWidget::NoSelection);
}

MainWindow::~MainWindow()
{
  delete ui;
  delete _connection;
}

void	MainWindow::connector() const
{
  QObject::connect(ui->ExitAction, SIGNAL(triggered()),
		   this, SLOT(close()));
  QObject::connect(ui->ConnectToAction, SIGNAL(triggered()),
		   &_connectWin, SLOT(show()));
  QObject::connect(this->ui->DisconnectAction, SIGNAL(triggered()),
		   this, SLOT(disconnect()));
  QObject::connect(this->ui->SendButton, SIGNAL(clicked()),
		   this, SLOT(sendCmd()));
  QObject::connect(this->ui->InputLine, SIGNAL(returnPressed()),
		   this, SLOT(sendCmd()));
  QObject::connect(this->ui->tabWidget, SIGNAL(tabCloseRequested(int)),
		   this, SLOT(tabRemover(int)));
  QObject::connect(this->ui->JoinChannelButton, SIGNAL(clicked()),
		   this, SLOT(joinChannel()));
  QObject::connect(this->ui->NicknameLabel, SIGNAL(doubleClicked()),
		   this, SLOT(changeNickname()));
  QObject::connect(&this->_timerList, SIGNAL(timeout()),
		   this, SLOT(refreshList()));
  QObject::connect(this->ui->tabWidget, SIGNAL(currentChanged(int)),
		   this, SLOT(refreshUser(int)));
  QObject::connect(this->ui->ChannelList,
		   SIGNAL(itemDoubleClicked(QListWidgetItem *)),
		   this, SLOT(joinChannel(QListWidgetItem *)));
  QObject::connect(this->ui->userList,
		   SIGNAL(itemDoubleClicked(QListWidgetItem *)),
		   this, SLOT(openMP(QListWidgetItem *)));
}

void	MainWindow::connectConnection() const
{
  QObject::connect(this->_connection, SIGNAL(nickNameChanged(const QString&)),
		   this, SLOT(changeNickname(const QString&)));
  QObject::connect(this->_connection, SIGNAL(msgReceived(const QString&,
							 const QString&,
							 const QString&)),
		   this, SLOT(receiveMsg(const QString&, const QString&,
					 const QString&)));
  QObject::connect(this->_connection, SIGNAL(connection(const QString&,
							const QString&)),
		   this, SLOT(connection(const QString&, const QString&)));
  QObject::connect(this->_connection, SIGNAL(disconnection(const QString&,
							   const QString&)),
		   this, SLOT(disconnection(const QString&, const QString&)));
  QObject::connect(this->_connection, SIGNAL(nickChanged(const QString&,
							 const QString&,
							 const QString&)),
		   this, SLOT(nickChanged(const QString&, const QString&,
					  const QString&)));
  QObject::connect(this->_connection, SIGNAL(mpReceived(const QString&,
							const QString&)),
		   this, SLOT(receiveMP(const QString&, const QString&)));
  QObject::connect(this->_connection, SIGNAL(notConnected(const QString&)),
		   this, SLOT(notConnected(const QString&)));
  QObject::connect(this->_connection, SIGNAL(listChan(const QString&)),
		   this, SLOT(printList(const QString&)));
  QObject::connect(this->_connection, SIGNAL(endList()),
		   this, SLOT(endList()));
  QObject::connect(this->_connection, SIGNAL(user(const QString&,
						  const QString&)),
		   this, SLOT(user(const QString&, const QString&)));
  QObject::connect(this->_connection, SIGNAL(endUser()),
		   this, SLOT(userEnd()));
}

void	MainWindow::connectTo(const QString& server, int port,
			      const QString& nickName)
{
  this->disconnect();
  _connection = new IrcConnection(server.toStdString().c_str(), port,
				  nickName.toStdString().c_str());
  if (!_connection->isConnected())
    {
      this->printToMain(QString("Connection failed: ")
			+ QString(_connection->getConnectError()));
      delete _connection;
      _connection = NULL;
    }
  else
    {
      this->connectConnection();
      this->changeNickname(this->_connection->getNickname());
      this->printToMain(QString("Connected to ") + _connection->getHost());
    }
}

void	MainWindow::disconnect()
{
  _isLogged = false;
  if (_connection)
    {
      this->printToMain(QString("Disconnected from ")
			+ _connection->getHost());
      delete _connection;
      _connection = NULL;
      while (this->ui->tabWidget->count() > 1)
      	this->ui->tabWidget->removeTab(1);
      _privateMsg.clear();
      this->_askingList = false;
      this->_channelList = NULL;
      this->_refreshList = false;
    }
}

void		MainWindow::tabRemover(int idx)
{
  std::string	channel;

  if (idx > 0)
    {
      if (this->_connection && this->_connection->isLogged())
	{
	  channel = this->ui->tabWidget->tabText(idx).toStdString();
	  this->_connection->sendCommand(PART, channel.c_str(), "", 0);
	}
      this->ui->tabWidget->removeTab(idx);
    }
}

void		MainWindow::changeNickname()
{
  if (this->_connection && this->_connection->isLogged())
    {
      bool	ok(false);
      QString	nick(QInputDialog::getText(this, "Change your nickname...",
					   "New nickname:",
					   QLineEdit::Normal, "", &ok));

      if (ok)
	this->_connection->sendCommand(NICK, "", nick.toStdString().c_str(),
				       nick.size());
    }
}

void	MainWindow::changeNickname(const QString& new_nick)
{
  this->ui->NicknameLabel->setText(new_nick + ":");
}

void	MainWindow::joinChannel()
{
  if (this->_connection && this->_connection->isLogged())
    {
      bool	ok(false);
      QString	channel(QInputDialog::getText(this, "Join channel...",
					      "Channel name:",
					      QLineEdit::Normal, "", &ok));

      if (ok)
	this->joinChannel(channel.toStdString().c_str());
    }
  else
    this->printToMain("You must be logged to a server to perform this action");
}

void	MainWindow::joinChannel(QListWidgetItem* item)
{
  this->joinChannel(item->text().toStdString().c_str());
}

void	MainWindow::show()
{
  QWidget::show();
  _connectWin.show();
}

void				MainWindow::sendCmd()
{
  if (this->ui->InputLine->text().size())
    {
      CmdMap::const_iterator	it;
      CmdMap::const_iterator	end(_commands.end());
      std::istringstream	stream(ui->InputLine->text().toStdString());
      std::string		word;

      stream >> word;
      it = _commands.begin();
      while (it != end && word != it->first)
	++it;
      if (it != end)
	(this->*(it->second))(stream);
      else if (this->ui->tabWidget->currentIndex() > 0)
	this->sendMsg();
      else
	this->printToMain("You must speak only inside a channel");
      this->ui->InputLine->clear();
    }
}

void			MainWindow::sendMsg()
{
  ChannelWidget		*chanWid;
  const char*		channel;
  const char*		data;
  int			size;

  if (this->_connection && this->_connection->isLogged()
      && (chanWid =
	  dynamic_cast<ChannelWidget*>(this->ui->tabWidget->currentWidget())))
    {
      channel = chanWid->getChannel().c_str();
      data = this->ui->InputLine->text().toStdString().c_str();
      size = this->ui->InputLine->text().size();
      this->_connection->sendCommand(MSG, channel, data, size);
    }
}

void	MainWindow::communicate()
{
  if (this->_connection)
    {
      this->_connection->refresh();
      if (!this->_isLogged && (this->_isLogged = _connection->isLogged()))
	{
	  printToMain(QString("Logged to the server with nickname: ")
		      + this->_connection->getNickname());
	  this->refreshList();
	}
      if (!this->_connection->isConnected())
	{
	  if (this->_connection->isLogged())
	    this->printToMain("Server disconnected");
	  else
	    this->printToMain("Server isn't a correct host");
	  this->disconnect();
	}
      else
	this->_connection->dealWithInputs();
    }
  LibC::Sleep::msleep(1);
}

void	MainWindow::printToMain(const QString& str)
{
  this->ui->MainText->appendPlainText(str);
  this->ui->tabWidget->setCurrentIndex(0);
}

void		MainWindow::join(std::istringstream& stream)
{
  StrArg	channel;

  if (this->_connection && this->_connection->isLogged())
    {
      if (!(stream >> channel))
	this->printToMain("Usage: /join __channel__");
      else
	this->joinChannel(channel.c_str());
    }
  else
    this->printToMain("You need to be connected to perform this action");
}

void		MainWindow::joinChannel(const char* name)
{
  int		i(0);
  QWidget*	tab;
  std::string	channel(name);

  if (std::find_if(channel.begin(), channel.end(), isalnum) != channel.end())
    {
      if (channel.size() >= CHANNEL_LEN)
	channel.resize(CHANNEL_LEN - 1);
      while (++i < this->ui->tabWidget->count()
	     && this->ui->tabWidget->tabText(i) != channel.c_str())
	;
      if (i >= this->ui->tabWidget->count())
	{
	  tab = new ChannelWidget(channel, this);
	  this->ui->tabWidget->addTab(tab, channel.c_str());
	  _connection->sendCommand(JOIN, channel.c_str(), "", 0);
	}
      else
	tab = this->ui->tabWidget->widget(i);
      this->ui->tabWidget->setCurrentWidget(tab);
      this->refreshUser();
    }
}

void		MainWindow::server(std::istringstream& stream)
{
  std::string	serv;
  int		port(PORT_DEFAULT);
  QString	nick(this->_connection
		     ? this->_connection->getNickname()
		     : getenv("USER"));
  StrArg	word;

  if (!(stream >> word))
    this->printToMain("Usage: /server __host__[:__port__]");
  else
    {
      std::istringstream	parser(word);

      std::getline(parser, serv, ':');
      if (parser)
	parser >> port;
      this->connectTo(serv.c_str(), port, nick);
    }
}

void			MainWindow::part(std::istringstream& stream)
{
  StrArg		channel;
  ChannelWidget*	chanWid;

  chanWid = dynamic_cast<ChannelWidget*>(this->ui->tabWidget->currentWidget());
  if (this->_connection && this->_connection->isLogged())
    {
      if (!(stream >> channel))
	{
	  if (this->ui->tabWidget->currentIndex() > 0 && chanWid)
	    this->leaveChannel(chanWid->getChannel().c_str());
	  else
	    this->printToMain("Usage: /part __channel__");
	}
      else
	this->leaveChannel(channel.c_str());
    }
  else
    this->printToMain("You need to be connected to perform this action");
}

void		MainWindow::nick(std::istringstream& stream)
{
  StrArg	newNick;

  if (this->_connection && this->_connection->isLogged())
    {
      if ((stream >> newNick))
	this->_connection->sendCommand(NICK, "", newNick.c_str(),
				       newNick.size());
      else
	this->printToMain("Usage: /nick __nickname__");
    }
  else
    this->printToMain("You need to be connected to perform this action");
  this->refreshUser();
}

void		MainWindow::privateMsg(std::istringstream& stream)
{
  StrArg	to;
  int		nbSpace(0);
  std::string	msg;

  if (this->_connection && this->_connection->isLogged())
    {
      if ((stream >> to) && std::getline(stream, msg))
	{
	  while (msg[nbSpace] == ' ' || msg[nbSpace] == '\t')
	    ++nbSpace;
	  if (nbSpace)
	    msg = msg.substr(nbSpace);
	  if (to.size() >= NAME_LEN)
	    to.resize(NAME_LEN - 1);
	  if (msg.size() >= REQUEST_LEN)
	    msg.resize(REQUEST_LEN - 1);
	  this->sendMP(to.c_str(), msg.c_str());
	}
      else
	this->printToMain("Usage: /msg __nickname__ __message__");
    }
  else
    this->printToMain("You need to be connected to perform this action");
}

void		MainWindow::list(std::istringstream& stream)
{
  StrArg	search;

  if (this->_connection && this->_connection->isLogged())
    {
      stream >> search;
      this->_connection->askList(search.c_str());
    }
}

void			MainWindow::user(std::istringstream& stream)
{
  StrArg		channel;
  ChannelWidget*	chanWid;

  chanWid = dynamic_cast<ChannelWidget*>(this->ui->tabWidget->currentWidget());
  if (this->_connection && this->_connection->isLogged())
    {
      if (!(stream >> channel))
	{
	  if (this->ui->tabWidget->currentIndex() > 0 && chanWid)
	    this->refreshUser(chanWid->getChannel().c_str());
	  else
	    this->printToMain("Usage: /users __channel__");
	}
      else
	this->refreshUser(channel.c_str());
    }
  else
    this->printToMain("You need to be connected to perform this action");
}

void		MainWindow::leaveChannel(const char* channel)
{
  int		i(0);

  while (++i < this->ui->tabWidget->count()
	 && this->ui->tabWidget->tabText(i) != channel)
    ;
  if (i < this->ui->tabWidget->count())
    this->tabRemover(i);
  this->refreshUser();
}

void		MainWindow::receiveMsg(const QString& channel,
				       const QString& nick,
				       const QString& msg)
{
  int		i(0);
  ChannelWidget	*chan;

  while (++i < this->ui->tabWidget->count()
	 && this->ui->tabWidget->tabText(i) != channel)
    ;
  if (i < this->ui->tabWidget->count()
      && (chan = dynamic_cast<ChannelWidget*>(this->ui->tabWidget->widget(i))))
    chan->appendPlainText(nick + ": " + msg);
}

void			MainWindow::connection(const QString& channel,
					       const QString& nickname)
{
  ChannelWidget*	chan;

  if ((chan = this->getChannel(channel)))
    this->appendColorText(chan, nickname + " joined the channel", "green");
  this->refreshUser();
}

void			MainWindow::disconnection(const QString& channel,
						  const QString& nickname)
{
  ChannelWidget*	chan;

  if ((chan = this->getChannel(channel)))
    this->appendColorText(chan, nickname + " left the channel", "red");
  this->refreshUser();
}

void			MainWindow::nickChanged(const QString& channel,
						const QString& from,
						const QString& to)
{
  ChannelWidget*	chan;

  if ((chan = getChannel(channel)))
    this->appendColorText(chan, from + " is now known as " + to, "blue");
  this->refreshUser();
}

void	MainWindow::openMP(QListWidgetItem* item)
{
  this->_privateMsg[item->text()]->show();
  this->_privateMsg[item->text()]->setFocus();
}

void	MainWindow::sendMP(const QString& to, const QString& msg)
{
  if (this->_connection && this->_connection->isLogged())
    {
      this->_connection->sendCommand(PRIVATE, "",
				     msg.toStdString().c_str() , msg.size(),
				     to.toStdString().c_str());
      _privateMsg[to]->receiveMessage(_connection->getNickname(), msg);
    }
}

void	MainWindow::receiveMP(const QString& from, const QString& msg)
{
  _privateMsg[from]->receiveMessage(from, msg);
}

void	MainWindow::notConnected(const QString& nick)
{
  _privateMsg[nick]->notConnected();
}

void	MainWindow::printList(const QString& channel)
{
  if (_refreshList)
    this->addToList(this->ui->ChannelList, channel);
  else
    {
      if (!_askingList)
	{
	  if ((_channelList =
	       dynamic_cast<QPlainTextEdit*>(ui->tabWidget->currentWidget())))
	    {
	      _channelList->appendPlainText(std::string(CHANNEL_LEN + 2,
							'-').c_str());
	      _channelList->appendPlainText("Channel already created:");
	    }
	  _askingList = true;
	}
      if (_channelList && this->ui->tabWidget->indexOf(_channelList) != -1)
	_channelList->appendPlainText(QString("- ") + channel);
    }
}

void	MainWindow::endList()
{
  if (_refreshList)
    _refreshList = false;
  else
    {
      _askingList = false;
      if (_channelList && this->ui->tabWidget->indexOf(_channelList) != -1)
	_channelList->appendPlainText(std::string(CHANNEL_LEN + 2,
						  '-').c_str());
      _channelList = NULL;
    }
}

void			MainWindow::user(const QString& channel,
					 const QString& user)
{
  QPlainTextEdit*	chan;

  if ((chan = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->currentWidget())))
    {
      if (_refreshUser)
	this->addToList(this->ui->userList, user);
      else
	{
	  if (!_askingUser)
	    {
	      chan->appendPlainText(std::string(50 + 2, '-').c_str());
	      chan->appendPlainText(QString("Users on ") + channel + ':');
	      _askingUser = true;
	    }
	  chan->appendPlainText(QString("- ") + user);
	}
    }
}

void			MainWindow::userEnd()
{
  QPlainTextEdit*	chan;

  _askingUser = false;
  if (!_refreshUser &&
      (chan = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->currentWidget())))
    chan->appendPlainText(std::string(50 + 2, '-').c_str());
  _refreshUser = false;
}

void	MainWindow::refreshList()
{
  if (!_askingList)
    {
      this->ui->ChannelList->clear();
      if (this->_connection && this->_connection->isLogged())
	{
	  this->_refreshList = true;
	  this->_connection->askList();
	}
    }
  if (this->_connection && this->_connection->isLogged())
    this->_timerList.start(2000);
}

void	MainWindow::refreshUser(int)
{
  this->refreshUser();
}

void			MainWindow::refreshUser(const QString& channel)
{
  ChannelWidget*	chan;

  if (!_askingUser && !_refreshUser)
    {
      chan = (channel.size()
	      ? this->getChannel(channel)
	      : dynamic_cast<ChannelWidget*>(ui->tabWidget->currentWidget()));
      if (this->_connection && this->_connection->isLogged() && chan)
	{
	  this->_connection->askUser(chan->getChannel().c_str());
	  if (!channel.size())
	    _refreshUser = true;
	}
    }
  if (!channel.size())
    this->ui->userList->clear();
}

ChannelWidget*	MainWindow::getChannel(const QString& channel) const
{
  int		i(0);
  ChannelWidget	*chan(NULL);

  while (++i < this->ui->tabWidget->count()
	 && this->ui->tabWidget->tabText(i) != channel)
    ;
  if (i < this->ui->tabWidget->count())
    chan = dynamic_cast<ChannelWidget*>(this->ui->tabWidget->widget(i));
  return (chan);
}

void	MainWindow::addToList(QListWidget* list, const QString& channel)
{
  list->addItem(channel);
}

void	MainWindow::appendColorText(QPlainTextEdit* area, const QString& str,
				    const QString& color)
{
  area->appendHtml(QString("<font color=\"") + color + "\">"
		   + str + "</font>");
}
