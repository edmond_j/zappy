//
// ChannelWidget.cpp for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Fri Apr 26 01:51:20 2013 julien edmond
// Last update Sat Apr 27 13:52:29 2013 julien edmond
//

#include <QTime>
#include "ChannelWidget.hh"

ChannelWidget::ChannelWidget(const std::string& channel, QWidget* parent)
  : QPlainTextEdit(parent), _channel(channel)
{
  this->setGeometry(0, 0, 521, 491);
  this->setReadOnly(true);
}

ChannelWidget::~ChannelWidget()
{}

const std::string&	ChannelWidget::getChannel() const
{
  return (_channel);
}

void	ChannelWidget::appendPlainText(const QString& text)
{
  QPlainTextEdit::appendPlainText(QTime::currentTime().toString("[hh:mm] ")
				  + text);
}
