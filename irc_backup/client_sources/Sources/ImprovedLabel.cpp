//
// ImprovedLabel.cpp for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sat Apr 27 14:09:35 2013 julien edmond
// Last update Sat Apr 27 14:16:25 2013 julien edmond
//

#include	<QMouseEvent>
#include	"ImprovedLabel.hh"

ImprovedLabel::ImprovedLabel(QWidget* parent, Qt::WindowFlags f)
  : QLabel(parent, f)
{}

ImprovedLabel::ImprovedLabel(const QString& text, QWidget* parent,
			     Qt::WindowFlags f)
  : QLabel(text, parent, f)
{}

ImprovedLabel::~ImprovedLabel()
{}

void	ImprovedLabel::mouseDoubleClickEvent(QMouseEvent* e)
{
  if (e->button() == Qt::LeftButton)
    {
      emit doubleClicked();
      QLabel::mouseDoubleClickEvent(e);
    }
}
