//
// MsgQueue.hh for LibC in /home/edmond_j//rendu/B4/C++/Plazza/rendu/LibC/MsgQueue
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sun Apr  7 20:41:55 2013 julien edmond
// Last update Sat Apr 13 14:31:44 2013 julien edmond
//

#ifndef		MSGQUEUE_HPP__
# define	MSGQUEUE_HPP__

# include	<sys/types.h>
# include	<sys/ipc.h>
# include	<sys/msg.h>
# include	<map>
# include	"Key/Key.hh"
# include	"Errno/Errno.hh"
# include	"CException/CException.hh"

namespace	LibC
{

  class MsgQueue
  {
  public:
    MsgQueue();
    ~MsgQueue();

    template <typename T>
    bool	send(const T* msgp, bool blocking = true);

    template <typename T>
    bool	rcv(int msgtyp, T* msgp, bool blocking = true);

  private:
    MsgQueue(const MsgQueue&);
    MsgQueue&	operator=(const MsgQueue&);

    int				_msq;
    static std::map<int, int>	_created;
  };

  template <typename T>
  bool		MsgQueue::send(const T* msgp, bool blocking)
  {
    int		ret;

    if ((ret = msgsnd(_msq, msgp, sizeof(T), (blocking ? 0 : IPC_NOWAIT)))
	&& Errno::getError() != EAGAIN)
      throw CException("MsgQueue: send");
    return (!ret);
  }

  template <typename T>
  bool		MsgQueue::rcv(int msgtyp, T* msgp, bool blocking)
  {
    int		ret;

    if ((ret = msgrcv(_msq, msgp, sizeof(T), msgtyp,
		      (blocking ? 0 : IPC_NOWAIT)))
	&& Errno::getError() != ENOMSG)
      throw CException("MsgQueue: rcv");
    return (!ret);
  }

}

#endif		// MSGQUEUE_HH__
