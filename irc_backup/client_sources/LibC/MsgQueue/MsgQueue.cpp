//
// MsgQueue.cpp for LibC in /home/edmond_j//rendu/B4/C++/Plazza/rendu/LibC/MsgQueue
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sun Apr  7 21:03:11 2013 julien edmond
// Last update Fri Apr 26 00:14:01 2013 julien edmond
//

#include	<sys/shm.h>
#include	"MsgQueue/MsgQueue.hpp"

namespace	LibC
{

  std::map<int, int>	MsgQueue::_created;

  MsgQueue::MsgQueue()
    : _msq(msgget(Key::getKey(), SHM_R | SHM_W))
  {
    if (_msq == -1)
      {
	if ((_msq = msgget(Key::getKey(), SHM_R | SHM_W | IPC_CREAT)) == -1)
	  throw CException("MsgQueue: msgget");
	_created[_msq] = 0;
      }
    else
      ++_created[_msq];
  }

  MsgQueue::~MsgQueue()
  {
    if (--_created[_msq] && msgctl(_msq, IPC_RMID, NULL))
      Errno::printError();
  }

}
