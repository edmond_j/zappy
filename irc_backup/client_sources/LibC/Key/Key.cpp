//
// Key.cpp for LibC in /home/edmond_j//rendu/B4/C++/Plazza/rendu/LibC/Key
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sun Apr  7 20:25:03 2013 julien edmond
// Last update Sat Apr 13 14:31:14 2013 julien edmond
//

#include	<cstdlib>
#include	<unistd.h>
#include	"Key.hh"
#include	"CException/CException.hh"

namespace	LibC
{

  key_t		Key::_key = -1;

  void		Key::initKey(const char *pathname, int proj_id)
  {
    bool	set(pathname);

    if (!set)
      pathname = get_current_dir_name();
    _key = ftok(pathname, proj_id);
    if (!set)
      free(const_cast<void *>(static_cast<const void *>(pathname)));
    if (_key == -1)
      throw CException("Key: ftok");
  }

  key_t	Key::getKey()
  {
    return (_key);
  }

}
