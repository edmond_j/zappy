//
// Key.hh for LibC in /home/edmond_j//rendu/B4/C++/Plazza/rendu/LibC/Key
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sun Apr  7 20:19:17 2013 julien edmond
// Last update Sun Apr  7 21:05:47 2013 julien edmond
//

#ifndef		KEY_HH__
# define	KEY_HH__

# include	<sys/types.h>
# include	<sys/ipc.h>

namespace	LibC
{

  class	Key
  {
  public:
    static void	initKey(const char *pathname = 0, int proj_id = 1);
    static key_t	getKey();

  private:
    Key();
    ~Key();
    Key(const Key&);
    Key&	operator=(const Key&);

    static key_t	_key;
  };

}

#endif		// KEY_HH__
