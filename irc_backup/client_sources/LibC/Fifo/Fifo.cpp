//
// Fifo.cpp for LibC in /home/edmond_j//rendu/B4/C++/Plazza/rendu/LibC/Fifo
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sun Apr  7 19:57:59 2013 julien edmond
// Last update Fri Apr 19 15:12:04 2013 julien edmond
//

#include	<sys/types.h>
#include	<sys/stat.h>
#include	<unistd.h>
#include	"CException/CException.hh"
#include	"Errno/Errno.hh"
#include	"Fifo.hh"
#include	"Stat/Stat.hh"

namespace	LibC
{

  /*
  ** Fifo class
  */

  Fifo::Fifo(const std::string& name)
    : _name(name)
  {
    try
      {
	Stat	s(_name.c_str());

	if (s.fileType() != Stat::Fifo)
	  throw std::runtime_error("File already exists and is not a Fifo");
      }
    catch (const CException& e)
      {
	if (e.getErrCode() != ENOENT)
	  throw e;
	if (mkfifo(name.c_str(), 0644))
	  throw CException("Fifo: mkfifo");
      }
  }

  Fifo::~Fifo()
  {
    if (unlink(_name.c_str()))
      Errno::printError("Fifo: unlink");
  }

  const std::string&	Fifo::getName() const
  {
    return (_name);
  }

  oFifo&	Fifo::endl(oFifo& os)
  {
    os.put(os.widen('\n'));
    os.flush();
    return (os);
  }

  oFifo&	Fifo::flush(oFifo& os)
  {
    os.flush();
    return (os);
  }

  /*
  ** iFifo class
  */

  iFifo::iFifo(Fifo& f, const Process& proc)
    : std::ifstream(f.getName().c_str()), _proc(proc)
  {}

  iFifo::~iFifo()
  {}

  /*
  ** oFifo class
  */

  oFifo::oFifo(Fifo& f, const Process& proc)
    : std::ofstream(f.getName().c_str()), _proc(proc)
  {}

  oFifo::~oFifo()
  {}

  oFifo&	oFifo::operator<<(fifoFunc func)
  {
    return (func(*this));
  }

  oFifo&	oFifo::flush()
  {
    if (_proc.isChild() || _proc.isChildAlive())
      std::ofstream::flush();
    return (*this);
  }

}
