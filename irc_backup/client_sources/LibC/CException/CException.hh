//
// CException.hh for libc in /home/edmond_j//Usefull/CException
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sat Mar 30 09:04:45 2013 julien edmond
// Last update Mon Apr 15 17:30:40 2013 julien edmond
//

#ifndef		CEXCEPTION_HH__
# define	CEXCEPTION_HH__

# include	<stdexcept>

namespace LibC
{

  class		CException: public std::runtime_error
  {
  public:
    explicit CException(const std::string& func) throw();
    CException(const std::string& func, int error) throw();

    virtual ~CException() throw();

    int		getErrCode() const;

  private:
    int		_errCode;
  };

}
#endif		// CEXCEPTION_HH__
