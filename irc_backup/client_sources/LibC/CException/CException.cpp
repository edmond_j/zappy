//
// CException.cpp for libc in /home/edmond_j//Usefull/CException
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sat Mar 30 13:59:15 2013 julien edmond
// Last update Mon Apr 15 17:33:23 2013 julien edmond
//

#include	<cstring>
#include	"Errno/Errno.hh"
#include	"CException/CException.hh"

namespace LibC
{

  CException::CException(const std::string& func) throw()
    : std::runtime_error(func + ": " + strerror(Errno::getError())),
      _errCode(Errno::getError())
  {}

  CException::CException(const std::string& func, int errcode) throw()
    : std::runtime_error(func + ": " + strerror(errcode)),
      _errCode(errcode)
  {}

  CException::~CException() throw()
  {}

  int	CException::getErrCode() const
  {
    return (_errCode);
  }
}
