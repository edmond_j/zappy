//
// Stat.hh for LibC in /home/edmond_j//rendu/B4/C++/Plazza/rendu/LibC/Stat
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Mon Apr 15 17:03:10 2013 julien edmond
// Last update Mon Apr 15 17:20:09 2013 julien edmond
//

#ifndef		STAT_HH__
# define	STAT_HH__

# include	<sys/types.h>
# include	<sys/stat.h>
# include	<unistd.h>

namespace	LibC
{

  class		Stat
  {
  public:

    enum FileType
      {
	NotSet,
	Regular,
	Directory,
	CharDev,
	BlkDev,
	Fifo,
	SymLink,
	Sock
      };

    Stat();
    Stat(const char* file, bool followLink = true);
    ~Stat();
    Stat(const Stat&);
    Stat&	operator=(const Stat&);

    FileType	fileType() const;

  private:
    bool	_isSet;
    struct stat	_buf;
  };

}
#endif		// STAT_HH__
