//
// Stat.cpp for LibC in /home/edmond_j//rendu/B4/C++/Plazza/rendu/LibC
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Mon Apr 15 17:12:07 2013 julien edmond
// Last update Mon Apr 15 17:23:20 2013 julien edmond
//

#include	"Stat.hh"
#include	"CException/CException.hh"

namespace	LibC
{

  Stat::Stat()
    : _isSet(false)
  {}

  Stat::Stat(const char* file, bool followLink)
  {
    int	ret;

    if (followLink)
      ret = stat(file, &_buf);
    else
      ret = lstat(file, &_buf);
    if (ret == -1)
      throw CException("Stat: stat");
    _isSet = true;
  }

  Stat::~Stat()
  {}

  Stat::Stat(const Stat& other)
    : _isSet(other._isSet), _buf(other._buf)
  {}

  Stat&	Stat::operator=(const Stat& other)
  {
    if (this != &other)
      {
	if ((_isSet = other._isSet))
	  _buf = other._buf;
      }
    return (*this);
  }

  Stat::FileType	Stat::fileType() const
  {
    FileType		sent(NotSet);

    if (_isSet)
      {
	const bool	tests[] = {S_ISREG(_buf.st_mode),
				   S_ISDIR(_buf.st_mode),
				   S_ISCHR(_buf.st_mode),
				   S_ISBLK(_buf.st_mode),
				   S_ISFIFO(_buf.st_mode),
				   S_ISLNK(_buf.st_mode),
				   S_ISSOCK(_buf.st_mode)};
	const FileType	types[] = {Regular, Directory, CharDev, BlkDev, Fifo,
				   SymLink, Sock};
	unsigned	i(0);

	while (i < (sizeof(tests) / sizeof(bool))
	       && i < sizeof(types) / sizeof(FileType)
	       && !tests[i])
	  ++i;
	if (i < (sizeof(tests) / sizeof(bool))
	    && i < sizeof(types) / sizeof(FileType))
	  sent = types[i];
      }
    return (sent);
  }

}
