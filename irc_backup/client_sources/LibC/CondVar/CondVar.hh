//
// CondVar.hh for LibC in /home/edmond_j//rendu/B4/C++/Plazza/rendu/LibC/CondVar
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sun Apr  7 21:15:23 2013 julien edmond
// Last update Sat Apr 13 14:32:40 2013 julien edmond
//

#ifndef		CONDVAR_HH__
# define	CONDVAR_HH__

# include	<pthread.h>
# include	"Mutex/Mutex.hh"

namespace	LibC
{

  class	CondVar
  {
  public:
    CondVar(Mutex& m);
    ~CondVar();

    void	wait();
    void	signal();
    void	broadcast();

    const pthread_cond_t	&getCond() const;

  private:
    CondVar();
    CondVar(const CondVar&);
    CondVar&	operator=(const CondVar&);

    pthread_cond_t	_cond;
    Mutex&		_mutex;
  };

}

#endif		// CONDVAR_HH__
