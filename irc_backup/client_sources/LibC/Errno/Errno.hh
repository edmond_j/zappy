/*
** Errno.c for Errno in /home/edmond_j//Usefull/Errno
** 
** Made by julien edmond
** Login   <edmond_j@epitech.net>
** 
** Started on  Fri Mar 29 11:27:34 2013 julien edmond
// Last update Sat Apr 13 17:03:57 2013 julien edmond
*/

#ifndef		ERRNO_HH__
# define	ERRNO_HH__

# include	<string>
# include	<iostream>
# include	<cerrno>

namespace LibC
{

  class Errno
  {
  public:
    static int		getError();
    static std::string	getStrError();
    static std::string	getStrError(int err);
    static bool		printError(const std::string& func = "",
				   std::ostream& out = std::cerr);

  private:
    Errno();
    ~Errno();
    Errno(const Errno &);
    Errno&	operator=(const Errno &);
  };

}
#endif		// ERRNO_HH__
