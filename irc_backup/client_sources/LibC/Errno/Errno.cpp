//
// Errno.cpp for errno in /home/edmond_j//Usefull/Errno
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Fri Mar 29 11:32:44 2013 julien edmond
// Last update Sat Apr 13 17:04:21 2013 julien edmond
//

#include	<cerrno>
#include	<cstring>
#include	"Errno.hh"

namespace LibC
{

  int	Errno::getError()
  {
    return (errno);
  }

  std::string	Errno::getStrError(int err)
  {
    return (strerror(err));
  }

  std::string	Errno::getStrError()
  {
    return (strerror(errno));
  }

  bool	Errno::printError(const std::string& func, std::ostream& out)
  {
    return (out << (func.size() ? (func + ": ") : func) << getStrError()
	    << std::endl);
  }

}
