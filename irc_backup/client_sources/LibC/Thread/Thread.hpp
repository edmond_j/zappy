//
// Thread.hpp for LibC in /home/edmond_j//rendu/B4/C++/Plazza/rendu
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sun Apr 14 22:10:08 2013 julien edmond
// Last update Mon Apr 15 21:12:50 2013 julien edmond
//

#ifndef			THREAD_HH__
# define		THREAD_HH__

# include		<pthread.h>
# include		<signal.h>
# include		<cerrno>
# include		"Mutex/Mutex.hh"
# include		"Sleep/Sleep.hh"

namespace		LibC
{
  class		Thread
  {
  public:
    enum Action
      {
	Kill,
	Wait,
	Leave
      };

    typedef void*	(*startRoutine)(void *param);

    template<typename T>
    Thread(void* (T::*start)(void *), T* obj, void *arg, Action act = Leave);

    Thread(startRoutine start, void *arg, Action act = Leave);
    ~Thread();

    void		setAct(Action act);

    void		join(void **) const;
    bool		tryJoin(void **) const;
    const pthread_t	&getId() const;

  private:

    template<typename T>
    struct	NestedRoutine
    {
      void		*_arg;
      void*		(T::*_start)(void *);
      T*		_obj;
      static void*	starter(void*);
    };

    Thread();
    Thread(const Thread&);
    Thread&	operator=(const Thread&);

    void init(startRoutine start, void *arg, Action act);

    pthread_t		_thread;
    Action		_act;
  };

  template<typename T>
  Thread::Thread(void* (T::*start)(void *), T* obj, void *arg, Action act)
  {
    NestedRoutine<T>	*routine = new NestedRoutine<T>;

    routine->_arg = arg;
    routine->_start = start;
    routine->_obj = obj;
    this->init(&NestedRoutine<T>::starter, routine, act);
  }

  template<typename T>
  void*	Thread::NestedRoutine<T>::starter(void* rout)
  {
    NestedRoutine<T>*	routine(static_cast<NestedRoutine<T>*>(rout));
    void*		arg;
    void*		(T::*start)(void *);
    T*			obj;

    if (!routine)
      throw std::runtime_error("You should'nt call this routine");
    arg = routine->_arg;
    start = routine->_start;
    obj = routine->_obj;
    delete routine;
    return ((obj->*start)(arg));
  }

}

#endif			/* THREAD_HH__ */
