 //
// Thread.cpp for plazza in /home/grange_s//rendulocal/tek2/plazza/Thread
// 
// Made by steve granger
// Login   <grange_s@epitech.net>
// 
// Started on  Sun Apr  7 19:25:36 2013 steve granger
// Last update Mon Apr 15 21:13:20 2013 julien edmond
//

#include	"CException/CException.hh"
#include	"Thread.hpp"

namespace	LibC
{
  Thread::Thread(startRoutine start, void *arg, Action act)
  {
    this->init(start, arg, act);
  }

  void	Thread::init(startRoutine start, void *arg, Action act)
  {
    int ret;

    this->_act = act;
    if ((ret = pthread_create(&this->_thread, NULL, start, arg)) != 0)
      throw CException("Thread", ret);
  }

  Thread::~Thread()
  {
    if (this->_act == Kill)
      pthread_kill(this->_thread, SIGTERM);
    if (this->_act == Wait || this->_act == Kill)
      join(NULL);
  }

  void	Thread::setAct(Action act)
  {
    _act = act;
  }

  void	Thread::join(void **param) const
  {
    int	ret;

    if ((ret = pthread_join(this->_thread, param)) != 0)
      throw CException("Thread_join", ret);
  }

  bool	Thread::tryJoin(void **param) const
  {
    int	ret;

    if ((ret = pthread_tryjoin_np(this->_thread, param)) == EBUSY)
      return (false);
    else if (ret != 0)
      throw CException("Thread_tryjoin", ret);
    return (true);
  }

  const pthread_t	&Thread::getId() const
  {
    return (this->_thread);
  }
}
