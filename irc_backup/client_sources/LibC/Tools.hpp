//
// Tools.hpp for plazza in /home/edmond_j//rendu/B4/C++/Nibbler/nibbler_snake_eater/includes
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Fri Mar  8 14:44:27 2013 julien edmond
// Last update Wed Apr 17 22:55:33 2013 julien edmond
//

#ifndef		TOOLS_HPP__
# define	TOOLS_HPP__

# include	<string>
# include	<sstream>
# include	<iostream>

namespace	Tools
{
  template<typename T>
  T			str_to_(const std::string &str)
  {
    std::istringstream	stream(str);
    T			sent;

    stream >> sent;
    return (sent);
  }

  template<typename T>
  bool			str_to_(const std::string &str, T& value)
  {
    std::istringstream	stream(str);

    stream >> value;
    return (stream);
  }

  template<typename T>
  std::string		str_of_(const T& var)
  {
    std::ostringstream	stream;

    stream << var;
    return (stream.str());
  }

  template<typename T>
  void	deleter(const T *ptr)
  {
    delete ptr;
  }
}

#endif		// TOOLS_HPP__
