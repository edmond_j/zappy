//
// StrArg.hh for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sat Apr 27 14:59:54 2013 julien edmond
// Last update Sat Apr 27 15:12:03 2013 julien edmond
//

#ifndef		STRARG_HH__
# define	STRARG_HH__

# include	<iostream>
# include	<string>

class		StrArg: public std::string
{
public:
  StrArg();
  StrArg(const StrArg& str);
  StrArg(const StrArg& str, size_t pos, size_t len = npos);
  StrArg(const char* s);
  StrArg(const char* s, size_t n);
  StrArg(size_t n, char c);
  template<class InputIterator>
  StrArg(InputIterator first, InputIterator last);

  StrArg&	operator=(const StrArg&);

  ~StrArg();
};

std::istream&	operator>>(std::istream&, StrArg& str);

template<class InputIterator>
StrArg::StrArg(InputIterator first, InputIterator last)
  : std::string(first, last)
{}

#endif		// STRARG_HH__
