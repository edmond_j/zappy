//
// IrcConnection.hh for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j/Includes
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Wed Apr 24 20:02:01 2013 julien edmond
// Last update Sun Apr 28 11:13:46 2013 julien edmond
//

#ifndef		IRCCONNECTION_HH__
# define	IRCCONNECTION_HH__

class		IrcConnection;

# include	<map>
# include	<QTimer>
# include	<QObject>
# include	"TCPSocket.hpp"

class		IrcConnection: public QObject, public TCPSocket
{
  Q_OBJECT

public:
  IrcConnection(const char* hostname, int port,
		const char* nickname);
  ~IrcConnection();

  bool		isLogged() const;
  const char*	getNickname() const;

  void	dealWithInputs();
  void	sendCommand(t_type type, const char* channel, const char* data,
		    int size, const char* nickname = NULL);
  void	askList(const char* pattern = "");
  void	askUser(const char* channel);

signals:
  void	nickNameChanged(const QString&);
  void	msgReceived(const QString& channel, const QString& from,
		    const QString& msg);
  void	connection(const QString& channel, const QString& nickname);
  void	disconnection(const QString& channel, const QString& nickname);
  void	nickChanged(const QString& channel, const QString& from,
		    const QString& to);
  void	mpReceived(const QString& from, const QString& msg);
  void	notConnected(const QString& nick);
  void	listChan(const QString& channel);
  void	endList();
  void	user(const QString& channel, const QString& nick);
  void	endUser();

public slots:
  void	checkLogged();

private:
  typedef void	(IrcConnection::*Actions)(const t_packet*);
  typedef std::map<t_type, Actions>	ActionMap;

  IrcConnection();
  IrcConnection&	operator=(const IrcConnection&);

  //
  // Actions
  //

  void		checkProto(const t_packet* packet);
  void		receiveMsg(const t_packet* packet);
  void		someoneConnected(const t_packet* packet);
  void		someoneDisconnected(const t_packet* packet);
  void		nickChanged(const t_packet* packet);
  void		privateMsg(const t_packet* packet);
  void		notConnected(const t_packet* packet);
  void		listChan(const t_packet* packet);
  void		endList(const t_packet* packet);
  void		user(const t_packet* packet);
  void		endUser(const t_packet* packet);

  ActionMap	_actions;
  std::string	_nickname;
  bool		_isLogged;
  QTimer	_timer;
  std::string	_pattern;
};

#endif		// IRCCONNECTION_HH__
