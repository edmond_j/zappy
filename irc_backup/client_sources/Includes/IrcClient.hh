//
// IrcClient.hh for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j/my_irc
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Wed Apr 24 15:01:53 2013 julien edmond
// Last update Wed Apr 24 15:09:19 2013 julien edmond
//

#ifndef		IRCCLIENT_HH__
# define	IRCCLIENT_HH__

# include	<QApplication>

class		IrcClient
{
public:
  IrcClient(int ac, char **av);
  ~IrcClient();

  int		start();

private:
  IrcClient(const IrcClient&);
  IrcClient&	operator=(const IrcClient&);

  QApplication	app;
};

#endif		// IRCCLIENT_HH__
