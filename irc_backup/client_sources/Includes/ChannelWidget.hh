//
// ChannelWidget.hh for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Fri Apr 26 01:23:59 2013 julien edmond
// Last update Sat Apr 27 13:50:10 2013 julien edmond
//

#ifndef		CHANNELWIDGET_HH__
# define	CHANNELWIDGET_HH__

# include	<QPlainTextEdit>
# include	<string>

class		ChannelWidget: public QPlainTextEdit
{
public:
  ChannelWidget(const std::string& channel = "Unnamed",
		QWidget *parent = NULL);
  ~ChannelWidget();

  const std::string&	getChannel() const;
  void			appendPlainText(const QString& text);

private:
  ChannelWidget(const ChannelWidget&);
  ChannelWidget&	operator=(const ChannelWidget&);

  std::string	_channel;
};

#endif		// CHANNELWIDGET_HH__
