//
// PrivateMsgContainer.hh for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sat Apr 27 16:18:01 2013 julien edmond
// Last update Sat Apr 27 18:25:23 2013 julien edmond
//

#ifndef		PRIVATEMSGCONTAINER_HH__
# define	PRIVATEMSGCONTAINER_HH__

class	PrivateMsgContainer;
class	MainWindow;

# include	<map>
# include	"PrivateMessage.hh"

class		PrivateMsgContainer
  : public std::map<const QString, PrivateMessage*>
{
public:
  PrivateMsgContainer(MainWindow* parent = 0);
  ~PrivateMsgContainer();

  PrivateMessage*&	operator[](const QString& name);

  void	clear();

private:
  PrivateMsgContainer(const PrivateMsgContainer&);
  PrivateMsgContainer&	operator=(const PrivateMsgContainer&);

  static void	deleter(std::pair<const QString, PrivateMessage*>&);

  MainWindow*	_parent;
};

#endif		// PRIVATEMSGCONTAINER_HH__
