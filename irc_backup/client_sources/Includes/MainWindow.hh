//
// MainWindow.hh for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j/my_irc
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Wed Apr 24 15:04:33 2013 julien edmond
// Last update Sun Apr 28 14:41:14 2013 julien edmond
//

#ifndef		MAINWINDOW_HH_
# define	MAINWINDOW_HH_

class		MainWindow;

# include	<QTimer>
# include	<sstream>
# include	<map>
# include	<QString>
# include	<QMainWindow>
# include	<QListWidget>
# include	"ConnectWindow.hh"
# include	"IrcConnection.hh"
# include	"ChannelWidget.hh"
# include	"PrivateMsgContainer.hh"

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();
  void	show();
  void	communicate();

  void	printToMain(const QString& str);

  static void   appendColorText(QPlainTextEdit* area, const QString& str,
                                const QString& color);

public slots:
  void		connectTo(const QString& server, int port,
			  const QString& nickname);
  void		disconnect();
  void		sendCmd();
  void		tabRemover(int);
  void		changeNickname();
  void		changeNickname(const QString& nick);
  void		joinChannel();
  void		joinChannel(const char*);
  void		joinChannel(QListWidgetItem *);
  void		leaveChannel(const char*);
  void		receiveMsg(const QString& channel, const QString& nick,
			   const QString& msg);
  void		connection(const QString& channel, const QString& nick);
  void		disconnection(const QString& channel, const QString& nick);
  void		nickChanged(const QString& channel, const QString& from,
			    const QString& to);
  void		openMP(QListWidgetItem*);
  void		sendMP(const QString& to, const QString& msg);
  void		receiveMP(const QString& from, const QString& msg);
  void		notConnected(const QString& nick);
  void		printList(const QString& channel);
  void		endList();
  void		user(const QString& channel, const QString& user);
  void		userEnd();

  void		refreshList();
  void		refreshUser(int);
  void		refreshUser(const QString& channel = QString());

private:
  typedef void	(MainWindow::*Command)(std::istringstream& stream);
  typedef std::map<const std::string, Command>	CmdMap;

  MainWindow(const MainWindow&);
  MainWindow& operator=(const MainWindow&);

  void		connector() const;
  void		connectConnection() const;
  void		sendMsg();
  void		addToList(QListWidget*, const QString&);

  ChannelWidget*	getChannel(const QString& channel) const;

  //
  // Commands
  //

  void	server(std::istringstream& stream);
  void	join(std::istringstream& stream);
  void	part(std::istringstream& stream);
  void	nick(std::istringstream& stream);
  void	privateMsg(std::istringstream& stream);
  void	list(std::istringstream& stream);
  void	user(std::istringstream& stream);

  Ui::MainWindow	*ui;
  bool			_isLogged;
  ConnectWindow		_connectWin;
  IrcConnection		*_connection;
  PrivateMsgContainer	_privateMsg;

  bool			_askingList;
  QPlainTextEdit	*_channelList;
  bool			_refreshList;
  QTimer		_timerList;

  bool			_askingUser;
  bool			_refreshUser;

  CmdMap		_commands;
};

#endif		// MAINWINDOW_HH
