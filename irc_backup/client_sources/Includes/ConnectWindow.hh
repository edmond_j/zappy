//
// ConnectWindow.hh for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j/my_irc
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Wed Apr 24 15:05:05 2013 julien edmond
// Last update Wed Apr 24 17:29:46 2013 julien edmond
//

#ifndef		CONNECTWINDOW_HH
# define	CONNECTWINDOW_HH

# include	<QDialog>

namespace Ui {
  class ConnectWindow;
}

class ConnectWindow : public QDialog
{
  Q_OBJECT

public:
  explicit ConnectWindow(QWidget *parent = 0);
  ~ConnectWindow();

public slots:
  void	connect();

signals:
  void	connect(const QString& host, int port, const QString& nick);

private:
  void	connectToParent(const QWidget *) const;

  Ui::ConnectWindow *ui;
};

#endif		// CONNECTWINDOW_HH
