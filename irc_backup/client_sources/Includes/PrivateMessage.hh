//
// PrivateMessage.hh for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sat Apr 27 16:17:20 2013 julien edmond
// Last update Sat Apr 27 18:49:09 2013 julien edmond
//

#ifndef		PRIVATEMESSAGE_HH_
# define	PRIVATEMESSAGE_HH_

# include	<QDialog>

class		PrivateMessage;
class		MainWindow;

namespace Ui {
  class PrivateMessage;
}

class PrivateMessage : public QWidget
{
  Q_OBJECT

public:
  explicit PrivateMessage(MainWindow *parent = 0);
  PrivateMessage(const QString& to, MainWindow* parent);
  ~PrivateMessage();

  void	notConnected();

signals:
  void	sendMsg(const QString& to, const QString&);

public slots:
  void	msgSender();
  void	receiveMessage(const QString& from, const QString& msg);

private:
  PrivateMessage(const PrivateMessage&);
  PrivateMessage&	operator=(const PrivateMessage&);

  Ui::PrivateMessage*	ui;
  QString		_to;
};

#endif // PRIVATEMESSAGE_HH_
