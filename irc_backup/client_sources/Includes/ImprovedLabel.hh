//
// ImprovedLabel.hh for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j/Includes
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sat Apr 27 14:02:18 2013 julien edmond
// Last update Sat Apr 27 14:09:17 2013 julien edmond
//

#ifndef		IMPROVEDLABEL_HH__
# define	IMPROVEDLABEL_HH__

# include	<QLabel>

class		ImprovedLabel: public QLabel
{
  Q_OBJECT

public:
  ImprovedLabel(QWidget* parent = 0, Qt::WindowFlags f = 0);
  ImprovedLabel(const QString& text, QWidget* parent = 0,
		Qt::WindowFlags f = 0);
  ~ImprovedLabel();

signals:
  void	doubleClicked();

protected:
  void	mouseDoubleClickEvent(QMouseEvent* e);

private:
  ImprovedLabel(const ImprovedLabel&);
  ImprovedLabel& operator=(const ImprovedLabel&);
};

#endif		// IMPROVEDLABEL_HH__
