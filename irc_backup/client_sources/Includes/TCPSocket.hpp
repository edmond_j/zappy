//
// TcpSocket.hpp for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j/Sources
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Wed Apr 24 20:12:35 2013 julien edmond
// Last update Thu Apr 25 23:57:27 2013 julien edmond
//

#ifndef		TCPSOCKET_HPP__
# define	TCPSOCKET_HPP__

# include	<string>
# include	"CircularBuffer.hpp"

class		TCPSocket
{
public:
  TCPSocket(const char *host, int port);
  virtual ~TCPSocket();

  void		refresh();

  template<typename T>
  TCPSocket&	operator<<(const T&);

  template<typename T>
  bool	operator>>(T&);

  bool	isConnected() const;
  const char*	getConnectError() const;

  int		getPort() const;
  const char*	getHost() const;

protected:
  TCPSocket();
  TCPSocket(const TCPSocket&);
  TCPSocket&	operator=(const TCPSocket&);

  int		readFromServ();
  int		writeToServ();

  bool		_isConnected;
  bool		_isReadable;
  bool		_isWritable;
  int		_socket;
  std::string	_connectError;
  std::string	_host;
  int		_port;

  CircularBuffer	_input;
  CircularBuffer	_output;
};

template<typename T>
TCPSocket&	TCPSocket::operator<<(const T& item)
{
  if (_isConnected)
    _output << item;
  return (*this);
}

template<typename T>
bool	TCPSocket::operator>>(T& item)
{
  bool	ret(false);

  if (_isConnected)
    ret = (_input >> item);
  return (ret);
}

#endif		// TCPSOCKET_HPP__
