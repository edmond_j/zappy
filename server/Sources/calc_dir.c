/*
** calc_dir.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server/Sources/player_act
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue Jun  4 10:10:33 2013 julien edmond
** Last update Tue Jun  4 10:54:50 2013 julien edmond
*/

#include		"zappy.h"

int			get_num_of_coord(int *vect)
{
  static const t_vect	vects[] = { {0, 0}, {0, -1}, {-1, -1}, {-1, 0},
				    {-1, 1}, {0, 1}, {1, 1}, {1, 0}, {1, -1} };
  unsigned		i;

  i = 0;
  while (i < sizeof(vects) / sizeof(*vects)
	 && (vects[i][X] != vect[X] || vects[i][Y] != vect[Y]))
    ++i;
  if (i >= sizeof(vects) / sizeof(*vects))
    i = 0;
  return (i);
}

void	earth_round(int width, int height, int *vect)
{
  if (ABS(vect[X]) > width / 2)
    vect[X] = (width - ABS(vect[X])) * -(vect[X] / ABS(vect[X]));
  if (ABS(vect[Y]) > height / 2)
    vect[Y] = (height - ABS(vect[Y])) * -(vect[Y] / ABS(vect[Y]));
}

int		calc_dir(t_zappy *zappy, t_client *from, t_client *to)
{
  int		vect[2];
  int		sent;

  vect[X] = to->pp->coord[X] - from->pp->coord[X];
  vect[Y] = to->pp->coord[Y] - from->pp->coord[Y];
  earth_round(zappy->width, zappy->height, vect);
  if (vect[X] && ABS(vect[Y]) > ABS(vect[X]))
    vect[X] = 0;
  if (vect[Y] && ABS(vect[X]) > ABS(vect[Y]))
    vect[Y] = 0;
  if (vect[X])
    vect[X] /= ABS(vect[X]);
  if (vect[Y])
    vect[Y] /= ABS(vect[Y]);
  if ((sent = get_num_of_coord(vect))
      && (sent = sent + (from->pp->orient - 1) * 2) > 8)
    sent -= 8;
  return (sent);
}
