/*
** random.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server/Sources
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Sat Jun  1 10:21:14 2013 julien edmond
** Last update Sat Jun  1 10:21:52 2013 julien edmond
*/

#include	<stdlib.h>

int	rand_in(int min, int max)
{
  return (rand() % (max - min + 1) + min);
}
