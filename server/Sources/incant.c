/*
** incant.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue Jun  4 14:25:19 2013 julien edmond
** Last update Tue Jun  4 17:46:28 2013 julien edmond
*/

#include	<stdlib.h>
#include	"zappy.h"

t_incant	*add_incant(t_incant **list)
{
  while (*list)
    list = &(*list)->next;
  return ((*list) = malloc(sizeof(t_incant)));
}

int	check_players(t_incant *incant, int nb_player)
{
  return (nb_player == 0
	  || (incant->concerned[--nb_player]
	      && vect_eq(&incant->concerned[nb_player]->pp->coord,
			 &incant->coord)
	      && check_players(incant, nb_player)));
}

int			check_incant(t_zappy *zappy, t_incant *incant)
{
  static const t_rite	rites[] = { {1, {0, 1, 0, 0, 0, 0, 0} },
				    {2, {0, 1, 1, 1, 0, 0, 0} },
				    {2, {0, 2, 0, 1, 0, 2, 0} },
				    {4, {0, 1, 1, 2, 0, 1, 0} },
				    {4, {0, 1, 2, 1, 3, 0, 0} },
				    {6, {0, 1, 2, 3, 0, 1, 0} },
				    {6, {0, 2, 2, 2, 2, 2, 2} } };

  return (incant->level - 2 >= 0
	  && (incant->level - 2) < (int)(sizeof(rites) / sizeof(*rites))
	  && check_players(incant, rites[incant->level - 2].nb_player)
	  && rec_ge(map_at(&zappy->map, incant->coord[X], incant->coord[Y]),
		    &rites[incant->level - 2].rec));
}

void	remove_incant(t_incant *incant, t_incant **list)
{
  while ((*list) && (*list) != incant)
    list = &(*list)->next;
  if ((*list))
    {
      *list = incant->next;
      free(incant->concerned);
      free(incant);
    }
}

void	destroy_incant(t_incant *incant)
{
  if (incant)
    {
      destroy_incant(incant->next);
      free(incant);
    }
}
