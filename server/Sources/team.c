/*
** team.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Fri May 24 15:11:51 2013 julien edmond
** Last update Sat Jun  1 09:45:33 2013 julien edmond
*/

#include	<stdlib.h>
#include	<stdio.h>
#include	<string.h>
#include	"zappy.h"

t_team	*find_team(t_team *teams, char *name)
{
  while (teams && strcmp(teams->team_name, name))
    teams = teams->next;
  return (teams);
}

int	add_client_to_team(t_client *client, t_team *team, t_zappy *zappy)
{
  int	cli_nb;

  cprintf(client, "%s\n%d\n", team->team_name,
	  (cli_nb = team->max_player - team->nb_players));
  if (cli_nb)
    {
      ++team->nb_players;
      client->team = team;
      cprintf(client, "%d %d\n", zappy->width, zappy->height);
    }
  return (0);
}
