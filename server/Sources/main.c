/*
** main.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server/Sources
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue May 21 08:29:17 2013 julien edmond
** Last update Mon Jun  3 22:52:19 2013 julien edmond
*/

#include	<string.h>
#include	"zappy.h"

int		main(int ac, char **av)
{
  int		ret;
  t_zappy	zappy;

  ret = 1;
  bzero(&zappy, sizeof(zappy));
  if (!(parse_av(ac, av, &zappy)))
    ret = run(&zappy);
  destroy_zappy(&zappy);
  return (ret);
}
