/*
** cmd_list.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Fri May 24 14:06:32 2013 julien edmond
** Last update Mon Jun  3 15:20:02 2013 julien edmond
*/

#include	<string.h>
#include	<stdlib.h>
#include	"zappy.h"

char		**str_to_wordtab(char *str)
{
  char		**sent;
  unsigned	size;
  char		*word;

  sent = NULL;
  size = 0;
  while ((word = strtok((size > 0 ? NULL : str), " \t"))
	 && (sent = realloc(sent, sizeof(char *) * (++size + 1))))
    {
      sent[size - 1] = strdup(word);
      sent[size] = NULL;
    }
  return (sent);
}

void		add_cmd(t_cmd **cmd, char *str)
{
  while (*cmd)
    cmd = &(*cmd)->next;
  if ((*cmd = malloc(sizeof(t_cmd))))
    {
      (*cmd)->cmd = str_to_wordtab(str);
      free(str);
      (*cmd)->next = NULL;
    }
  if (*cmd && !(*cmd)->cmd)
    {
      free(*cmd);
      *cmd = NULL;
    }
}

t_cmd		*pop_front_cmd(t_cmd **cmd)
{
  t_cmd		*next;
  unsigned	i;

  i = 0;
  next = NULL;
  if ((*cmd))
    {
      next = (*cmd)->next;
      while ((*cmd)->cmd[i])
	{
	  free((*cmd)->cmd[i]);
	  ++i;
	}
      free((*cmd)->cmd);
      free(*cmd);
    }
  return ((*cmd = next));
}
