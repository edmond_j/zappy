/*
** ressource.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Fri May 24 13:53:34 2013 julien edmond
** Last update Tue Jun  4 17:51:16 2013 julien edmond
*/

#include	<stdlib.h>
#include	<string.h>
#include	"zappy.h"

int			get_rsrc_by_name(t_ressources *rec, const char *name)
{
  int			ret;
  const char * const	strs[] = {"nourriture", "linemate", "deraumere",
				  "sibur", "mendiane", "phiras", "thystame"};
  unsigned		i;

  i = 0;
  while (i < sizeof(strs) / sizeof(*strs) && strcmp(name, strs[i]))
    ++i;
  if (i < sizeof(strs) / sizeof(*strs))
    {
      ret = 1;
      *rec = i;
    }
  else
    ret = 0;
  return (ret);
}

const char		*ressource_name(t_ressources rec)
{
  const char * const	strs[] = {"nourriture", "linemate", "deraumere",
				  "sibur", "mendiane", "phiras", "thystame"};
  const char		*ret;

  if (rec < sizeof(strs) / sizeof(*strs))
    ret = strs[rec];
  else
    ret = NULL;
  return (ret);
}

void		add_ressource(t_map *map, t_ressources rec)
{
  /* A VIRER */
  int		x;
  int		y;

  ++(*(map_at(map, x = rand(), y = rand())))[rec];
  printf("Adding %s at (%d, %d)\n", ressource_name(rec), x % map->width,
	 y % map->height);
}

void	add_ressources(t_map *map, t_ressources rec, int nb)
{
  while (--nb >= 0)
    add_ressource(map, rec);
}

int	rec_ge(t_square *left, const t_square *right)
{
  int	i;

  i = -1;
  while (++i < NB_RESSOURCES && left[i] >= right[i])
    ;
  return (i == NB_RESSOURCES);
}
