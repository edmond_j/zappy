/*
** end.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server/Sources
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue May 21 09:35:24 2013 julien edmond
** Last update Tue Jun  4 15:20:53 2013 julien edmond
*/

#include	<stdlib.h>
#include	<stdio.h>
#include	"zappy.h"

void		dump_zappy(t_zappy *zappy)
{
  t_team	*runner;

  if (printf("Port: %hd\nSize: %ux%u\nCpt: %u\nT: %u\nTeams: ",
	     zappy->port, zappy->width, zappy->height, zappy->client_per_team,
	     zappy->time_delay) == -1)
    perror("printf");
  runner = zappy->teams;
  while (runner)
    if (printf("(%s [%u/%u])", runner->team_name, runner->nb_players,
	       runner->max_player) == -1
	|| ((runner = runner->next) && printf(", ") == -1))
      perror("printf");
  if (printf("\n") == -1)
    perror("printf");
}

void		free_list(t_team *elmt)
{
  if (elmt && elmt->next)
    free_list(elmt->next);
  free(elmt);
}

void		destroy_zappy(t_zappy *zappy)
{
  dump_zappy(zappy);
  destroy_incant(zappy->incants);
  free_list(zappy->teams);
  destroy_map(&zappy->map);
}
