/*
** client_handler.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue May 21 12:55:58 2013 julien edmond
** Last update Mon Jun  3 15:18:27 2013 julien edmond
*/

#include	<unistd.h>
#include	<sys/types.h>
#include	<sys/socket.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<sys/select.h>
#include	"zappy.h"

void	set_clients(fd_set *wfds, fd_set *rfds, t_client *clients, int *max)
{
  if (clients)
    {
      FD_SET(clients->sock, rfds);
      if (buff_size(clients->output) > 0)
	FD_SET(clients->sock, wfds);
      if (clients->next)
	set_clients(wfds, rfds, clients->next, max);
      *max = MAX(*max, clients->sock);
    }
}

void				operate_clients(fd_set *wfds, fd_set *rfds,
						t_client **clients)
{
  t_client			**next;
  t_client			*tmp;

  if (*clients)
    {
      next = &(*clients)->next;
      if ((FD_ISSET((*clients)->sock, wfds)
	   && write_from_buff((*clients)->output, (*clients)->sock) <= 0)
	  || (FD_ISSET((*clients)->sock, rfds)
	      && read_to_buff((*clients)->input, (*clients)->sock) <= 0)
	  || (*clients)->ttl <= 0)
	{
	  tmp = *clients;
	  *clients = *next;
	  destroy_client(tmp);
	  if (*clients)
	    operate_clients(wfds, rfds, clients);
	}
      else if (next)
	operate_clients(wfds, rfds, next);
    }
}

void				connect_client(int serv_sock,
					       t_client **clients)
{
  static int			id = 0;
  int				sock;

  if ((sock = accept(serv_sock, NULL, NULL)) != -1)
    {
      while (*clients)
	clients = &(*clients)->next;
      if ((*clients = malloc(sizeof(t_client)))
	  && ((*clients)->input = new_buff(BUFF_SIZE))
	  && ((*clients)->output = new_buff(BUFF_SIZE)))
	{
	  (*clients)->id = ++id;
	  (*clients)->team = NULL;
	  (*clients)->sock = sock;
	  (*clients)->next = NULL;
	  (*clients)->to_treat = NULL;
	  (*clients)->ttl = 10;
	  (*clients)->pp = NULL;
	  write_to_client(*clients, "BIENVENUE\n");
	}
      else if (close(sock) == -1)
	perror("close");
    }
}

void		operate_all_clients(fd_set *wfds, fd_set *rfds,
				    t_client **clients)
{
  operate_clients(wfds, rfds, &clients[UNLOGGED]);
  operate_clients(wfds, rfds, &clients[PLAYER]);
  operate_clients(wfds, rfds, &clients[GRAPHIC]);
}

int				client_handler(int serv_sock,
					       t_client **clients,
					       double *elapsed)
{
  fd_set			wfds;
  fd_set			rfds;
  struct timeval		timeout;
  int				max;
  int				ret;
  int				i;

  i = -1;
  max = serv_sock;
  timeout.tv_sec = TO_SEC;
  timeout.tv_usec = TO_USEC;
  FD_ZERO(&wfds);
  FD_ZERO(&rfds);
  FD_SET(serv_sock, &rfds);
  while (++i < 3)
    set_clients(&wfds, &rfds, clients[i], &max);
  if (!(ret = !(select(max + 1, &rfds, &wfds, NULL, &timeout) != -1)))
    {
      operate_all_clients(&wfds, &rfds, clients);
      if (FD_ISSET(serv_sock, &rfds))
	connect_client(serv_sock, clients);
    }
  *elapsed = TO_DOUBLE - timeval_to_double(&timeout);
  return (ret);
}
