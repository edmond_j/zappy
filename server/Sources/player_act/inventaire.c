/*
** inventaire.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server/Sources/player_act
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Mon Jun  3 17:35:37 2013 julien edmond
** Last update Tue Jun  4 11:04:26 2013 julien edmond
*/

#include	"zappy.h"

void	inventaire(t_zappy *zappy, t_client *client, char **av)
{
  int	i;

  (void)zappy;
  (void)av;
  i = 0;
  write_to_client(client, "{");
  while (i < NB_RESSOURCES)
    {
      cprintf(client, "%s %d", ressource_name(i), client->pp->inv[i]);
      if (++i < NB_RESSOURCES)
	write_to_client(client, ", ");
    }
  write_to_client(client, "}\n");
  client->pp->delay = 1 / zappy->time_delay;
}
