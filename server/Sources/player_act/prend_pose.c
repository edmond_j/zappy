/*
** prend_pose.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server/Sources/player_act
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Mon Jun  3 20:47:00 2013 julien edmond
** Last update Tue Jun  4 11:04:11 2013 julien edmond
*/

#include	"zappy.h"

void		prend(t_zappy *zappy, t_client *client, char **av)
{
  t_ressources	rec;
  t_square	*sq;

  if (av[1] && get_rsrc_by_name(&rec, av[1])
      && (sq = map_at(&zappy->map, client->pp->coord[X],
		      client->pp->coord[Y]))
      && (*sq)[rec])
    {
      --(*sq)[rec];
      client->pp->inv[rec] += (rec == FOOD ? 126 : 1);
      write_to_client(client, "ok\n");
    }
  else
    write_to_client(client, "ko\n");
  client->pp->delay = 7 / zappy->time_delay;
}

void		pose(t_zappy *zappy, t_client *client, char **av)
{
  t_ressources	rec;
  t_square	*sq;

  if (av[1] && get_rsrc_by_name(&rec, av[1])
      && client->pp->inv[rec] > (rec == FOOD ? 125 : 0)
      && (sq = map_at(&zappy->map, client->pp->coord[X],
		      client->pp->coord[Y])))
    {
      ++(*sq)[rec];
      client->pp->inv[rec] -= (rec == FOOD ? 126 : 1);
      write_to_client(client, "ok\n");
    }
  else
    write_to_client(client, "ko\n");
  client->pp->delay = 7 / zappy->time_delay;
}
