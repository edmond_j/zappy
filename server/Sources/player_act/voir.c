/*
** voir.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server/Sources/player_act
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Mon Jun  3 16:58:48 2013 julien edmond
** Last update Tue Jun  4 16:05:54 2013 julien edmond
*/

#include	"zappy.h"

void		desc_square(t_zappy *zappy, t_client *client, int x, int y)
{
  t_square	*sq;
  int		i;

  i = -1;
  sq = map_at(&zappy->map, x, y);
  if (player_at(zappy, x, y, client))
    write_to_client(client, " joueur");
  while (++i < NB_RESSOURCES)
    if ((*sq)[i])
      cprintf(client, " %s", ressource_name(i));
}

int	get_from_x(int orig_x, t_orientation o, int range)
{
  return (orig_x + (o == NORTH || o == WEST ? -1 : 1) * (range + 1));
}

int	get_from_y(int orig_y, t_orientation o, int range)
{
  return (orig_y + (o == NORTH || o == EAST ? -1 : 1) * (range + 1));
}

void		voir(t_zappy *zappy, t_client *client, char **av)
{
  t_vect	from;
  int	range;
  int	i;

  (void)av;
  write_to_client(client, "{");
  desc_square(zappy, client, client->pp->coord[X], client->pp->coord[Y]);
  vect_assign(&from, &client->pp->coord);
  range = -1;
  while (++range < client->pp->level)
    {
      from[X] = get_from_x(client->pp->coord[X], client->pp->orient, range);
      from[Y] = get_from_y(client->pp->coord[Y], client->pp->orient, range);
      i = -1;
      while (++i < (range + 1) * 2 + 1)
	{
	  write_to_client(client, ",");
	  desc_square(zappy, client, from[X], from[Y]);
	  from[X] -= dir_y(client->pp->orient);
	  from[Y] += dir_x(client->pp->orient);
	}
    }
  write_to_client(client, "}\n");
  client->pp->delay = 7 / zappy->time_delay;
}
