/*
** incantation.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue Jun  4 15:35:56 2013 julien edmond
** Last update Tue Jun  4 17:35:35 2013 julien edmond
*/

#include	<stdlib.h>
#include	<string.h>
#include	"zappy.h"

void		init_incant(t_zappy *zappy, t_client *client, t_incant *incant,
			    int nb_players)
{
  int		i;
  t_client	*runner;

  i = 0;
  incant->next = NULL;
  incant->ttl = 300 / zappy->time_delay;
  incant->level = client->pp->level + 1;
  bzero(incant->concerned, sizeof(t_client *) * (nb_players + 1));
  runner = zappy->clients[PLAYER];
  incant->concerned[0] = client;
  while (++i < nb_players && runner)
    {
      while (runner
	     && (runner == client
		 || !vect_eq(&runner->pp->coord, &client->pp->coord)))
	runner = runner->next;
      if ((incant->concerned[i] = runner))
	runner = runner->next;
    }
  vect_assign(&incant->coord, &client->pp->coord);
}

void		incantation(t_zappy *zappy, t_client *client,
			    __attribute__((unused)) char **av)
{
  t_incant	*incant;
  int		nb_players;

  nb_players = MAX((client->pp->level) / 2 * 2, 1);
  if ((incant = add_incant(&zappy->incants))
      && (incant->concerned = malloc(sizeof(t_client *) * (nb_players + 1))))
    init_incant(zappy, client, incant, nb_players);
  write_to_client(client, "elevation en cours\n");
  client->ttl = 300 / zappy->time_delay;
}
