/*
** avance.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server/Sources/player_act
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Mon Jun  3 15:08:03 2013 julien edmond
** Last update Tue Jun  4 11:04:44 2013 julien edmond
*/

#include	"zappy.h"

void	avance(t_zappy *zappy, t_client *client, char **av)
{
  (void)av;
  if (client->pp->orient == NORTH || client->pp->orient == SOUTH)
    {
      if ((client->pp->coord[Y] =
	   client->pp->coord[Y] + (client->pp->orient == NORTH ? -1 : 1)) < 0)
	client->pp->coord[Y] += zappy->height;
      else
	client->pp->coord[Y] %= zappy->height;
    }
  else
    {
      if ((client->pp->coord[X] =
	   client->pp->coord[X] + (client->pp->orient == WEST ? -1 : 1)) < 0)
	client->pp->coord[X] += zappy->width;
      else
	client->pp->coord[X] %= zappy->width;
    }
  write_to_client(client, "ok\n");
  client->pp->delay = 7 / zappy->time_delay;
}

void	droite(t_zappy *zappy, t_client *client, char **av)
{
  (void)zappy;
  (void)av;
  if (++client->pp->orient > 4)
    client->pp->orient = 1;
  write_to_client(client, "ok\n");
  client->pp->delay = 7 / zappy->time_delay;
}

void	gauche(t_zappy *zappy, t_client *client, char **av)
{
  (void)zappy;
  (void)av;
  if (--client->pp->orient < 1)
    client->pp->orient = 4;
  write_to_client(client, "ok\n");
  client->pp->delay = 7 / zappy->time_delay;
}
