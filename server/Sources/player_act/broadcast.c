/*
** broadcast.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server/Sources/player_act
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue Jun  4 09:39:38 2013 julien edmond
** Last update Tue Jun  4 11:04:34 2013 julien edmond
*/

#include	"zappy.h"

void		broadcast(t_zappy *zappy, t_client *client, char **av)
{
  t_client	*runner;
  int		i;

  runner = zappy->clients[PLAYER];
  while (runner)
    {
      if (runner != client)
	{
	  i = 0;
	  cprintf(runner, "message %d,", calc_dir(zappy, runner, client));
	  while (av[++i])
	    {
	      if (i > 1)
		write_to_client(runner, " ");
	      write_to_client(runner, av[i]);
	    }
	  write_to_client(runner, "\n");
	}
      runner = runner->next;
    }
  write_to_client(client, "ok\n");
  client->pp->delay = 7 / zappy->time_delay;
}
