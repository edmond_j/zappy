/*
** connect_nbr.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server/Sources/player_act
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue Jun  4 18:03:41 2013 julien edmond
** Last update Tue Jun  4 18:06:37 2013 julien edmond
*/

#include	"zappy.h"

void	connect_nbr(__attribute__((unused)) t_zappy *zappy,
		    t_client *client,
		    __attribute__((unused)) char **cmd)
{
  cprintf(client, "%d\n", client->team->max_player - client->team->nb_players);
}
