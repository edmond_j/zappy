/*
** expulse.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server/Sources/player_act
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Mon Jun  3 21:19:42 2013 julien edmond
** Last update Tue Jun  4 11:04:31 2013 julien edmond
*/

#include	"zappy.h"

void		expulse(t_zappy *zappy, t_client *client, char **av)
{
  t_client	*victim;

  (void)av;
  while ((victim = player_at(zappy, client->pp->coord[X], client->pp->coord[Y],
			     client)))
    {
      move_to(victim, client->pp->orient);
      cprintf(victim, "deplacement: %d\n", calc_dir(zappy, victim, client));
    }
  write_to_client(client, "ok\n");
  client->pp->delay = 7 / zappy->time_delay;
}
