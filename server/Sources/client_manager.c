/*
** client_manager.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Fri May 24 14:55:40 2013 julien edmond
** Last update Tue Jun  4 18:00:34 2013 julien edmond
*/

#include	<unistd.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	"zappy.h"

void	remove_client(t_client **list, t_client *client)
{
  while (*list && *list != client)
    list = &(*list)->next;
  if (*list)
    {
      *list = (*list)->next;
      destroy_client(client);
    }
}

void	move_client(t_client *client, t_client **from, t_client **to)
{
  while (*from && *from != client)
    from = &(*from)->next;
  if (*from)
    {
      *from = (*from)->next;
      while (*to)
	to = &(*to)->next;
      *to = client;
      client->next = 0;
    }
}

void	destroy_client(t_client *client)
{
  if (close(client->sock) == -1)
    perror("close");
  if (client->team)
    --client->team->nb_players;
  destroy_buff(client->input);
  destroy_buff(client->output);
  while (pop_front_cmd(&client->to_treat))
    ;
  free(client->pp);
  free(client);
}

void			replace_rec(t_zappy *zappy, t_incant *incant)
{
  static const t_square	refs[] = { {0, 1, 0, 0, 0, 0, 0},
				   {0, 1, 1, 1, 0, 0, 0},
				   {0, 2, 0, 1, 0, 2, 0},
				   {0, 1, 1, 2, 0, 1, 0},
				   {0, 1, 2, 1, 3, 0, 0},
				   {0, 1, 2, 3, 0, 1, 0},
				   {0, 2, 2, 2, 2, 2, 2} };
  t_square		*sq;
  int			i;

  sq = map_at(&zappy->map, incant->coord[X], incant->coord[Y]);
  i = -1;
  while (sq && ++i < NB_RESSOURCES)
    {
      (*sq)[i] -= refs[incant->level - 2][i];
      add_ressources(&zappy->map, i, refs[incant->level - 2][i]);
    }
}

void		handle_incant(t_zappy *zappy)
{
  t_incant	*incant;
  t_incant	*next;
  int		i;

  incant = zappy->incants;
  while (incant)
    {
      next = incant->next;
      if (!check_incant(zappy, incant))
	incant->ttl = -1;
      else
	incant->ttl = MAX(incant->ttl - 1, 0);
      if (incant->ttl <= 0)
	{
	  if (incant->ttl == 0)
	    replace_rec(zappy, incant);
	  i = -1;
	  while (incant->concerned[++i])
	    cprintf(incant->concerned[i], "niveau actuel : %d\n",
		    incant->concerned[i]->pp->level += (incant->ttl == 0));
	  remove_incant(incant, &zappy->incants);
	}
      incant = next;
    }
}
