/*
** start.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue May 21 08:35:50 2013 julien edmond
** Last update Mon Jun  3 22:57:51 2013 julien edmond
*/

#include	<unistd.h>
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<netinet/ip.h>
#include	<stdlib.h>
#include	<stdio.h>
#include	<signal.h>
#include	"zappy.h"

static int	g_quit = 0;

void		keyboard_interrupt(__attribute__((unused)) int sigil)
{
  g_quit = 1;
}

void		free_clients(t_client *client)
{
  if (client && client->next)
    free_clients(client->next);
  if (client)
    destroy_client(client);
}

int			start_server(t_zappy *zappy)
{
  int			sent;
  struct sockaddr_in	sin;

  if ((sent = socket(AF_INET, SOCK_STREAM, 0)) != -1)
    {
      sin.sin_family = AF_INET;
      sin.sin_port = htons(zappy->port);
      sin.sin_addr.s_addr = INADDR_ANY;
      if (bind(sent, (const struct sockaddr *)&sin, sizeof(sin)) == -1
	  || listen(sent, LISTEN_VAL) == -1)
	{
	  close(sent);
	  sent = -1;
	}
    }
  return (sent);
}

void		init_game(t_zappy *zappy)
{
  t_team	*runner;
  unsigned	nb_rec;

  runner = zappy->teams;
  while (runner)
    {
      runner->max_player = zappy->client_per_team;
      runner = runner->next;
    }
  nb_rec = zappy->width * zappy->height * RESSOURCES_COEF + 1;
  while (nb_rec > 0)
    {
      add_ressource(&zappy->map, nb_rec % NB_RESSOURCES);
      --nb_rec;
    }
}

int		run(t_zappy *zappy)
{
  int		serv_sock;
  int		quit;
  double	elapsed;

  quit = 0;
  if (signal(SIGINT, &keyboard_interrupt) == SIG_ERR)
    perror("signal");
  else
    {
      if (new_map(&zappy->map, zappy->width, zappy->height)
	  || (serv_sock = start_server(zappy)) == -1)
	perror("Init failed");
      else
	{
	  init_game(zappy);
	  while (!g_quit && !quit)
	    if (!(quit = client_handler(serv_sock, zappy->clients,
					&elapsed)))
	      quit = game_cycle(zappy, elapsed);
	  if (close(serv_sock) == -1)
	    perror("close");
	}
    }
  return (0);
}
