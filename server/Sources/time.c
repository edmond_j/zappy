/*
** time.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Wed May 22 09:40:22 2013 julien edmond
** Last update Sat Jun  1 09:55:29 2013 julien edmond
*/

#include	<stdio.h>
#include	<sys/time.h>
#include	"zappy.h"

double			get_time()
{
  struct timeval	tv;
  double		sent;

  if (gettimeofday(&tv, 0) == -1)
    {
      perror("gettimeofday");
      sent = -1;
    }
  else
    sent = (double)tv.tv_sec + (double)tv.tv_usec / 1000000.0;
  return (sent);
}

double	timeval_to_double(struct timeval *tv)
{
  return ((double)tv->tv_sec + (double)tv->tv_usec / 1000000.0);
}
