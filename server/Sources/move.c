/*
** move.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Mon Jun  3 21:23:18 2013 julien edmond
** Last update Mon Jun  3 21:24:05 2013 julien edmond
*/

#include	"zappy.h"

void		move_to(t_client *client, t_orientation o)
{
  if (client && client->pp)
    {
      client->pp->coord[X] += dir_x(o);
      client->pp->coord[Y] += dir_y(o);
    }
}
