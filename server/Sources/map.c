/*
** map.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Wed May 22 09:35:24 2013 julien edmond
** Last update Mon Jun  3 22:50:12 2013 julien edmond
*/

#include	<string.h>
#include	<stdlib.h>
#include	"zappy.h"

int	new_map(t_map *map, unsigned width, unsigned height)
{
  int	ret;

  if (width * height > 0
      && (map->map = malloc(sizeof(t_square) * width * height)))
    {
      bzero(map->map, sizeof(t_square) * width * height);
      map->height = height;
      map->width = width;
      ret = 0;
    }
  else
    {
      map->map = NULL;
      ret = 1;
    }
  return (ret);
}

t_square	*map_at(t_map *map, int x, int y)
{
  if (x < 0)
    x = x % map->width + map->width;
  else
    x = x % map->width;
  if (y < 0)
    y = y % map->height + map->height;
  else
    y = y % map->height;
  return (&(map->map[y * map->width + x]));
}

void		destroy_map(t_map *map)
{
  free(map->map);
}

int		dir_x(t_orientation o)
{
  int		sent;

  if (o == EAST)
    sent = 1;
  else if (o == WEST)
    sent = -1;
  else
    sent = 0;
  return (sent);
}

int		dir_y(t_orientation o)
{
  int		sent;

  if (o == SOUTH)
    sent = 1;
  else if (o == NORTH)
    sent = -1;
  else
    sent = 0;
  return (sent);
}
