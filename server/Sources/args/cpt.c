/*
** width.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server/Sources/args
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue May 21 08:57:21 2013 julien edmond
** Last update Tue May 21 09:32:23 2013 julien edmond
*/

#include	<stdio.h>
#include	<limits.h>
#include	"args.h"

t_ret		parse_cpt(t_zappy *zappy, char *str)
{
  unsigned long	tmp;
  t_ret		ret;

  if (sscanf(str, "%lu", &tmp) && tmp <= UINT_MAX)
    {
      zappy->client_per_team = tmp;
      ret = OK;
    }
  else
    ret = ERR;
  return (ret);

}
