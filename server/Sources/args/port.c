/*
** port.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server/Sources/args
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue May 21 08:55:27 2013 julien edmond
** Last update Tue May 21 09:25:37 2013 julien edmond
*/

#include	<limits.h>
#include	<stdio.h>
#include	"args.h"

t_ret			parse_port(t_zappy *zappy, char *str)
{
  unsigned int		tmp;
  t_ret			ret;

  if (sscanf(str, "%u", &tmp) && tmp <= USHRT_MAX)
    {
      zappy->port = tmp;
      ret = OK;
    }
  else
    ret = ERR;
  return (ret);
}
