/*
** team.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server/Sources/args
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue May 21 08:58:33 2013 julien edmond
** Last update Fri May 24 15:57:11 2013 julien edmond
*/

#include	<string.h>
#include	<stdlib.h>
#include	<stdio.h>
#include	"args.h"

int		add_team(t_team **list, char *name)
{
  while (*list)
    list = &(*list)->next;
  if ((*list = malloc(sizeof(t_team))))
    {
      (*list)->team_name = name;
      (*list)->nb_players = 0;
      (*list)->next = 0;
    }
  return (!(*list));
}

t_ret		parse_team(t_zappy *zappy, char *str)
{
  t_ret		ret;

  ret = NEXT;
  if (strcmp(str, "GRAPHIC")
      && !find_team(zappy->teams, str)
      && (add_team(&zappy->teams, str)))
    ret = ERR;
  return (ret);
}
