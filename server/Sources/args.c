/*
** args.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue May 21 08:34:57 2013 julien edmond
** Last update Fri May 24 14:29:58 2013 julien edmond
*/

#include	<stdlib.h>
#include	<time.h>
#include	<string.h>
#include	<stdio.h>
#include	"zappy.h"
#include	"args.h"

void		init_zappy(t_zappy *zappy)
{
  srand(time(NULL));
  zappy->port = DEFAULT_PORT;
  zappy->width = DEFAULT_WIDTH;
  zappy->height = DEFAULT_HEIGHT;
  zappy->teams = NULL;
  zappy->client_per_team = DEFAULT_CPT;
  zappy->time_delay = DEFAULT_TD;
  bzero(zappy->clients, sizeof(zappy->clients));
}

t_arg_parser		get_func(char opt, t_ret *last)
{
  static const t_opts	opts[] = { {'p', &parse_port, OK},
				   {'x', &parse_width, OK},
				   {'y', &parse_height, OK},
				   {'n', &parse_team, NEXT},
				   {'c', &parse_cpt, OK},
				   {'t', &parse_td, OK} };
  unsigned		i;
  t_arg_parser		sent;

  i = 0;
  while (i < sizeof(opts) / sizeof(t_opts) && opts[i].opt != opt)
    ++i;
  if (i < sizeof(opts) / sizeof(t_opts))
    {
      sent = opts[i].parser;
      if (last)
	*last = opts[i].ret;
    }
  else
    sent = 0;
  return (sent);
}

int			print_usage(char *prog_name)
{
  return (dprintf(2, "Usage: %s [-p port] [-x width] [-y height] "
		  "[-n team_name1 team_name2 ...] [-c start_player_by_team] "
		  "[-t time_delay]\n", prog_name));
}

int			parse_av(int ac, char **av, t_zappy *zappy)
{
  int			ret;
  t_arg_parser		parser;
  int			i;
  t_ret			last;

  i = 0;
  ret = 0;
  parser = NULL;
  last = OK;
  init_zappy(zappy);
  while (!ret && ++i < ac)
    if ((av[i][0] == '-'
	 && (!av[i][1] || av[i][2] || (parser && last != NEXT)
	     || !(parser = get_func(av[i][1], &last))))
	|| (av[i][0] != '-'
	    && (!parser || (last = parser(zappy, av[i])) == ERR)))
      ret = print_usage(av[0]);
    else if (av[i][0] != '-' && last == OK)
      parser = NULL;
  if (!ret && ((parser && last != NEXT) || zappy->height * zappy->width <= 0))
    ret = print_usage(av[0]);
  else if (!ret && !zappy->teams)
    ret = add_team(&zappy->teams, DEFAULT_TEAM);
  return (ret);
}
