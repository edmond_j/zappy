/*
** ioclient.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Fri May 24 14:27:44 2013 julien edmond
** Last update Fri May 31 10:39:44 2013 julien edmond
*/

#include	<stdio.h>
#include	<stdarg.h>
#include	<string.h>
#include	"zappy.h"

void		write_to_client(t_client *client, char *str)
{
  buff_write(client->output, str, strlen(str));
}

void		cprintf(t_client *client, const char *format, ...)
{
  char		str[256];
  va_list	ap;

  va_start(ap, format);
  if (vsnprintf(str, sizeof(str), format, ap) == -1)
    perror("vsnprintf");
  else
    write_to_client(client, str);
  va_end(ap);
}

void		write_to_all(t_client *clients, char *str)
{
  if (clients)
    {
      write_to_client(clients, str);
      write_to_all(clients->next, str);
    }
}

void		acprintf(t_client *clients, const char *format, ...)
{
  char		str[256];
  va_list	ap;

  va_start(ap, format);
  if (vsnprintf(str, sizeof(str), format, ap) == -1)
    perror("vsnprintf");
  else
    write_to_all(clients, str);
  va_end(ap);
}
