/*
** vector.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue Jun  4 16:01:29 2013 julien edmond
** Last update Tue Jun  4 16:07:56 2013 julien edmond
*/

#include	"zappy.h"

void	vect_assign(t_vect *left, t_vect *right)
{
  (*left)[X] = (*right)[X];
  (*left)[Y] = (*right)[Y];
}

int	vect_eq(t_vect *left, t_vect *right)
{
  return ((*left)[X] == (*right)[X]
	  && (*left)[Y] == (*right)[Y]);
}
