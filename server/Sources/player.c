/*
** player.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Sat Jun  1 09:29:16 2013 julien edmond
** Last update Tue Jun  4 18:07:02 2013 julien edmond
*/

#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	"zappy.h"

int	init_player_client(t_client *client, t_zappy *zappy)
{
  client->ttl = 10 * 126;
  if ((client->pp = malloc(sizeof(*client->pp))))
    {
      bzero(&client->pp->inv, sizeof(client->pp->inv));
      client->pp->inv[FOOD] = client->ttl;
      client->pp->coord[X] = rand() % zappy->width;
      client->pp->coord[Y] = rand() % zappy->height;
      client->pp->orient = rand_in(NORTH, WEST);
      client->pp->level = 1;
      client->pp->delay = 0;
    }
  else
    perror("malloc");
  return (client->pp == 0);
}

const t_act		*player_act(char *cmd)
{
  unsigned		i;
  static const t_act	acts[] = { {"avance", &avance}, {"gauche", &gauche},
				   {"droite", &droite}, {"voir", &voir},
				   {"inventaire", &inventaire},
				   {"prend", &prend}, {"pose", &pose},
				   {"expulse", &expulse},
				   {"broadcast", &broadcast},
				   {"incantation", &incantation},
				   {"connect_nbr", &connect_nbr} };
  const t_act		*sent;

  i = 0;
  while (i < sizeof(acts) / sizeof(*acts)
	 && (!cmd || strcmp(cmd, acts[i].name)))
    ++i;
  if (i < sizeof(acts) / sizeof(*acts))
    sent = &acts[i];
  else
    sent = NULL;
  return (sent);
}

int	circle_var(int val, int max)
{
  if ((val %= max) < 0)
    val += max;
  return (val);
}

t_client	*player_at(t_zappy *zappy, int x, int y,
			   t_client *except)
{
  t_client	*client;
  t_client	*sent;

  sent = NULL;
  x = circle_var(x, zappy->width);
  y = circle_var(y, zappy->height);
  client = zappy->clients[PLAYER];
  while (!sent && client)
    if (client != except
	&& circle_var(client->pp->coord[X], zappy->width) == x
	&& circle_var(client->pp->coord[Y], zappy->height) == y)
      sent = client;
    else
      client = client->next;
  return (sent);
}

int		nb_player_at(t_zappy *zappy, int x, int y)
{
  int		sent;
  t_client	*client;

  sent = 0;
  x = circle_var(x, zappy->width);
  y = circle_var(y, zappy->height);
  client = zappy->clients[PLAYER];
  while (client)
    {
      sent += (circle_var(client->pp->coord[X], zappy->width) == x
	       && circle_var(client->pp->coord[Y], zappy->height) == y);
      client = client->next;
    }
  return (sent);
}
