/*
** game.c for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue May 21 14:21:40 2013 julien edmond
** Last update Tue Jun  4 15:18:37 2013 julien edmond
*/

#include	<string.h>
#include	<stdlib.h>
#include	"zappy.h"

t_client	*none_handler(t_client *client, t_zappy *zappy)
{
  t_client	*next;
  t_team	*team;

  next = client->next;
  while (client->to_treat)
    {
      if (!strcmp(client->to_treat->cmd[0], "GRAPHIC")
	  && !init_graphic_client(client))
	move_client(client, &zappy->clients[UNLOGGED],
		    &zappy->clients[GRAPHIC]);
      else if ((team = find_team(zappy->teams, client->to_treat->cmd[0]))
	       && !add_client_to_team(client, team, zappy)
	       && !init_player_client(client, zappy))
	move_client(client, &zappy->clients[UNLOGGED],
		    &zappy->clients[PLAYER]);
      else
	--client->ttl;
      pop_front_cmd(&client->to_treat);
    }
  return (next);
}

t_client	*player_handler(t_client *client, t_zappy *zappy)
{
  t_client	*next;
  const t_act	*act;

  next = client->next;
  if (client)
    client->ttl = --client->pp->inv[FOOD];
  client->pp->delay -= (client->pp->delay > 0);
  while (client && client->to_treat && client->ttl > 0
	 && client->pp->delay <= 0)
    {
      if ((act = player_act(client->to_treat->cmd[0])))
	act->func(zappy, client, client->to_treat->cmd);
      else
	write_to_client(client, "ko\n");
      pop_front_cmd(&client->to_treat);
    }
  if (client->ttl <= 0)
    write_to_client(client, "mort\n");
  return (next);
}

t_client	*graphic_handler(t_client *client, t_zappy *zappy)
{
  t_client	*next;
  unsigned	i;

  next = client->next;
  while (client && client->to_treat)
    {
      i = 0;
      printf("Graphic:");
      while (client->to_treat->cmd[i])
	printf(" %s", client->to_treat->cmd[i++]);
      printf("\n");
      pop_front_cmd(&client->to_treat);
    }
  return (next);
}

int				game_cycle(t_zappy *zappy, double elapsed)
{
  int				i;
  t_client			*runner;
  static const t_cmd_handler	handlers[] = {&none_handler, &player_handler,
					      &graphic_handler};
  static double			time = 0;

  i = -1;
  time -= elapsed;
  handle_incant(zappy);
  while (++i < 3)
    if (i != PLAYER || time <= 0)
      {
	runner = zappy->clients[i];
	while (runner)
	  {
	    buff_to_cmd(runner->input, &runner->to_treat);
	    runner = handlers[i](runner, zappy);
	  }
      }
  if (time <= 0)
    time = 1.0 / (double)zappy->time_delay;
  return (0);
}
