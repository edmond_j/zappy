/*
** args.h for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue May 21 08:41:36 2013 julien edmond
** Last update Tue May 21 10:17:14 2013 julien edmond
*/

#ifndef		ARGS_H_
# define	ARGS_H_

# include	"zappy.h"

typedef enum
  {
    OK,
    ERR,
    NEXT
  }		t_ret;

typedef t_ret	(*t_arg_parser)(t_zappy *, char *);

t_ret		parse_port(t_zappy *, char *);
t_ret		parse_width(t_zappy *, char *);
t_ret		parse_height(t_zappy *, char *);
t_ret		parse_team(t_zappy *, char *);
t_ret		parse_cpt(t_zappy *, char *);
t_ret		parse_td(t_zappy *, char *);

int		add_team(t_team **list, char *name);

typedef struct
{
  const char		opt;
  const t_arg_parser	parser;
  const t_ret		ret;
}			t_opts;

#endif		/* !ARGS_H_ */
