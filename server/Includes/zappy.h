/*
** zappy.h for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server/Includes
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue May 21 08:23:01 2013 julien edmond
** Last update Tue Jun  4 18:04:16 2013 julien edmond
*/

#ifndef		ZAPPY_H_
# define	ZAPPY_H_

# include	<sys/time.h>
# include	"buffer.h"

# define	DEFAULT_PORT	1337
# define	DEFAULT_WIDTH	10
# define	DEFAULT_HEIGHT	10
# define	DEFAULT_TEAM	"Arachide"
# define	DEFAULT_CPT	6
# define	DEFAULT_TD	100

# define	LISTEN_VAL	20

# define	TO_SEC		0
# define	TO_USEC		100
# define	TO_DOUBLE	((double)TO_SEC + (double)TO_USEC / 1000000.0)

# ifndef	MAX
#  define	MAX(x, y)	((x) < (y) ? (y) : (x))
# endif		/* !MAX */

# define	BUFF_SIZE	4096

# define	NB_RESSOURCES	7
# define	RESSOURCES_COEF	0.5

# define	X		0
# define	Y		1

# define	OPPOSE_DIR(x)	(((x) + 2) % 4)
# define	ABS(x)		((x) < 0 ? -(x) : (x))

typedef	enum
  {
    NORTH = 1,
    EAST = 2,
    SOUTH = 3,
    WEST = 4
  }			t_orientation;

typedef	unsigned char	t_square[NB_RESSOURCES];

typedef int	t_vect[2];

typedef struct
{
  unsigned	width;
  unsigned	height;
  t_square	*map;
}		t_map;

typedef	struct	s_team
{
  char		*team_name;
  unsigned	nb_players;
  unsigned	max_player;
  struct s_team	*next;
}		t_team;

typedef struct	s_cmd
{
  char		**cmd;
  struct s_cmd	*next;
}		t_cmd;

typedef int		t_inventory[NB_RESSOURCES];

typedef struct
{
  t_vect	coord;
  t_inventory	inv;
  t_orientation	orient;
  int		level;
  int		delay;
  int		rise;
}		t_player_param;

typedef struct		s_client
{
  int			id;
  t_team		*team;
  int			sock;
  t_buff		*input;
  t_buff		*output;
  t_cmd			*to_treat;
  int			ttl;
  t_player_param	*pp;
  struct s_client	*next;
}			t_client;

typedef struct		s_incant
{
  int			ttl;
  int			level;
  t_client		**concerned;
  t_vect		coord;
  struct s_incant	*next;
}			t_incant;

typedef struct
{
  int		nb_player;
  t_square	rec;
}		t_rite;

typedef struct
{
  unsigned short	port;
  unsigned		width;
  unsigned		height;
  t_team		*teams;
  unsigned		client_per_team;
  unsigned		time_delay;
  t_map			map;
  t_client		*clients[3];
  t_incant		*incants;
}			t_zappy;

typedef struct
{
  char		*name;
  void		(*func)(t_zappy *, t_client *, char **);
}		t_act;

typedef enum
  {
    FOOD,
    LINEMATE,
    DERAUMERE,
    SIBUR,
    MENDIANE,
    PHIRAS,
    THYSTAME
  }		t_ressources;

typedef enum
  {
    UNLOGGED,
    PLAYER,
    GRAPHIC
  }		t_client_type;

typedef t_client	*(*t_cmd_handler)(t_client *, t_zappy *);

int		circle_var(int val, int max);

int		run(t_zappy *);
int		parse_av(int ac, char **argv, t_zappy *);
void		destroy_zappy(t_zappy *zappy);
void		clean_quit(int sigil);
int		client_handler(int serv_sock, t_client **clients,
			       double *elapsed);

void		destroy_client(t_client *client);

int		game_cycle(t_zappy *zappy, double elapsed);

int		new_map(t_map *map, unsigned width, unsigned height);
t_square	*map_at(t_map *map, int x, int y);
void		destroy_map(t_map *map);
int		dir_x(t_orientation o);
int		dir_y(t_orientation o);
void		move_to(t_client *client, t_orientation o);

int		calc_dir(t_zappy *zappy, t_client *from, t_client *to);

t_client	*player_at(t_zappy *zappy, int x, int y,
			   t_client *except);
int		nb_player_at(t_zappy *zappy, int x, int y);

double		get_time();
double		timeval_to_double(struct timeval *);

void		add_ressource(t_map *map, t_ressources rec);
void		add_ressources(t_map *map, t_ressources rec, int nb);
const char	*ressource_name(t_ressources rec);
int		get_rsrc_by_name(t_ressources *rec, const char *name);
int		rec_ge(t_square *left, const t_square *right);

void		add_cmd(t_cmd **cmd, char *str);
t_cmd		*pop_front_cmd(t_cmd **cmd);

void		buff_to_cmd(t_buff *buff, t_cmd **cmd);

void		write_to_client(t_client *client, char *str);
void		write_to_all(t_client *clients, char *str);
void		cprintf(t_client *client, const char *format, ...);
void		acprintf(t_client *clients, const char *format, ...);

void		remove_client(t_client **list, t_client *client);
void		move_client(t_client *client, t_client **from, t_client **to);

t_team		*find_team(t_team *teams, char *name);
int		add_client_to_team(t_client *client, t_team *team,
				   t_zappy *zappy);

int		init_graphic_client(t_client *client);
int		init_player_client(t_client *client, t_zappy *zappy);

t_incant	*add_incant(t_incant **list);
int		check_incant(t_zappy *zappy, t_incant *incant);
void		remove_incant(t_incant *incant, t_incant **list);
void		destroy_incant(t_incant *list);
void		handle_incant(t_zappy *zappy);

int		rand_in(int min, int max);

void		vect_assign(t_vect *left, t_vect *right);
int		vect_eq(t_vect *left, t_vect *right);

const t_act	*player_act(char *cmd);
void		avance(t_zappy *zappy, t_client *client, char **cmd);
void		gauche(t_zappy *zappy, t_client *client, char **cmd);
void		droite(t_zappy *zappy, t_client *client, char **cmd);
void		voir(t_zappy *zappy, t_client *client, char **cmd);
void		inventaire(t_zappy *zappy, t_client *client, char **cmd);
void		prend(t_zappy *zappy, t_client *client, char **cmd);
void		pose(t_zappy *zappy, t_client *client, char **cmd);
void		expulse(t_zappy *zappy, t_client *client, char **cmd);
void		broadcast(t_zappy *zappy, t_client *client, char **cmd);
void		incantation(t_zappy *zappy, t_client *client, char **cmd);
void		connect_nbr(t_zappy *zappy, t_client *client, char **cmd);

#endif		/* !ZAPPY_H_ */
