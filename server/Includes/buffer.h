/*
** buffer.h for zappy in /home/edmond_j//rendu/B4/systemUnix/zappy/zappy_git/server
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Tue May 21 10:42:37 2013 julien edmond
** Last update Tue May 21 13:50:34 2013 julien edmond
*/

#ifndef		BUFFER_H_
# define	BUFFER_H_

typedef struct
{
  int		size;
  int		start;
  int		end;
  char		*buf;
}		t_buff;

t_buff		*new_buff(int size);
void		destroy_buff(t_buff *);

char		*buff_readline(t_buff *);
void		buff_write(t_buff *, char *data, int size);

int		buff_size(t_buff *);

int		read_to_buff(t_buff *, int fd);
int		write_from_buff(t_buff *, int fd);

#endif		/* !BUFFER_H_ */
