#-------------------------------------------------
#
# Project created by QtCreator 2013-05-21T09:21:26
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


TARGET = zappy
TEMPLATE = app

OBJECTS_DIR = obj
MOC_DIR = moc
UI_DIR = ui_make
RCC_DIR = rcc_make

LIBS += -lGLU -lpng

SOURCES += Sources/main.cpp\
        Sources/splashscreen.cpp \
    Sources/comm.cpp \
    Sources/CircularBuffer.cpp \
    Sources/window.cpp \
    Sources/basicusagescene.cpp \
    Sources/skybox.cpp \
    Sources/Vector3f.cpp \
    Sources/CameraFreeFly.cpp \
    Sources/CameraFollow.cpp \
    Sources/Util.cpp

HEADERS  += Includes/splashscreen.h \
    Includes/comm.h \
    Includes/Encapsulations/MyThread.hpp \
    Includes/Encapsulations/MySocket.hpp \
    Includes/Encapsulations/MyScopelock.hpp \
    Includes/Encapsulations/MySafeQueue.hpp \
    Includes/Encapsulations/MyMutex.hpp \
    Includes/CircularBuffer.hpp \
    Includes/window.h \
    Includes/abstractscene.h \
    Includes/basicusagescene.h \
    Includes/skybox.h \
    Includes/Vector3f.hh \
    Includes/ACamera.hpp \
    Includes/CameraFreeFly.hh \
    Includes/CameraFollow.hh \
    Includes/Util.hpp



FORMS    += ui/splashscreen.ui

RESOURCES += \
    Assets/Assets.qrc

QMAKE_CXXFLAGS = -W -Wall -Wextra -Werror -I./Includes -I./Includes/Encapsulations

re.commands = $(MAKE) distclean qmake all
QMAKE_EXTRA_TARGETS += re

fclean.commands = $(MAKE) distclean qmake
QMAKE_EXTRA_TARGETS += fclean

