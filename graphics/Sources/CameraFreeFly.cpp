//
// CameraFreeFly.cpp for bomberman in /home/olivie_d//depotsvn/bomberman-2016-lequeu_m/Classes/Graphics
//
// Made by david olivier
// Login   <olivie_d@epitech.net>
//
// Started on  Tue May 28 15:03:35 2013 david olivier
// Last update Wed Jun  5 12:37:15 2013 david olivier
//

#include			"CameraFreeFly.hh"

CameraFreeFly::CameraFreeFly(const Vector3f& position)
  : ACamera(position)
{

}

CameraFreeFly::CameraFreeFly(const CameraFreeFly& copy)
  : ACamera(copy)
{

}

CameraFreeFly::~CameraFreeFly()
{
}


void				CameraFreeFly::mouseChange(QMouseEvent * ev)
{
int				posX_tmp;
int				posY_tmp;

    if (this->_prevMousePos.x() != -1 && this->_prevMousePos.y() != -1)
{
    posX_tmp = (float)(this->_prevMousePos.x() - ev->pos().x()) ;
    posY_tmp = (float)(this->_prevMousePos.y() - ev->pos().y()) ;
    this->_theta -= posX_tmp * this->_sensivity;
    this->_phi += posY_tmp * this->_sensivity;
    this->vectorsFromAngles();
}
    this->_prevMousePos = ev->pos();
}

void				CameraFreeFly::keyEventForward(float elapsed, bool boost)
{
  this->_position += this->_forward * (((boost) ? (10 * this->_speed) : (this->_speed)) * elapsed);
}

void				CameraFreeFly::keyEventBack(float elapsed, bool boost)
{
  this->_position -= this->_forward * (((boost) ? (10 * this->_speed) : (this->_speed)) * elapsed);
}

void				CameraFreeFly::keyEventLeft(float elapsed, bool boost)
{
  this->_position += this->_left * (((boost) ? (10 * this->_speed) : (this->_speed)) * elapsed);
}

void				CameraFreeFly::keyEventRight(float elapsed, bool boost)
{
  this->_position -= this->_left * (((boost) ? (10 * this->_speed) : (this->_speed)) * elapsed);
}

void				CameraFreeFly::keyEventUp(float elapsed, bool boost)
{
  double				timeBeforeStoppingVerticalMotion = 250;
  double				timestep = elapsed;

  if (timestep <= timeBeforeStoppingVerticalMotion)
    timeBeforeStoppingVerticalMotion -= timestep;
  this->_position += Vector3f(0.0f, 1 * ((boost) ? (10 * this->_speed) : (this->_speed)) * timestep, 0.0f);
}

void				CameraFreeFly::keyEventDown(float elapsed, bool boost)
{
  double				timeBeforeStoppingVerticalMotion = 250;
  double				timestep = elapsed;

  if (timestep <= timeBeforeStoppingVerticalMotion)
    timeBeforeStoppingVerticalMotion -= timestep;
  this->_position += Vector3f(0.0f, -1 * ((boost) ? (10 * this->_speed) : (this->_speed)) * timestep, 0.0f);
}

void				CameraFreeFly::update()
{
}

