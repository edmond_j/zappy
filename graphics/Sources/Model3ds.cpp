//
// Model3DS.cpp for zappy in /home/crouze_t//Rendu/Tek2/SystemUnix/zappy-2016-lequeu_m
//
// Made by thomas crouzet
// Login   <crouze_t@epitech.net>
//
// Started on  Tue Jul  2 16:25:13 2013 thomas crouzet
// Last update Fri Jul  5 16:14:19 2013 martin lequeux-gruninger
//

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <QImage>
#include <Qt/qapplication.h>
#include <QGLWidget>
#include "Model3ds.hh"

Model::Model()
{
  this->_curFrame = 0; // current frame of our model
  if (glIsEnabled(GL_LIGHTING))
    this->_lightEnabled = true;
  else
    this->_lightEnabled = false;
}

Model::~Model()
{
  unsigned int	i;

  if (this->_file) // if the file isn't freed yet
    lib3ds_file_free(this->_file); //free up memory
  //disable texture generation
  for (i = 0; i < this->_textureIndices.size(); i++)
    glDeleteTextures(1, &this->_textureIndices.at(i));
}

void		Model::LoadFile(const char *name)
{
  std::string	event;
  std::string	online;
  Lib3dsMesh	*mesh;


  this->_filename = name;
  this->_file = lib3ds_file_load(this->_filename);
  if (this->_file == NULL)
    {
      event = "Error loading: ";
      online = "On line 48 in file ";
      online.append(__FILE__);
      event.append(this->_filename);
      std::cout << event << std::endl;
      std::cout << online << std::endl;
      exit(1);
    }
  lib3ds_file_eval(this->_file, 0); // set current frame to 0
  // apply texture to all meshes that have texels
 for (mesh = this->_file->meshes; mesh != 0; mesh = mesh->next)
   {
     if (mesh->texels) //if there's texels for the mesh
       ApplyTexture(mesh); //then apply texture to it
   }
  if (this->_file->lights) //if we have lights in our model
    CreateLightList();
}

void		Model::CreateLightList()
{
  Lib3dsLight	*light;
  GLint		curlight = GL_LIGHT0;
  GLfloat	diff[4];
  GLfloat	amb[4];
  GLfloat	pos[4];
  int		j;

  curlight = GL_LIGHT0;
  this->_lightListIndex = glGenLists(1);
  glNewList(this->_lightListIndex, GL_COMPILE_AND_EXECUTE);
  for (light = this->_file->lights; light != NULL; light = light->next)
    {
      if (!light->off)
        {
          for (j = 0; j < 3; j++)
            {
              diff[j] = light->color[j];
              amb[j] = light->color[j] / 4.5; // We might wanna change this;
              pos[j] = light->position[j];
            }
          diff[3] = amb[3] = pos[3] = 1.0;
          curlight++;
          // Start setting light
          glLightfv(GL_LIGHT0, GL_DIFFUSE, diff); //set the diffuse color
          glLightfv(GL_LIGHT0, GL_POSITION, pos); //set the position of the light
          glLightfv(GL_LIGHT0, GL_AMBIENT, amb); // set the ambient color of the light
          glLightfv(GL_LIGHT0, GL_SPECULAR, diff); // set the specular color of the light
          if (light->spot) // if it's a light spot
            {
              for (j = 0; j <= 3; j++) // get position of the light
                pos[j] = light->spot[j] - light->position[j];
              glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, pos); //specify that we have a spot at position 'pos'
            }
        }
    }
  glEndList();
}

// what is basicly does is, set the properties of the texture for our mesh
void		Model::ApplyTexture(Lib3dsMesh *mesh)
{
  Lib3dsMaterial	*mat;
  Lib3dsFace	*f;
  QImage	img;
  GLuint	tmpIndex;
  bool		found;
  unsigned int	i;
  unsigned int  j;

  for (i = 0; i < mesh->faces; ++i)
    {
      f = &mesh->faceL[i];
      found = false;
      if (f->material != NULL)
        {
          mat = lib3ds_file_material_by_name(this->_file, f->material);
          for (j = 0; j < this->_textureFilenames.size(); j++)
            {
              if (this->_textureFilenames.at(j) == mat->texture1_map.name)
                {
                  this->_textureIndices.push_back(this->_textureIndices.at(j));
                  this->_textureFilenames.push_back(mat->texture1_map.name);
                  found = true;
                  j = this->_textureFilenames.size();
                }
            }
          if (found)
            {
              this->_textureFilenames.push_back(mat->texture1_map.name);
              if (!img.load(mat->texture1_map.name))
                {
                  std::cerr << "3DS error" << std::endl;
                  std::cerr << "Error loading 3ds texture, perhaps file format not supported?" << std::endl;
                  exit(1);
                }
              img = QGLWidget::convertToGLFormat(img);
              glGenTextures(1, &tmpIndex); // allocate memory for one texture
              this->_textureIndices.push_back(tmpIndex); // add the index of our newly created texture to textureIndices
              glBindTexture(GL_TEXTURE_2D, tmpIndex); // use our newest texture
              gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, img.width() , img.height(), GL_RGBA, GL_UNSIGNED_BYTE, img.bits()); // genereate MipMap levels for our texture
              glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // give the best result for texture magnification
              glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //give the best result for texture minification
              glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP); // don't repeat texture
              glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP); // don't repeat texture
            }
        }
    }
}

// this code is rewritten from player.c example that came with lib3ds
// what it does is render a node from our model
void		Model::RenderNode(Lib3dsNode *node)
{
  Lib3dsNode	*tmp;
  Lib3dsMesh	*mesh;

  ASSERT(this->_file); //this is for debugging
  {
    for (tmp = node->childs; tmp != 0; tmp = tmp->next)
      RenderNode(tmp); //render all child nodes of this note
  }
  if (node->type == LIB3DS_OBJECT_NODE) //check wheter the node is a 3ds node
    {
      if (!node->user.d) //Wheter we have a list or not, if not we're gonna create one
        {
          if (!(mesh = lib3ds_file_mesh_by_name(this->_file, node->name))) //get all the meshes of the current node
            return;
          ASSERT(mesh); //for debugging in case we don't have a mesh
          node->user.d = glGenLists(1); //alocate memory for one list
          /////////////////////////////////////////////////////////////
          if (glGetError() != GL_NO_ERROR)
            {
              std::cerr << "ERROR!\n" << std::endl;
              qApp->exit(0);
            }
          /////////////////////////////////////////////////////////////
          glNewList(node->user.d, GL_COMPILE); //here we create our list
          {
            unsigned p;
            Lib3dsVector *normals;
            normals = static_cast<float(*)[3]> (std::malloc (3*sizeof(Lib3dsVector)*mesh->faces)); //alocate memory for our normals
            {
              Lib3dsMatrix m;
              lib3ds_matrix_copy(m, mesh->matrix); //copy the matrix of the mesh in our temporary matrix
              lib3ds_matrix_inv(m);
              glMultMatrixf(&m[0][0]); //adjust our current matrix to the matrix of the mesh
            }
            lib3ds_mesh_calculate_normals(mesh, normals); //calculate the normals of the mesh
            int j = 0;
            for (p = 0;p < mesh->faces;p++)
              {
                Lib3dsFace *f = &mesh->faceL[p];
                Lib3dsMaterial *mat=0;
                if (f->material[0]) //if the face of the mesh has material properties
                  mat = lib3ds_file_material_by_name(this->_file, f->material); //read material properties from file
                if (mat) //if we have material
                  {
                    static GLfloat ambient[4] = { 0.0, 0.0, 0.0, 1.0 };
                    glMaterialfv(GL_FRONT, GL_AMBIENT, ambient); // Ambient color
                    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat->diffuse); //diffuse color
                    glMaterialfv(GL_FRONT, GL_SPECULAR, mat->specular); //specular color
                    float shine;
                    shine = pow(2, 10.0 * mat->shininess);
                    if (shine > 128.0)
                      shine = 128.0;
                    glMaterialf(GL_FRONT, GL_SHININESS, shine);
                  }
                else // if we do not have material properties, we have to set them manually
                  {
                    GLfloat diff[4] = { 0.75, 0.75, 0.75, 1.0 }; // color: white/grey
                    GLfloat amb[4] = { 0.25, 0.25, 0.25, 1.0 }; //color: black/dark gray
                    GLfloat spec[4] = { 0.0, 0.0, 0.0, 1.0 }; //color: completly black
                    glMaterialfv(GL_FRONT, GL_DIFFUSE, diff);
                    glMaterialfv(GL_FRONT, GL_AMBIENT, amb);
                    glMaterialfv(GL_FRONT, GL_AMBIENT, spec);
                  }
                {
                  if (mesh->texels)
                    {
                      glBindTexture(GL_TEXTURE_2D, this->_textureIndices.at(j));
                      j++;
                    }
                  glBegin(GL_TRIANGLES);
                  for (int i = 0;i < 3;i++)
                    {
                      glNormal3fv(normals[3*p+i]); //set normal vector of that point
                      if (mesh->texels)
                        glTexCoord2f(mesh->texelL[f->points[i]][0], mesh->texelL[f->points[i]][1]);
                      glVertex3fv(mesh->pointL[f->points[i]].pos); //Draw the damn triangle
                    }
                  glEnd();
                }
              }
            free(normals); //free up memory
          }
          glEndList(); // end of list
        }
      if (node->user.d) // if we have created a link list(with glNewList)
        {
          Lib3dsObjectData *tmpdat;
          glPushMatrix(); //save transformation values
          tmpdat = &node->data.object; // get the position data
          glMultMatrixf(&node->matrix[0][0]); //adjust matrix according to the node
          glTranslatef(-tmpdat->pivot[0], -tmpdat->pivot[1], -tmpdat->pivot[2]); //move to the right place;
          glCallList(node->user.d); //render node
          glPopMatrix(); //return transformation original values
        }
    }
}

// this function actually renders the model at place (x, y, z) and then rotated around the y axis by 'angle' degrees
void		Model::RenderModel()
{
  Lib3dsNode	*nodes;

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  if (this->_file->lights) //if we have lights in the model
    {
      EnableLights(); //enable all lights
      glCallList(this->_lightListIndex); //set lights.
    }
  for (nodes = this->_file->nodes; nodes != NULL; nodes = nodes->next) // Render all nodes
    RenderNode(nodes);
  if (this->_file->lights)
    DisableLights(); // disable lighting, we don't want it have it enabled longer than necessary
  this->_curFrame++;
  if (this->_curFrame > this->_file->frames) //if the next frame doesn't exist, go to frame 0
    this->_curFrame = 0;
  lib3ds_file_eval(this->_file, this->_curFrame); // set current frame
  glDisable(GL_CULL_FACE);
}

void		Model::EnableLights()
{
  GLuint	lightNum = GL_LIGHT0;
  Lib3dsLight	*light;

  glEnable(GL_LIGHTING);
  for (light = this->_file->lights; light != NULL; light = light->next)
    {
      if (!glIsEnabled(lightNum))
        glEnable(lightNum);
      lightNum++;
    }
}

void		Model::DisableLights()
{
  GLuint	lightNum = GL_LIGHT0;
  Lib3dsLight	*light;

  glDisable(GL_LIGHTING);
  for (light = this->_file->lights; light != NULL; light = light->next)
    {
      if (glIsEnabled(lightNum))
        glDisable(lightNum);
      lightNum++;
    }
}

Lib3dsFile	*Model::Get3DSPointer()
{
  return (this->_file);
}

std::string	Model::GetFilename()
{
  return (this->_filename);
}
