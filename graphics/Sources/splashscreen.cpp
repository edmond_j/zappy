#include "splashscreen.h"
#include "ui_splashscreen.h"
#include "window.h"

SplashScreen::SplashScreen(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SplashScreen)
{
    ui->setupUi(this);
}

SplashScreen::~SplashScreen()
{
    delete ui;
}

void SplashScreen::connectUser(const std::string & ip,
                               const std::string & port)
{
    _comm = new Comm(ip, port, this);

    try
      {
        _comm->connectMe();
        std::cout << "Connecting" << std::endl;
      }
    catch (const MySocket::SocketException & e)
      {
        QMessageBox *box = new QMessageBox(this);
        box->setText(e.what());
        box->show();
        delete _comm;
        _comm = NULL;
        return;
      }
    QObject::connect(_comm, SIGNAL(updateButton(const QString &)), this, SLOT(updateButton(const QString &)));
    QObject::connect(_comm, SIGNAL(error(const QString &)), this, SLOT(error(const QString &)));
    QObject::connect(_comm, SIGNAL(msz(int, int)), this, SLOT(msz(int, int)));

    _comm->launchLoop();
}

void SplashScreen::updateButton(const QString & text)
{
    ui->connect->setText(text);

    //DEBUG PART
    if (text == DISCONNECT)
    {
        ui->debugSendButton->setEnabled(true);
        ui->debugSendLine->setEnabled(true);
    }
    if (text == CONNECT)
    {
        ui->debugSendButton->setEnabled(false);
        ui->debugSendLine->setEnabled(false);
    }
}

void SplashScreen::error(const QString & text)
{
    QMessageBox *box = new QMessageBox(this);
    box->setText(text);
    box->show();
}

void SplashScreen::on_connect_clicked()
{
    if (ui->connect->text() == CONNECT)
        this->connectUser(ui->ip->text().toStdString(), ui->port->text().toStdString());
    else if (ui->connect->text() == DISCONNECT)
        this->_comm->disconnect();
}

void SplashScreen::on_debugSendButton_clicked()
{
    this->_comm->addCommand(ui->debugSendLine->text().toStdString());
}

void SplashScreen::on_button3D_clicked()
{
    Window *w = new Window(10, 10, this->_comm);
    w->show();
}

void SplashScreen::msz(int width, int height)
{
    Window *w = new Window(width, height, this->_comm);
    w->show();
}
