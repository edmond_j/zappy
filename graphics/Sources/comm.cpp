#include "comm.h"
#include "Util.hpp"

Comm::Comm(const std::string & ip,
           const std::string & port,
           QObject *parent) :
    QObject(parent),
    MySocket(ip, port),
    _loopThread(&Comm::serverLoop, this, NULL)
{
    _ready = true;
    _map["BIENVENUE"] = &Comm::BIENVENUE;
    _map["msz"] = &Comm::msz;
    _map["bct"] = &Comm::bct;
    _map["tna"] = &Comm::tnaf;
    _map["pnw"] = &Comm::pnw;
    _map["ppo"] = &Comm::ppo;
    _map["plv"] = &Comm::plv;
    _map["pin"] = &Comm::pin;
    _map["pex"] = &Comm::pex;
    _map["pbc"] = &Comm::pbc;
    _map["pic"] = &Comm::pic;
    _map["pie"] = &Comm::pie;
    _map["pfk"] = &Comm::pfk;
    _map["pdr"] = &Comm::pdr;
    _map["pgt"] = &Comm::pgt;
    _map["pdi"] = &Comm::pdi;
    _map["enw"] = &Comm::enw;
    _map["eht"] = &Comm::eht;
    _map["ebo"] = &Comm::ebo;
    _map["edi"] = &Comm::edi;
    _map["sgt"] = &Comm::sgt;
    _map["seg"] = &Comm::seg;
    _map["smg"] = &Comm::smg;
    _map["suc"] = &Comm::suc;
    _map["sbp"] = &Comm::sbp;
}

Comm::~Comm()
{
  _loopActive = false;
  FD_ZERO(&this->_fds.write);
  FD_ZERO(&this->_fds.read);
}

void              Comm::ready()
{
    this->_ready = true;

}

bool    Comm::getConnected() const
{
    return (this->_loopActive);
}

void    Comm::disconnect()
{
    this->_loopActive = false;
}

void    Comm::BIENVENUE(const std::string &msg)
{
    (void)msg;
    this->_commands.push("GRAPHIC");
}

void    Comm::msz(const std::string & msg)
{
    std::string stream(msg);

    Util::getNext(stream, " ");
    int w = Util::fromString<int>(Util::getNext(stream, " "));
    int h = Util::fromString<int>(Util::getNext(stream, " "));
    this->_ready = false;
    emit msz(w, h);
}

void              Comm::bct(const std::string &msg)
{
    std::string stream(msg);

    Util::getNext(stream, " ");
    int x = Util::fromString<int>(Util::getNext(stream, " "));
    int y = Util::fromString<int>(Util::getNext(stream, " "));
    int a = Util::fromString<int>(Util::getNext(stream, " "));
    int b = Util::fromString<int>(Util::getNext(stream, " "));
    int c = Util::fromString<int>(Util::getNext(stream, " "));
    int d = Util::fromString<int>(Util::getNext(stream, " "));
    int e = Util::fromString<int>(Util::getNext(stream, " "));
    int f = Util::fromString<int>(Util::getNext(stream, " "));
    int g = Util::fromString<int>(Util::getNext(stream, " "));
    emit bct(x, y, a, b, c, d, e, f, g);
}

void              Comm::tnaf(const std::string &)
{
}

void              Comm::pnw(const std::string &)
{
}

void              Comm::ppo(const std::string &)
{
}

void              Comm::plv(const std::string &)
{
}

void              Comm::pin(const std::string &)
{
}

void              Comm::pex(const std::string &)
{
}

void              Comm::pbc(const std::string &)
{
}

void              Comm::pic(const std::string &)
{
}

void              Comm::pie(const std::string &)
{
}

void              Comm::pfk(const std::string &)
{
}

void              Comm::pdr(const std::string &)
{
}

void              Comm::pgt(const std::string &)
{
}

void              Comm::pdi(const std::string &)
{
}

void              Comm::enw(const std::string &)
{
}

void              Comm::eht(const std::string &)
{
}

void              Comm::ebo(const std::string &)
{
}

void              Comm::edi(const std::string &)
{
}

void    Comm::sgt(const std::string & msg)
{
    std::string stream(msg);
    (void)msg;

    Util::getNext(stream, " ");
    int t = Util::fromString<int>(Util::getNext(stream, " "));
    emit sgt(t);
    std::cout << "EMITTING SGT " << t << std::endl;
}

void              Comm::seg(const std::string &)
{
}

void              Comm::smg(const std::string &)
{
}

void              Comm::suc(const std::string &)
{
}

void              Comm::sbp(const std::string &)
{

}

int     Comm::eval(const std::string & msg)
{
    std::cout << "J'evalue "<< msg << std::endl;

    for (std::map<std::string, funcPtr>::iterator it = this->_map.begin();
         it != this->_map.end();
         ++it)
        if (msg.compare(0, it->first.size(), it->first) == 0)
            (this->*(it->second))(msg);
    return (0);
}

int     Comm::reader(int fd)
{
    std::cout << "READY IS" << std::endl;
   if (MySocket::FullRead(fd) <= 0)
    {
      std::cout << "Bye ;) - Connection closed by server on socket " << fd << std::endl;
      if (MySocket::MyClose(fd))
        std::cerr << "Warning - close" << std::endl;
      FD_CLR(fd, &this->_fds.read);
      return (-1);
    }
  return (0);
}

int    Comm::readHandler()
{
  for (int i = 0; i <= this->_fds.nfds; ++i)
    if (FD_ISSET(i, &this->_fds.read_tmp) && !FD_ISSET(i, &this->_fds.write_tmp))
      return (this->reader(i));

  std::string res;
  while (this->_ready == true && this->_buffer.getLine(res) != false)
      this->eval(res);
  return (0);
}

int     Comm::writer(int fd)
{
    if (this->_commands.empty())
    {
        std::cerr << "Empty queue" << std::endl;
        return (0);
    }
    std::string command = this->_commands.pop();
    command += "\n";
    std::cout << "SENDING " << command << std::endl;
    if (write(fd, command.c_str(), command.length()) == -1)
    {
        std::cerr << "Error Write" << std::endl;
        return (-1);
    }
    return (0);
}

int    Comm::writeHandler()
{
  for (int i = 0; i <= this->_fds.nfds; ++i)
    if (FD_ISSET(i, &this->_fds.write_tmp))
      return (this->writer(i));
  return (0);
}

void    Comm::launchLoop()
{
  _loopActive = true;
  _loopThread.run();
}

int    Comm::serverLoop(void *)
{
  struct timeval	t;

  emit updateButton(DISCONNECT);
  while (0x5F3759DF && this->_loopActive)
    {
      FD_ZERO(&this->_fds.write);
      if (this->_commands.size() > 0)
        FD_SET(this->_sock_fd, &this->_fds.write);
      t.tv_sec = 0;
      t.tv_usec = TIMEOUT;
      this->_fds.read_tmp = this->_fds.read;
      this->_fds.write_tmp = this->_fds.write;
      if (MySocket::MySelect(this->_fds.nfds + 1, &this->_fds.read_tmp,
                             &this->_fds.write_tmp, NULL, &t) == -1)
        {
          emit error("Select - Disconnected by Server");
          updateButton(CONNECT);
          return (-1);
        }
      if (this->readHandler() == -1)
        {
          emit error("Read - Disconnected by Server");
          updateButton(CONNECT);
          return (-1);
        }
      if (this->writeHandler() == -1)
        {
          emit error("Write Failure - Aborting");
          updateButton(CONNECT);
          return (-1);
        }
    }
  std::cout << "Disconnecting" << std::endl;
  updateButton(CONNECT);
  return (0);
}

void    Comm::addCommand(const std::string &command)
{
    this->_commands.push(command);
}
