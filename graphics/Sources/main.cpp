#include <QtGui/QApplication>
#include "splashscreen.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    SplashScreen w;
    w.show();

    return a.exec();
}
