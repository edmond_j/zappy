//
// Vector3f.cpp for gdl in /home/lequeu_m/
//
// Made by martin lequeux-gruninger
// Login   <lequeu_m@epitech.net>
//
// Started on  Mon May  6 18:20:14 2013 martin lequeux-gruninger
// Last update Fri Jun 28 16:46:22 2013 martin lequeux-gruninger
//

#include		"Vector3f.hh"

Vector3f::Vector3f(void)
  : x(0.0f), y(0.0f), z(0.0f)
{
}

Vector3f::Vector3f(float x, float y, float z)
  : x(x), y(y), z(z)
{
}

Vector3f::Vector3f(const Vector3f& v)
  : x(v.x), y(v.y), z(v.z)
{
}

Vector3f::Vector3f(const Vector3f& from, const Vector3f& to)
  : x(to.x - from.x), y(to.y - from.y), z(to.z - from.z)
{
}

Vector3f&	Vector3f::operator=(const Vector3f& v)
{
  if (this != &v)
    {
      this->x = v.x;
      this->y = v.y;
      this->z = v.z;
    }
  return (*this);
}

bool	Vector3f::operator==(const Vector3f& v) const
{
  return (this->x == v.x && this->y == v.y && this->z == v.z);
}

bool	Vector3f::operator<(const Vector3f& v) const
{
  float me = 1.0/2.0 * (this->x + this->y) * (this->x + this->y + 1) + this->y;
  float other= 1.0/2.0 * (v.x + v.y) * (v.x + v.y + 1) + v.y;

  return (me < other);
}

bool	Vector3f::operator>(const Vector3f& v) const
{
    float me = 1.0/2.0 * (this->x + this->y) * (this->x + this->y + 1) + this->y;
    float other= 1.0/2.0 * (v.x + v.y) * (v.x + v.y + 1) + v.y;

    return (me > other);
}

Vector3f&	Vector3f::operator+=(const Vector3f& v)
{
  if (this != &v)
    {
      this->x += v.x;
      this->y += v.y;
      this->z += v.z;
    }
  return (*this);
}

Vector3f	Vector3f::operator+(const Vector3f& v) const
{
  Vector3f	t(*this);

  t += v;
  return (t);
}

Vector3f&	Vector3f::operator-=(const Vector3f& v)
{
  if (this != &v)
    {
      this->x -= v.x;
      this->y -= v.y;
      this->z -= v.z;
    }
  return (*this);
}

Vector3f	Vector3f::operator-(const Vector3f & v) const
{
  Vector3f	t(*this);

  t -= v;
  return (t);
}

Vector3f&	Vector3f::operator*=(const double a)
{
  this->x *= a;
  this->y *= a;
  this->z *= a;
  return (*this);
}

Vector3f	Vector3f::operator* (const double a) const
{
  Vector3f	t(*this);

  t *= a;
  return (t);
}

Vector3f&	Vector3f::operator/=(const double a)
{
  if (a != 0.0)
    {
      this->x /= a;
      this->y /= a;
      this->z /= a;
    }
  return (*this);
}

Vector3f	Vector3f::operator/(const double a) const
{
  Vector3f	t(*this);

  t /= a;
  return (t);
}

Vector3f	Vector3f::crossProduct(const Vector3f& v) const
{
  Vector3f	t;

  t.x = (this->y * v.z) - (this->z * v.y);
  t.y = (this->z * v.x) - (this->x * v.z);
  t.z = (this->x * v.y) - (this->y * v.x);
  return (t);
}

double			Vector3f::dotProduct(const Vector3f& v) const
{
    return ((this->x * v.x) + (this->y * v.y) + (this->z * v.z));
}

double			Vector3f::length() const
{
  return (sqrt((this->x * this->x) + (this->y * this->y) + (this->z * this->z)));
}

int			Vector3f::distance(const Vector3f& v) const
{
  return (sqrt(((int)((int)v.x - (int)this->x) * ((int)v.x - (int)this->x)) +
	       ((int)((int)v.z - (int)this->z) * ((int)v.z - (int)this->z))));
}

Vector3f&	Vector3f::normalize()
{
  double		lenght_tmp = this->length();

  if (lenght_tmp != 0.0)
    (*this) /= lenght_tmp;
  return (*this);
}

void			Vector3f::setValues(double x, double y, double z)
{
  this->x = x;
  this->y = y;
  this->z = z;
}
