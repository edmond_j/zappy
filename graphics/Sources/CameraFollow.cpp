//
// CameraFreeFly.cpp for bomberman in /home/olivie_d//depotsvn/bomberman-2016-lequeu_m/Classes/Graphics
//
// Made by david olivier
// Login   <olivie_d@epitech.net>
//
// Started on  Tue May 28 15:03:35 2013 david olivier
// Last update Wed Jun  5 12:37:15 2013 david olivier
//

#include			"CameraFollow.hh"

CameraFollow::CameraFollow(const Vector3f& position)
    : ACamera(position)
{

}

CameraFollow::CameraFollow(const CameraFollow& copy)
    : ACamera(copy)
{

}

CameraFollow::~CameraFollow()
{
}

void				CameraFollow::update()
{
    static float toPi = 0.0174532925;

    float dist = 100 + (50 + this->_len)  * cos(this->_phi * toPi);

    this->_position.x = sin(this->_theta * toPi) * dist;
    this->_position.y = cos(this->_theta * toPi) * dist;
    this->_position.z = (50 + this->_len) * sin(this->_phi * toPi);

    this->_forward.x = (sin(this->_theta * toPi) * (125)) - this->_position.x ;
    this->_forward.y = (cos(this->_theta * toPi) * (125)) - this->_position.y  ;
    this->_forward.z =  0 - this->_position.z;
    this->_forward.normalize();
     this->_target = this->_position + this->_forward;
}


void				CameraFollow::mouseChange(QMouseEvent * ev)
{

        if ((ev->buttons() == Qt::LeftButton) ||
            (ev->buttons() == Qt::RightButton) ||
            (ev->buttons() == Qt::MiddleButton))
    {
        if (this->_prevMousePos.x() != -1 && this->_prevMousePos.y() != -1)
        {
            this->_theta += (float)(this->_prevMousePos.x() - ev->pos().x()) / 10.0f;
            if (this->_theta > 360.0)
                this->_theta -= 360.0;
            if (this->_theta < 0.0)
                this->_theta += 360.0;

            this->_phi += (float)(this->_prevMousePos.y() - ev->pos().y()) / 10.0f;
            if (this->_phi > 360.0)
                this->_phi -= 360.0;
            if (this->_phi < 0.0)
                this->_phi += 360.0;
            this->update();
        }
        this->_prevMousePos = ev->pos();
    }
}

void				CameraFollow::keyEventForward(float elapsed, bool boost)
{
    (void)elapsed;
    (void)boost;
}

void				CameraFollow::keyEventBack(float elapsed, bool boost)
{
    (void)elapsed;
    (void)boost;
}

void				CameraFollow::keyEventLeft(float elapsed, bool boost)
{
    (void)elapsed;
    (void)boost;
}

void				CameraFollow::keyEventRight(float elapsed, bool boost)
{
    (void)elapsed;
    (void)boost;
}

void				CameraFollow::keyEventUp(float elapsed, bool boost)
{
    (void)elapsed;
    (void)boost;
}

void				CameraFollow::keyEventDown(float elapsed, bool boost)
{
    (void)elapsed;
    (void)boost;
}
