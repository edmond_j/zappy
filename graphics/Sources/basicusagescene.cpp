//
// basicusagescene.cpp for basic in /home/lequeu_m/
//
// Made by martin lequeux-gruninger
// Login   <lequeu_m@epitech.net>
//
// Started on  Wed Jun 12 19:29:37 2013 martin lequeux-gruninger
// Last update Wed Jun 12 19:29:38 2013 martin lequeux-gruninger
//

#include "basicusagescene.h"

#include <QOpenGLContext>
#include <iostream>
#include <cmath>

# include					<GL/glu.h>
# include					<GL/glut.h>
#include                    "Random/Random.hh"

BasicUsageScene::BasicUsageScene(int w, int h, Comm *comm)
{
    this->_width = w;
    this->_height = h;
    this->_comm = comm;
    this->_cameraVector.push_back(new CameraFollow());
    this->_cameraVector.push_back(new CameraFreeFly());
    this->_camera = this->_cameraVector[0];
    this->_moonOscillator = 0.0f;
    this->_moonRotation = 0.0f;


    QObject::connect(this->_comm, SIGNAL(bct(int, int, int, int, int, int, int, int, int)), this, SLOT(bct(int, int, int, int, int, int, int, int, int)));
    QObject::connect(this->_comm, SIGNAL(tna(const std::string &)), this, SLOT(tna(const std::string &)));
    QObject::connect(this->_comm, SIGNAL(pnw(int, int, int, int, int, const std::string &)), this, SLOT(pnw(int, int, int, int, int, const std::string &)));
    QObject::connect(this->_comm, SIGNAL(ppo(int, int, int, int)), this, SLOT(ppo(int, int, int, int)));
    QObject::connect(this->_comm, SIGNAL(plv(int, int)), this, SLOT(plv(int, int)));
    QObject::connect(this->_comm, SIGNAL(pin(int, int, int, int, int, int, int, int, int, int)), this, SLOT(pin(int, int, int, int, int, int, int, int, int, int)));
    QObject::connect(this->_comm, SIGNAL(pex(int)), this, SLOT(pex(int)));
    QObject::connect(this->_comm, SIGNAL(pbc(int,  const std::string &)), this, SLOT(pbc(int,  const std::string &)));
    QObject::connect(this->_comm, SIGNAL(pic(int, int, int, const std::vector<int> &)), this, SLOT(pic(int, int, int, const std::vector<int> &)));
    QObject::connect(this->_comm, SIGNAL(pie(int, int, int)), this, SLOT(pie(int, int, int)));
    QObject::connect(this->_comm, SIGNAL(pfk(int)), this, SLOT(pfk(int)));
    QObject::connect(this->_comm, SIGNAL(pdr(int, int)), this, SLOT(pdr(int, int)));
    QObject::connect(this->_comm, SIGNAL(pgt(int, int)), this, SLOT(pgt(int, int)));
    QObject::connect(this->_comm, SIGNAL(pdi(int)), this, SLOT(pdi(int)));
    QObject::connect(this->_comm, SIGNAL(enw(int, int, int, int)), this, SLOT(enw(int, int, int, int)));
    QObject::connect(this->_comm, SIGNAL(eht(int)), this, SLOT(eht(int)));
    QObject::connect(this->_comm, SIGNAL(ebo(int)), this, SLOT(ebo(int)));
    QObject::connect(this->_comm, SIGNAL(edi(int)), this, SLOT(edi(int)));
    QObject::connect(this->_comm, SIGNAL(sgt(int)), this, SLOT(sgt(int)));
    QObject::connect(this->_comm, SIGNAL(seg(int)), this, SLOT(seg(int)));
    QObject::connect(this->_comm, SIGNAL(smg(int)), this, SLOT(smg(int)));
    QObject::connect(this->_comm, SIGNAL(suc(void)), this, SLOT(suc(void)));
    QObject::connect(this->_comm, SIGNAL(sbp(void)), this, SLOT(sbp(void)));

    this->_comm->ready();

}

void BasicUsageScene::lightThisBitchUp()
{
    GLfloat ambientLight[]={0.1,0.1,0.1,1.0};
    glLightfv(GL_LIGHT0,GL_AMBIENT,ambientLight);
    GLfloat diffuseLight[]=	{2.0,2.0,2.0,1.0};
    glLightfv(GL_LIGHT0,GL_DIFFUSE,diffuseLight);
    GLfloat specularLight[]={0.5,0.5,0.5,1.0};
    glLightfv(GL_LIGHT0,GL_SPECULAR,specularLight);
    GLfloat lightPos[]= {1000.0,500.0,1000.0,1.0};
    glLightfv(GL_LIGHT0,GL_POSITION,lightPos);
    GLfloat specularReflection[]={0.6,0.6,0.6,1.0};
    glMaterialfv(GL_FRONT, GL_SPECULAR, specularReflection);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);
    glMateriali(GL_FRONT,GL_SHININESS,1000);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT,GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_NORMALIZE);
    glShadeModel(GL_SMOOTH);
    glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL, GL_SEPARATE_SPECULAR_COLOR);
}

GLuint BasicUsageScene::loadTexture(const std::string filename, int &width, int &height)
{
    //header for testing if it is a png
    png_byte header[8];

    //open file as binary
    FILE *fp = fopen(filename.c_str(), "rb");
    if (!fp) {
      return TEXTURE_LOAD_ERROR;
    }

    //read the header
    fread(header, 1, 8, fp);

    //test if png
    int is_png = !png_sig_cmp(header, 0, 8);
    if (!is_png) {
      fclose(fp);
      return TEXTURE_LOAD_ERROR;
    }

    //create png struct
    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL,
        NULL, NULL);
    if (!png_ptr) {
      fclose(fp);
      return (TEXTURE_LOAD_ERROR);
    }

    //create png info struct
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
      png_destroy_read_struct(&png_ptr, (png_infopp) NULL, (png_infopp) NULL);
      fclose(fp);
      return (TEXTURE_LOAD_ERROR);
    }

    //create png info struct
    png_infop end_info = png_create_info_struct(png_ptr);
    if (!end_info) {
      png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp) NULL);
      fclose(fp);
      return (TEXTURE_LOAD_ERROR);
    }

    //png error stuff, not sure libpng man suggests this.
    if (setjmp(png_jmpbuf(png_ptr))) {
      png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
      fclose(fp);
      return (TEXTURE_LOAD_ERROR);
    }

    //init png reading
    png_init_io(png_ptr, fp);

    //let libpng know you already read the first 8 bytes
    png_set_sig_bytes(png_ptr, 8);

    // read all the info up to the image data
    png_read_info(png_ptr, info_ptr);

    //variables to pass to get info
    int bit_depth, color_type;
    png_uint_32 twidth, theight;

    // get info about png
    png_get_IHDR(png_ptr, info_ptr, &twidth, &theight, &bit_depth, &color_type,
        NULL, NULL, NULL);

    //update width and height based on png info
    width = twidth;
    height = theight;

    // Update the png info struct.
    png_read_update_info(png_ptr, info_ptr);

    // Row size in bytes.
    int rowbytes = png_get_rowbytes(png_ptr, info_ptr);

    // Allocate the image_data as a big block, to be given to opengl
    png_byte *image_data = new png_byte[rowbytes * height];
    if (!image_data) {
      //clean up memory and close stuff
      png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
      fclose(fp);
      return TEXTURE_LOAD_ERROR;
    }

    //row_pointers is for pointing to image_data for reading the png with libpng
    png_bytep *row_pointers = new png_bytep[height];
    if (!row_pointers) {
      //clean up memory and close stuff
      png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
      delete[] image_data;
      fclose(fp);
      return TEXTURE_LOAD_ERROR;
    }
    // set the individual row_pointers to point at the correct offsets of image_data
    for (int i = 0; i < height; ++i)
      row_pointers[height - 1 - i] = image_data + i * rowbytes;

    //read the png into image_data through row_pointers
    png_read_image(png_ptr, row_pointers);

    //Now generate the OpenGL texture object
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D,0, GL_RGBA, width, height, 0,
        GL_RGB, GL_UNSIGNED_BYTE, (GLvoid*) image_data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    //clean up memory and close stuff
    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
    delete[] image_data;
    delete[] row_pointers;
    fclose(fp);

    return texture;
}


void BasicUsageScene::initialize()
{
    glEnable(GL_DEPTH_TEST);

    this->_skybox.initialize();

        this->_cameraVector[0]->initialize(1920.0f, 1080.0f);
        this->_cameraVector[0]->updateLen(50);
        this->_cameraVector[0]->update();

    this->_cameraVector[1]->initialize(1920.0f, 1080.0f);
    this->_cameraVector[1]->setPosition(Vector3f(0, 0, 200));

    this->lightThisBitchUp();

    int w, h;
    if ((this->_planetTexture = loadTexture("Assets/Ressources/torus.png", w, h)) == TEXTURE_LOAD_ERROR)
        std::cerr << "Cannot load Planet Texture" << std::endl;
    if ((this->_moonTexture = loadTexture("Assets/Ressources/moon.png", w, h)) == TEXTURE_LOAD_ERROR)
        std::cerr << "Cannot load Moon Texture" << std::endl;
    if ((this->_moonMoonTexture = loadTexture("Assets/Ressources/moonmoon.png", w, h)) == TEXTURE_LOAD_ERROR)
        std::cerr << "Cannot load Moon Texture" << std::endl;

    this->_planetDisplayList = glGenLists(1);
    glNewList(this->_planetDisplayList, GL_COMPILE);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, this->_planetTexture);
    this->solidTorus(49.0f, 100.0f, 100, 100);
    glDisable(GL_TEXTURE_2D);
    glColor4f(1,1,1,1);
    this->wireTorus(50.0f, 100.0f, 1, 1);
    glEndList();

    this->_moonDisplayList = glGenLists(1);
    glNewList(this->_moonDisplayList, GL_COMPILE);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, this->_moonTexture);
    glRotatef(20.0f, 0, 0, 1);
    this->drawSphere(20, 30, 30, false);
    glDisable(GL_TEXTURE_2D);
    glEndList();

    this->_moonMoonDisplayList = glGenLists(1);
    glNewList(this->_moonMoonDisplayList, GL_COMPILE);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, this->_moonMoonTexture);
    glRotatef(20.0f, 0, 0, 1);
    this->drawSphere(3, 10, 10, false);
    glDisable(GL_TEXTURE_2D);
    glEndList();
}

void    BasicUsageScene::updateMoon(float t)
{
   t = 1; //REMOVE THIS SHIT;
    this->_moonOscillator += 0.003 * t;
   this->_moonPosition = Vector3f(sin(this->_moonOscillator) * 200, cos(this->_moonOscillator) * 200 / 2, cos(this->_moonOscillator) * 200);
    this->_moonRotation += 0.5 * t;
}

void    BasicUsageScene::updateMoonMoon(float t)
{
   t = 1; //REMOVE THIS SHIT;
    this->_moonMoonOscillator += 0.03 * t;
   this->_moonMoonPosition = Vector3f(sin(this->_moonMoonOscillator) * 30, cos(this->_moonMoonOscillator) * 30 / 2, cos(this->_moonMoonOscillator) * 30);
    this->_moonMoonRotation += 0.5 * t;
}

void BasicUsageScene::update(float t)
{
    this->updateMoon(t);
    this->updateMoonMoon(t);
    this->_camera->updateKey(1.0/300.0, this->_pressedKeys);
}

void BasicUsageScene::doughnut(GLfloat r, GLfloat R, GLint nsides, GLint rings)
{
  int i, j;
  GLfloat theta, phi, theta1;
  GLfloat cosTheta, sinTheta;
  GLfloat cosTheta1, sinTheta1;
  GLfloat ringDelta, sideDelta;

  ringDelta = 2.0 * M_PI / rings;
  sideDelta = 2.0 * M_PI / nsides;

  theta = 0.0;
  cosTheta = 1.0;
  sinTheta = 0.0;
  for (i = rings - 1; i >= 0; --i) {
    theta1 = theta + ringDelta;
    cosTheta1 = cos(theta1);
    sinTheta1 = sin(theta1);
    glBegin(GL_QUAD_STRIP);
    phi = 0.0;
    for (j = nsides; j >= 0; --j) {
      GLfloat cosPhi, sinPhi, dist;

      phi += sideDelta;
      cosPhi = cos(phi);
      sinPhi = sin(phi);
      dist = R + r * cosPhi;

      std::cout << "DIST IS " << dist<< " & " << cosTheta << " &" << cosTheta1<< std::endl;
      glNormal3f(cosTheta1 * cosPhi, -sinTheta1 * cosPhi, sinPhi);
      glTexCoord2f((double)i/nsides,(double)j/rings);
      glVertex3f(cosTheta1 * dist, -sinTheta1 * dist, r * sinPhi);
      glNormal3f(cosTheta * cosPhi, -sinTheta * cosPhi, sinPhi);
      glTexCoord2f((double)(i+1)/nsides,(double)j/rings);
      glVertex3f(cosTheta * dist, -sinTheta * dist,  r * sinPhi);
    }
    glEnd();
    theta = theta1;
    cosTheta = cosTheta1;
    sinTheta = sinTheta1;
  }
}

/* CENTRY */
void BasicUsageScene::wireTorus(GLdouble r, GLdouble R, GLint nsides, GLint rings)
{
    for (float anglePhi = 0; anglePhi <= 360.0 + 360.0 / rings ; anglePhi += 360.0 / rings)

    {
        glBegin(GL_LINE_STRIP);
        for (float angleTheta = 0; angleTheta <= 360.0 + 360.0 / 100.0; angleTheta += 360.0 / 100.0)
        {
            static float toPi = 0.0174532925;

            float dist = R + r * cos(anglePhi * toPi);
            float nx = sin(angleTheta * toPi) * cos(anglePhi * toPi);
            float ny = cos(angleTheta * toPi) * cos(anglePhi * toPi);
            float nz = sin(anglePhi * toPi);

            float tx = sin(angleTheta * toPi) * dist;
            float ty = cos(angleTheta * toPi) * dist;
            float tz = r  * sin(anglePhi * toPi);
            glNormal3f(nx, ny, nz);
            glVertex3f(tx, ty,  tz);
        }
        glEnd();

    }

    for (float angleTheta = 0; angleTheta <= 360.0 + 360.0 / nsides; angleTheta += 360.0 / nsides)

    {
        glBegin(GL_LINE_STRIP);

        for (float anglePhi = 0; anglePhi <= 360.0 + 360.0 / 100.0 ; anglePhi += 360.0 / 100.0)
        {
            static float toPi = 0.0174532925;

            float dist = R + r * cos(anglePhi * toPi);
            float nx = sin(angleTheta * toPi) * cos(anglePhi * toPi);
            float ny = cos(angleTheta * toPi) * cos(anglePhi * toPi);
            float nz = sin(anglePhi * toPi);

            float tx = sin(angleTheta * toPi) * dist;
            float ty = cos(angleTheta * toPi) * dist;
            float tz = r  * sin(anglePhi * toPi);

            glNormal3f(nx, ny, nz);
            glVertex3f(tx, ty,  tz);
        }
        glEnd();
    }
}

void BasicUsageScene::solidTorus(GLdouble innerRadius, GLdouble outerRadius,
  GLint nsides, GLint rings)
{
  doughnut(innerRadius, outerRadius, nsides, rings);
}


void BasicUsageScene::drawSphere(double r, int lats, int longs, bool grid)
{
    double x, y, z, dTheta=180.0/lats, dLon=360.0/longs, degToRad=3.141592665885/180 ;

    if (!grid)
    {
      for(double lat =0; lat <=180; lat+=dTheta)
      {
          glBegin( GL_QUAD_STRIP ) ;
          for(double lon =0 ; lon <=360 ; lon+=dLon)
          {
              //Vertex 1
              x = r*cos(lon * degToRad) * sin(lat * degToRad) ;
              y = r*cos(lat * degToRad) ;
              z = r*sin(lon * degToRad) * sin(lat * degToRad) ;
              glNormal3d( x, y, z) ;
              glTexCoord2d(lon/360-0.25, lat/180);
              glVertex3d( x, y, z ) ;
              //Vetex 2
              x = r*cos(lon * degToRad) * sin( (lat + dTheta)* degToRad) ;
              y = r*cos( (lat + dTheta) * degToRad ) ;
              z = r*sin(lon * degToRad) * sin((lat + dTheta) * degToRad) ;
              glNormal3d( x, y, z ) ;
              glTexCoord2d(lon/360-0.25, (lat + dTheta-1)/(180));
              glVertex3d( x, y, z ) ;
              //Vertex 3
              x = r*cos((lon + dLon) * degToRad) * sin((lat) * degToRad) ;
              y = r*cos((lat) * degToRad ) ;
              z = r*sin((lon + dLon) * degToRad) * sin((lat) * degToRad) ;
              glNormal3d( x, y, z ) ;
              glTexCoord2d((lon + dLon)/(360)-0.25 ,(lat)/180);
               glVertex3d( x, y, z ) ;
              //Vertex 4
              x = r*cos((lon + dLon) * degToRad) * sin((lat + dTheta)* degToRad) ;
              y = r*cos((lat + dTheta)* degToRad ) ;
              z = r*sin((lon + dLon)* degToRad) * sin((lat + dTheta)* degToRad) ;
              glNormal3d( x, y, z ) ;
              glTexCoord2d((lon + dLon)/360-0.25, (lat + dTheta)/(180));
              glVertex3d( x, y, z ) ;
          }
          glEnd() ;
      }
      glDisable(GL_TEXTURE_2D);
    }
    if (grid)
    {
      r *= 1.005;

      for(double lat =0; lat <=180; lat+=dTheta)
      {
          glBegin( GL_LINE_STRIP ) ;
          glColor3ub(255, 255, 255);
          for(double lon =0 ; lon <=360 ; lon+=dLon)
          {
              //Vertex 1
              x = r*cos(lon * degToRad) * sin(lat * degToRad) ;
              y = r*cos(lat * degToRad) ;
              z = r*sin(lon * degToRad) * sin(lat * degToRad) ;
              glVertex3d( x, y, z ) ;
              //Vetex 2
              x = r*cos(lon * degToRad) * sin( (lat + dTheta)* degToRad) ;
              y = r*cos( (lat + dTheta) * degToRad ) ;
              z = r*sin(lon * degToRad) * sin((lat + dTheta) * degToRad) ;
              glVertex3d( x, y, z ) ;
              //Vertex 4
              x = r*cos((lon + dLon) * degToRad) * sin((lat + dTheta)* degToRad) ;
              y = r*cos((lat + dTheta)* degToRad ) ;
              z = r*sin((lon + dLon)* degToRad) * sin((lat + dTheta)* degToRad) ;
              glVertex3d( x, y, z ) ;
              //Vertex 3
              x = r*cos((lon + dLon) * degToRad) * sin((lat) * degToRad) ;
              y = r*cos((lat) * degToRad ) ;
              z = r*sin((lon + dLon) * degToRad) * sin((lat) * degToRad) ;
              glVertex3d( x, y, z ) ;

          }
          glEnd() ;
      }
    }
}

void    BasicUsageScene::translateAndRotate(float x, float y, float up)
{
    static float toPi = 0.0174532925;

    float dist = 100.0 + (50.0 + up) * cos(360.0 / (float)this->_height * (y + 0.5) * toPi);

    float tx = sin(360.0 / (float)this->_width * (x + 0.5) * toPi) * dist;
    float ty = cos(360.0 / (float)this->_width * (x + 0.5) * toPi) * dist;
    float tz = (50.0 + up) * sin(360.0 / (float)this->_height * (y + 0.5) * toPi);

    float nx = sin(360.0 / (float)this->_width * (x + 0.5) * toPi) * cos(360.0 / (float)this->_height * (y + 0.5) * toPi);
    float ny = cos(360.0 / (float)this->_width * (x + 0.5) * toPi) * cos(360.0 / (float)this->_height * (y + 0.5) * toPi);
    float nz = sin(360.0 / (float)this->_height * (y + 0.5) * toPi);


    glTranslatef(tx, ty, tz);

    Vector3f from(0, 0, 1);
    Vector3f to(nx, ny, nz);

    Vector3f target_dir = to.normalize();
    float rot_angle = acos( target_dir.dotProduct(from) );

        Vector3f rot_axis = target_dir.crossProduct(from);
        rot_axis.normalize();
        glRotatef(- rot_angle / M_PI * 180.0, rot_axis.x, rot_axis.y, rot_axis.z );
}

std::triple<Vector3f, Vector3f, float>     BasicUsageScene::getTanslateAndRotateRand(float x, float y, float up)
{
    static float toPi = 0.0174532925;
    float r = ((double) LibC::Random::randVal() / (RAND_MAX));
    float r2 = ((double) LibC::Random::randVal() / (RAND_MAX));

    float dist = 100.0 + (50.0 + up) * cos(360.0 / (float)this->_height * (y + r2) * toPi);

    float tx = sin(360.0 / (float)this->_width * (x + r) * toPi) * dist;
    float ty = cos(360.0 / (float)this->_width * (x + r) * toPi) * dist;
    float tz = (50.0 + up) * sin(360.0 / (float)this->_height * (y + r2) * toPi);

    float nx = sin(360.0 / (float)this->_width * (x + r) * toPi) * cos(360.0 / (float)this->_height * (y + r2) * toPi);
    float ny = cos(360.0 / (float)this->_width * (x + r) * toPi) * cos(360.0 / (float)this->_height * (y + r2) * toPi);
    float nz = sin(360.0 / (float)this->_height * (y + r2) * toPi);

    Vector3f translated(tx, ty, tz);
    Vector3f from(0, 0, 1);
    Vector3f to(nx, ny, nz);
    Vector3f target_dir = to.normalize();
    float rot_angle = acos( target_dir.dotProduct(from) );
    Vector3f rot_axis = target_dir.crossProduct(from);
    rot_axis.normalize();
    return (std::triple<Vector3f, Vector3f, float>(translated, rot_axis, - rot_angle / M_PI * 180.0));
}

void BasicUsageScene::renderRessources()
{
    std::map<Vector3f, std::map<t_ressource, std::vector<std::triple<Vector3f, Vector3f, float> > > >        _map;

    for (map_iterator it = this->_map.begin(); it != this->_map.end(); ++it)
    {
        for (ressources_iterator it2 = (*it).second.begin(); it2 != (*it).second.end(); ++it2)
        {
            switch ((*it2).first)
            {
            case         FOOD:
                glColor4f(1, 0, 0, 1);
                break;
            case         LINEMATE:
                glColor4f(0, 1, 0, 1);

                break;
            case         DERAUMERE:
                glColor4f(0, 0, 1, 1);

                break;
            case         SIBUR:
                glColor4f(1, 0, 1, 1);

                break;
            case         MENDIANE:
                glColor4f(0, 1, 1, 1);

                break;
            case         PHIRAS:
                glColor4f(1, 1, 0, 1);

                break;
            case         THYSTAME:
                glColor4f(1, 1, 1, 1);
                break;
            default:
                break;
            }

            for (ressource_iterator it3 = (*it2).second.begin(); it3 != (*it2).second.end(); ++it3)
            {
                glPushMatrix();

                glTranslatef((*it3).first.x, (*it3).first.y, (*it3).first.z);
                glRotatef((*it3).third, (*it3).second.x, (*it3).second.y, (*it3).second.z);
                this->drawSphere(1.5, 4, 4, false);
                glPopMatrix();

            }
        }
    }
    glColor4f(1, 1, 1, 1);
}

void BasicUsageScene::render()
{
    this->lightThisBitchUp();

    static int i = 0;
    ++i;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glClearDepth(1.0f);
    glClearColor(1, 1, 1, 1);

   this->_camera->look();

    this->_skybox.draw(this->_camera->getPosition());

    glPushMatrix();
     glLineWidth(5);
     glBegin(GL_LINES);
     glColor3ub(255, 0, 0);
     glVertex3d(0, 0, 0);
     glVertex3d(1000, 0, 0);
     glColor3ub(0, 255, 0);
     glVertex3d(0, 0, 0);
     glVertex3d(0, 1000, 0);
     glColor3ub(0, 0, 255);
     glVertex3d(0, 0, 0);
     glVertex3d(0, 0, 1000);
     glColor3ub(255, 255, 255);
     glEnd();
     glLineWidth(1);
     glPopMatrix();

     glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
     glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
     glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
     glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);

     glPushMatrix();
     glRotatef(-i * 0.005, 0,0,1);
    glPushMatrix();
    glCallList(this->_planetDisplayList);


    //DRAW SCENE HERE

 this->renderRessources();

    ////


    glPopMatrix();


     glPushMatrix();
     glTranslatef(this->_moonPosition.x,this->_moonPosition.y,this->_moonPosition.z);
     glTranslatef(-200.0f, 0.0f, -0.0f);
     glRotatef(this->_moonRotation, 0, 1, 0);
     glCallList(this->_moonDisplayList);
     glPushMatrix();
     glTranslatef(this->_moonMoonPosition.x,this->_moonMoonPosition.y,this->_moonMoonPosition.z);
     glRotatef(this->_moonMoonRotation, 0, 1, 0);
     glCallList(this->_moonMoonDisplayList);
     glPopMatrix();
     glPopMatrix();
     glPopMatrix();
}

void BasicUsageScene::resize(int width, int height)
{
    glViewport(0, 0, width, height);
}

void  BasicUsageScene::keyPressEvent(QKeyEvent * ev)
{
    if (ev->key() == Qt::Key_1)
        this->_camera = this->_cameraVector[0];
    else if (ev->key() == Qt::Key_2)
        this->_camera = this->_cameraVector[1];
    if(std::find(this->_pressedKeys.begin(), this->_pressedKeys.end(), ev->key()) == this->_pressedKeys.end())
     {
        this->_pressedKeys.push_back((Qt::Key)ev->key());
        ev->accept();
     }
     else
     {
        ev->ignore();
     }
};

void  BasicUsageScene::keyReleaseEvent(QKeyEvent * ev)
{
    if(std::find(this->_pressedKeys.begin(), this->_pressedKeys.end(), ev->key()) != this->_pressedKeys.end())
    {
        this->_pressedKeys.erase(std::find(this->_pressedKeys.begin(), this->_pressedKeys.end(), ev->key()));
        ev->accept();
    }
    else
    {
       ev->ignore();
    }
};

void BasicUsageScene::mouseMoveEvent(QMouseEvent * ev)
{
  this->_camera->updateMouse(ev);
    this->lightThisBitchUp();
}


void BasicUsageScene::mousePressEvent(QMouseEvent * ev)
{
    this->_camera->resetPrevPos();

    (void)ev;
}

void  BasicUsageScene::wheelEvent(QWheelEvent * ev)
{
    this->_camera->updateLen(ev->angleDelta().y() / 10);
    this->_camera->update();
}





















void              BasicUsageScene::bct(int x, int y, int a, int b, int c, int d, int e, int f, int g)
{
    int             i;
    int             lim;
    t_ressource     type;
    Vector3f        pos(x, y, 0);

    lim = a;
    type = FOOD;
    i = _map[pos][type].size();
    if (i < lim)
        while (++i <= lim)
            _map[pos][type].push_back(this->getTanslateAndRotateRand(x, y, 0));
    else if (i > lim)
        while (--i >= lim)
            _map[pos][type].pop_back();

    lim = b;
    type = LINEMATE;
    i = _map[pos][type].size();
    if (i < lim)
        while (++i <= lim)
            _map[pos][type].push_back(this->getTanslateAndRotateRand(x, y, 0));
    else if (i > lim)
        while (--i >= lim)
            _map[pos][type].pop_back();

    lim = c;
    type = DERAUMERE;
    i = _map[pos][type].size();
    if (i < lim)
        while (++i <= lim)
            _map[pos][type].push_back(this->getTanslateAndRotateRand(x, y, 0));
    else if (i > lim)
        while (--i >= lim)
            _map[pos][type].pop_back();

    lim = d;
    type = SIBUR;
    i = _map[pos][type].size();
    if (i < lim)
        while (++i <= lim)
            _map[pos][type].push_back(this->getTanslateAndRotateRand(x, y, 0));
    else if (i > lim)
        while (--i >= lim)
            _map[pos][type].pop_back();

    lim = e;
    type = MENDIANE;
    i = _map[pos][type].size();
    if (i < lim)
        while (++i <= lim)
            _map[pos][type].push_back(this->getTanslateAndRotateRand(x, y, 0));
    else if (i > lim)
        while (--i >= lim)
            _map[pos][type].pop_back();

    lim = f;
    type = PHIRAS;
    i = _map[pos][type].size();
    if (i < lim)
        while (++i <= lim)
            _map[pos][type].push_back(this->getTanslateAndRotateRand(x, y, 0));
    else if (i > lim)
        while (--i >= lim)
            _map[pos][type].pop_back();

    lim = g;
    type = THYSTAME;
    i = _map[pos][type].size();
    if (i < lim)
        while (++i <= lim)
            _map[pos][type].push_back(this->getTanslateAndRotateRand(x, y, 0));
    else if (i > lim)
        while (--i >= lim)
            _map[pos][type].pop_back();

    std::cout << "GOT BCT" << x << y << a << b << c << d << e << f << g << std::endl;
}

void              BasicUsageScene::tna(const std::string &)
{

}

void              BasicUsageScene::pnw(int, int, int, int, int, const std::string &)
{

}

void              BasicUsageScene::ppo(int, int, int, int)
{

}

void              BasicUsageScene::plv(int, int)
{

}

void              BasicUsageScene::pin(int, int, int, int, int, int, int, int, int, int)
{

}

void              BasicUsageScene::pex(int)
{

}

void              BasicUsageScene::pbc(int,  const std::string &)
{

}

void              BasicUsageScene::pic(int, int, int, const std::vector<int> &)
{

}

void              BasicUsageScene::pie(int, int, int)
{

}

void              BasicUsageScene::pfk(int)
{

}

void              BasicUsageScene::pdr(int, int)
{

}

void              BasicUsageScene::pgt(int, int)
{

}

void              BasicUsageScene::pdi(int)
{

}

void              BasicUsageScene::enw(int, int, int, int)
{

}

void              BasicUsageScene::eht(int)
{

}

void              BasicUsageScene::ebo(int)
{

}

void              BasicUsageScene::edi(int)
{

}

void              BasicUsageScene::sgt(int t)
{
    this->_sgt = t;
}

void              BasicUsageScene::seg(int)
{

}

void              BasicUsageScene::smg(int)
{

}

void              BasicUsageScene::suc(void)
{

}

void              BasicUsageScene::sbp(void)
{

}
