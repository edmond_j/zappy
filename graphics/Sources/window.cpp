//
// window.cpp for window in /home/lequeu_m/
//
// Made by martin lequeux-gruninger
// Login   <lequeu_m@epitech.net>
//
// Started on  Wed Jun 12 19:26:46 2013 martin lequeux-gruninger
// Last update Wed Jun 12 19:26:50 2013 martin lequeux-gruninger
//

#include "window.h"
# include <iostream>
# include <cmath>

#include "basicusagescene.h"

#include <QOpenGLContext>
#include <QTimer>


Window::Window(int w, int h, Comm *comm, QScreen *screen) :
  QWindow(screen),
  mScene(new BasicUsageScene(w, h, comm))
{
  setSurfaceType(OpenGLSurface);

  QSurfaceFormat format;
  format.setDepthBufferSize(24);
  format.setMajorVersion(3);
  format.setMinorVersion(0);
  format.setSamples(4);
  format.setProfile(QSurfaceFormat::CoreProfile);

  setFormat(format);
  create();

  mContext = new QOpenGLContext();
  mContext->setFormat(format);
  mContext->create();

  mScene->setContext(mContext);
  initializeGl();
  connect(this, SIGNAL(widthChanged(int)), this, SLOT(resizeGl()));
  connect(this, SIGNAL(heightChanged(int)), this, SLOT(resizeGl()));

  QTimer *timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), this, SLOT(updateScene()));
  timer->start(16);

  this->showFullScreen();
}

Window::~Window()
{
}

void Window::initializeGl()
{
  mContext->makeCurrent(this);
  mScene->initialize();
}

void Window::paintGl()
{
    if (this->isVisible())
    {
      mContext->makeCurrent(this);
      mScene->render();
     mContext->swapBuffers(this);
    }
}

void Window::resizeGl()
{
    mContext->makeCurrent(this);
    mScene->resize(this->width(),this->height());
}

void Window::updateScene()
{
  mScene->update(0.0f);
  paintGl();
}

void  Window::keyPressEvent(QKeyEvent * ev)
{
    if (ev->key() == Qt::Key_Escape)
        emit close();
    mScene->keyPressEvent(ev);
};

void  Window::keyReleaseEvent(QKeyEvent * ev)
{
    mScene->keyReleaseEvent(ev);
};

void Window::mouseMoveEvent(QMouseEvent * ev)
{
    mScene->mouseMoveEvent(ev);
}

void Window::mousePressEvent(QMouseEvent * ev)
{
   mScene->mousePressEvent(ev);
}

void  Window::wheelEvent(QWheelEvent * ev)
{
    mScene->wheelEvent(ev);
}
