//
// CircularBuffer.hpp for my_irc in /home/edmond_j//rendu/B4/systemUnix/my_irc/myirc-2016-edmond_j/edmond_j/Sources
//
// Made by julien edmond
// Login   <edmond_j@epitech.net>
//
// Started on  Wed Apr 24 20:17:38 2013 julien edmond
// Last update Tue May 21 16:44:54 2013 julien edmond
//

#ifndef		CIRCULARBUFFER_HPP__
# define	CIRCULARBUFFER_HPP__

# include	<algorithm>
# include	<cstring>
# include	<cstdio>
# include	<string>

class		CircularBuffer
{
public:
  CircularBuffer(int size = 512*512);
  CircularBuffer(const CircularBuffer&);
  CircularBuffer&	operator=(const CircularBuffer&);
  ~CircularBuffer();

  template<typename T>
  CircularBuffer&	operator<<(const T&);

  template<typename T>
  bool	operator>>(T&);

  template<typename T>
  void		write(const T* area, int size);

  template<typename T>
  bool		read(T* area, int size);

  int		getReadable() const;
  int		getSize() const;
  bool		isEmpty() const;
  bool		isFull() const;

  bool		getLine(std::string& line);

private:
  int		_size;
  int		_start;
  int		_end;
  char*		_buff;
};

template<typename T>
CircularBuffer&	CircularBuffer::operator<<(const T& item)
{
  this->write(&item, sizeof(T));
  return (*this);
}

template<typename T>
bool	CircularBuffer::operator>>(T& item)
{
  return (this->read(&item, sizeof(T)));
}

template<typename T>
void		CircularBuffer::write(const T* area, int size)
{
  int		i(-1);
  const char*	tmp(reinterpret_cast<const char*>(area));

  while (++i < size)
    {
      _buff[_end] = tmp[i];
      _end = (_end + 1) % _size;
      if (_end == _start)
	_start = (_start + 1) % _size;
    }
  // unsigned	copied(0);
  // int		toCopy;

  // while (static_cast<int>(copied) < size)
  //   {
  //     toCopy = std::min<unsigned>(_size - _end, size - copied);
  //     memcpy(&_buff[_end], &tmp[copied], toCopy);
  //     _end = _end + toCopy;
  //     if (_end - toCopy < _start && _end >= _start)
  // 	_start = (_end + 1) % _size;
  //     _end %= _size;
  //     copied += toCopy;
  //   }
}

template<typename T>
bool		CircularBuffer::read(T* area, int size)
{
  int		i(-1);
  char*		tmp(reinterpret_cast<char*>(area));

  if (getReadable() >= size)
    while (++i < size)
      {
	tmp[i] = _buff[_start];
	_start = (_start + 1) % _size;
      }
  return (i == -1);
  // unsigned	copied(0);
  // int		toCopy;
  // char*		tmp(reinterpret_cast<char*>(area));

  // if (getReadable() >= size)
  //   while (static_cast<int>(copied) < size)
  //     {
  // 	if (_end > _start)
  // 	  toCopy = std::min<unsigned>(_end - _start, size - copied);
  // 	else
  // 	  toCopy = std::min<unsigned>(_size - _start, size - copied);
  // 	memcpy(&tmp[copied], &_buff[_start], toCopy);
  // 	_start = (_start + toCopy) % _size;
  // 	copied += toCopy;
  //     }
  // return (copied);
}

#endif		// CIRCULARBUFFER_HPP__
