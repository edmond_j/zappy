//
// AObject.hh for gdl in /home/lequeu_m/
//
// Made by martin lequeux-gruninger
// Login   <lequeu_m@epitech.net>
//
// Started on  Mon May  6 17:46:09 2013 martin lequeux-gruninger
// Last update Thu Jul  4 10:22:11 2013 martin lequeux-gruninger
//

#ifndef						CAMERAFOLLOW_HH_
# define					CAMERAFOLLOW_HH_

# include					<iostream>
# include					"ACamera.hpp"


class						CameraFollow : public ACamera
{
public:
  CameraFollow(const Vector3f& position = Vector3f(0.0f, 0.0f, 900.0f));
  CameraFollow(const CameraFollow& copy);
  virtual ~CameraFollow();
  CameraFollow&				operator=(const CameraFollow& other);

  virtual void				update();
  virtual void				mouseChange(QMouseEvent * ev);
  virtual void				keyEventForward(float elapsed, bool boost);
  virtual void				keyEventBack(float elapsed, bool boost);
  virtual void				keyEventLeft(float elapsed, bool boost);
  virtual void				keyEventRight(float elapsed, bool boost);
  virtual void				keyEventUp(float elapsed, bool boost);
  virtual void				keyEventDown(float elapsed, bool boost);
};


#endif						/* !CAMERAFOLLOW_HH_ */
