/*
** basicusagescene.h for basic in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Wed Jun 12 19:29:24 2013 martin lequeux-gruninger
** Last update Thu Jul  4 10:20:36 2013 martin lequeux-gruninger
*/

#ifndef BASICUSAGESCENE_H
#define BASICUSAGESCENE_H

#include	<QOpenGLShaderProgram>
#include	<QOpenGLBuffer>
#include <QObject>

#include	"abstractscene.h"
#include	"skybox.h"
#include       	"Vector3f.hh"
#include	"ACamera.hpp"
#include	"CameraFreeFly.hh"
#include	"CameraFollow.hh"
#include	"Model3ds.hh"
#include	"triple.hpp"



class BasicUsageScene : public QObject, public AbstractScene
{
    Q_OBJECT
public:
    typedef enum
      {
        FOOD,
        LINEMATE,
        DERAUMERE,
        SIBUR,
        MENDIANE,
        PHIRAS,
        THYSTAME
      }		t_ressource;
 public:
  BasicUsageScene(int, int, Comm *);

  virtual void	initialize();
  virtual void	update(float t);
  virtual void	render();

  virtual void	resize(int width, int height);
  virtual void	keyPressEvent(QKeyEvent * ev);
  virtual void	keyReleaseEvent(QKeyEvent * ev);
  virtual void	mouseMoveEvent(QMouseEvent * ev);
  virtual void	mousePressEvent(QMouseEvent * ev);
  virtual void	wheelEvent(QWheelEvent * ev);

private:
  void		doughnut(GLfloat r, GLfloat R, GLint nsides, GLint rings);
  void		wireTorus(GLdouble r, GLdouble R, GLint nsides, GLint rings);
  void		solidTorus(GLdouble innerRadius, GLdouble outerRadius, GLint nsides, GLint rings);


    void	drawSphere(double r, int lats, int longs, bool);
    void	lightThisBitchUp();
    GLuint	loadTexture(const std::string filename, int &width, int &height);

    void	updateMoon(float);
    void	updateMoonMoon(float);

    void    translateAndRotate(float x, float y, float up);
    std::triple<Vector3f, Vector3f, float>    getTanslateAndRotateRand(float x, float y, float);
    void renderRessources();

 private:
    Comm            *_comm;
    int             _width;
    int             _height;

    Skybox			_skybox;
    std::vector<ACamera*>	_cameraVector;
    ACamera			*_camera;

    GLuint			_planetTexture;
    GLuint			_moonTexture;
    GLuint			_moonMoonTexture;

    GLuint			_planetDisplayList;

    GLuint			_moonDisplayList;
    float			_moonOscillator;
    Vector3f			_moonPosition;
    float			_moonRotation;

    GLuint			_moonMoonDisplayList;
    float			_moonMoonOscillator;
    Vector3f			_moonMoonPosition;
    float			_moonMoonRotation;

    std::vector<Qt::Key>	_pressedKeys;

    Vector3f			_cameraTarget;
    Vector3f			_cameraPosition;

    Model               _model;


    //SRV INVO
    int             _sgt;
    std::map<Vector3f, std::map<t_ressource, std::vector<std::triple<Vector3f, Vector3f, float> > > >        _map;

    typedef std::map<Vector3f, std::map<t_ressource, std::vector<std::triple<Vector3f, Vector3f, float> > > >::iterator     map_iterator;
    typedef std::map<t_ressource, std::vector<std::triple<Vector3f, Vector3f, float> > >::iterator                          ressources_iterator;
    typedef std::vector<std::triple<Vector3f, Vector3f, float> >::iterator                                                  ressource_iterator;

private slots:
    void              bct(int, int, int, int, int, int, int, int, int);
    void              tna(const std::string &);
    void              pnw(int, int, int, int, int, const std::string &);
    void              ppo(int, int, int, int);
    void              plv(int, int);
    void              pin(int, int, int, int, int, int, int, int, int, int);
    void              pex(int);
    void              pbc(int,  const std::string &);
    void              pic(int, int, int, const std::vector<int> &);
    void              pie(int, int, int);
    void              pfk(int);
    void              pdr(int, int);
    void              pgt(int, int);
    void              pdi(int);
    void              enw(int, int, int, int);
    void              eht(int);
    void              ebo(int);
    void              edi(int);
    void              sgt(int);
    void              seg(int);
    void              smg(int);
    void              suc(void);
    void              sbp(void);
};

#endif // BASICUSAGESCENE_H
