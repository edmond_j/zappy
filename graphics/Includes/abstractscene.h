/*
** abstractscene.h for scene in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Wed Jun 12 19:28:41 2013 martin lequeux-gruninger
** Last update Thu Jul  4 10:22:49 2013 martin lequeux-gruninger
*/

#ifndef			ABSTRACTSCENE_H
#define			ABSTRACTSCENE_H

#include		<QKeyEvent>
#include		<QMouseEvent>
#include		<QWheelEvent>

#include        "comm.h"

class QOpenGLContext;

class AbstractScene
{
 public:
  AbstractScene() {}

  void			setContext(QOpenGLContext *context) { mContext = context; }
  QOpenGLContext*	context() const { return mContext; }

  virtual void		initialize() = 0;

  virtual void		update(float t) = 0;

  virtual void		render() = 0;

  virtual void		resize(int width, int height) = 0;

  virtual void		keyPressEvent(QKeyEvent * ev) = 0;
  virtual void		keyReleaseEvent(QKeyEvent * ev) = 0;
  virtual void		mouseMoveEvent(QMouseEvent * ev) = 0;
  virtual void		mousePressEvent(QMouseEvent * ev) = 0;
  virtual void		wheelEvent(QWheelEvent * ev) = 0;

 protected:
  QOpenGLContext	*mContext;
};

#endif // ABSTRACTSCENE_H
