/*
** skybox.h for zappy in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Thu Jul  4 10:15:04 2013 martin lequeux-gruninger
** Last update Thu Jul  4 10:15:28 2013 martin lequeux-gruninger
*/

#ifndef SKYBOX_H
#define SKYBOX_H

#include <GL/gl.h>
#include <GL/glu.h>
#include <png.h>
#include <iostream>
#include <cstdio>
#include <string>
#include "Vector3f.hh"

#define TEXTURE_LOAD_ERROR 0

class Skybox
{
 public:
  Skybox();

  virtual void initialize(void);
  virtual void draw(const Vector3f &);
  GLuint loadTexture(const std::string, int &width, int &height);

 private:
  GLuint			_up;
  GLuint			_down;
  GLuint			_front;
  GLuint			_back;
  GLuint			_left;
  GLuint			_right;
  GLuint			_displayList;
  bool				_ok;
};

#endif // SKYBOX_H
