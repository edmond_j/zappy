//
// MyThread.hpp for MyThread in /home/lequeu_m/
//
// Made by martin lequeux-gruninger
// Login   <lequeu_m@epitech.net>
//
// Started on  Fri Apr 12 15:15:11 2013 martin lequeux-gruninger
// Last update Tue Apr 16 11:54:49 2013 martin lequeux-gruninger
//

#ifndef		__MY_SCOPELOCK_HPP__
# define	__MY_SCOPELOCK_HPP__

# include	"MyMutex.hpp"

class	MyScopelock
{
  MyMutex &		_mutex;

  MyScopelock(const MyScopelock &);
  MyScopelock &	operator=(const MyScopelock &);
public:
  MyScopelock(MyMutex & mutex) : _mutex(mutex)
  {
    this->_mutex.lock();
  }

  ~MyScopelock()
  {
    this->_mutex.unlock();
  }
};

#endif		// __MY_CONVAR_HPP__
