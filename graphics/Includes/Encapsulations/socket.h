/*
** socket.h for ftp in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Sun Apr 14 10:13:47 2013 martin lequeux-gruninger
** Last update Tue May 21 11:36:33 2013 martin lequeux-gruninger
*/

#ifndef		__SOCKET_H__
# define	__SOCKET_H__

# include	<stdio.h>
# include	<stdlib.h>
# include	<string.h>
# include	<unistd.h>
# include	<errno.h>
# include	<limits.h>
# include	<sys/types.h>
# include	<sys/socket.h>
# include	<netinet/in.h>
# include	<arpa/inet.h>
# include	<netdb.h>
# include	<dirent.h>
# include	<sys/stat.h>
# include	<fcntl.h>

# define	CLIENT_MAX	10
# define	OPEN_FILE	0xDEADBEEF
# define	CLOSE_FILE	0xB16B00B5
# define	TIMEOUT		500000
# define	PORT		"1337"

typedef struct		s_fds
{
  fd_set		read;
  fd_set		read_tmp;
  fd_set		write;
  fd_set		write_tmp;
  int			nfds;
}			t_fds;

typedef struct		s_addrinfos
{
  struct addrinfo	hints;
  struct addrinfo	*ai;
}			t_addrinfos;

#endif			/* __SOCKET_H__ */
