//
// MyThread.hpp for MyThread in /home/lequeu_m/
//
// Made by martin lequeux-gruninger
// Login   <lequeu_m@epitech.net>
//
// Started on  Fri Apr 12 15:15:11 2013 martin lequeux-gruninger
// Last update Sun Apr 28 17:03:30 2013 martin lequeux-gruninger
//

#ifndef		__MY_MUTEX_HPP__
# define	__MY_MUTEX_HPP__

# include	<pthread.h>

class	MyMutex
{
private:
  pthread_mutex_t		_mutex;

  MyMutex(const MyMutex &);
  MyMutex &			operator=(const MyMutex &);

public:
  MyMutex() { pthread_mutex_init(&(this->_mutex), NULL); }
  ~MyMutex() { pthread_mutex_destroy(&(this->_mutex)); }

  bool			lock()
  {
    if (pthread_mutex_lock(&(this->_mutex)) == 0)
      return (true);
    return (false);
  }

  bool			unlock()
  {
    if (pthread_mutex_unlock(&(this->_mutex)) == 0)
      return (true);
    return (false);
  }

  bool			trylock()
  {
    if (!pthread_mutex_trylock(&(this->_mutex)))
      return (true);
    return (false);
  }

  pthread_mutex_t &	getMutex()
  {
    return (this->_mutex);
  }
};

#endif		// __MY_MUTEX_HPP__
