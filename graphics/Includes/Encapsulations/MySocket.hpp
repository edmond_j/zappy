//
// MySocket.hpp for MySocket in /home/lequeu_m/
//
// Made by martin lequeux-gruninger
// Login   <lequeu_m@epitech.net>
//
// Started on  Wed Apr 24 15:01:31 2013 martin lequeux-gruninger
// Last update Sun Apr 28 17:00:48 2013 martin lequeux-gruninger
//

#ifndef		__MY_SOCKET_HPP__
# define	__MY_SOCKET_HPP__

# include	<string>
# include	<cstring>
# include	<cstdio>
# include	<iostream>
# include	<limits.h>
# include	<exception>
# include	<stdexcept>
# include	<sys/types.h>
# include	<sys/socket.h>
# include	<netinet/in.h>
# include	<arpa/inet.h>
# include	<netdb.h>
# include       "socket.h"
# include       "../CircularBuffer.hpp"

class			MySocket
{
public :
  class		SocketException: public std::runtime_error
  {
  public:
    SocketException(const std::string &str) throw()
      : std::runtime_error(str) {}
  };

protected:
  CircularBuffer        _buffer;
  std::string			_ip;
  std::string			_port;
  int				_sock_fd;
  t_fds				_fds;
  t_addrinfos			_addrinfos;

  MySocket(const MySocket &other);
  MySocket			 &operator=(const MySocket &other);
public:
  MySocket(const std::string & ip, const std::string & port)
    : _ip(ip), _port(port), _sock_fd(-1)
  {
  //  _buffer.i = 0;
  }

  void	connectMe()
  {
    //INIT FDS
    FD_ZERO(&this->_fds.read);
    FD_ZERO(&this->_fds.read_tmp);
    FD_ZERO(&this->_fds.write);
    FD_ZERO(&this->_fds.write_tmp);
    this->_fds.nfds = 0;

    //INIT ADDRINFOS
    int		ret;

    MySocket::MyBzero(&this->_addrinfos.hints, sizeof(this->_addrinfos.hints));
    this->_addrinfos.hints.ai_family = AF_INET;
    this->_addrinfos.hints.ai_socktype = SOCK_STREAM;
    if ((ret = MySocket::MyGetaddrinfos(this->_ip.c_str(),
					this->_port.c_str(),
					&this->_addrinfos.hints,
					&this->_addrinfos.ai)) != 0)
      {
	throw SocketException("Error on getaddrinfo : " + std::string(MySocket::MyGai_strerror(ret)));
	return ;
      }

    //INIT SOCKET
    struct addrinfo	*tmp;

    tmp = this->_addrinfos.ai;
    while (tmp)
      {
	this->_sock_fd = MySocket::MySocketInit(tmp->ai_family,
						tmp->ai_socktype,
						tmp->ai_protocol);
	if (this->_sock_fd >= 0)
          {
	    if (MySocket::MyConnect(this->_sock_fd, tmp->ai_addr, tmp->ai_addrlen) == -1)
              {
		if (MySocket::MyClose(this->_sock_fd) == -1)
                  {
		    throw SocketException("Error on close");
		    return;
                  }
		throw SocketException("Error on connect");
		return;
              }
	    MySocket::MyFreeaddrinfo(this->_addrinfos.ai);
	    break;
          }
	else
	  tmp = tmp->ai_next;
      }
    if (tmp == NULL)
      {
	throw SocketException("Error : can't connect");
	return ;
      }

    //SET FDS
    FD_SET(this->_sock_fd, &this->_fds.read);
    this->_fds.nfds = this->_sock_fd;
  }

  virtual ~MySocket()
  {
    close(this->_sock_fd);
  }
public:
  static void	MyBzero(void *s, size_t n)
  {
    bzero(s, n);
  }

  static int	MyGetaddrinfos(const char *node, const char *service,
			       const struct addrinfo *hints,
			       struct addrinfo **res)
  {
    return (getaddrinfo(node, service, hints, res));
  }

  static int	MySocketInit(int domain, int type, int protocol)
  {
    return (socket(domain, type, protocol));
  }

  static int	MyConnect(int socket, const struct sockaddr *address,
			  socklen_t address_len)
  {
    return (connect(socket, address, address_len));
  }

  static int	MyClose(int fildes)
  {
    return (close(fildes));
  }

  static void	MyFreeaddrinfo(struct addrinfo *res)
  {
    freeaddrinfo(res);
  }

  static const char *MyGai_strerror(int errcode)
  {
    return (gai_strerror(errcode));
  }

  static int		MySelect(int nfds, fd_set *readfds,
				 fd_set *writefds, fd_set *errorfds,
                 struct timeval *timeout)
  {
    return (select(nfds, readfds, writefds, errorfds, timeout));
  }

  int		FullRead(int sock_fd)
  {
    int		ret;
    char    buff[1024];

    ret = read(sock_fd, buff, 1024);
    if (ret <= 0)
      return (ret);
    this->_buffer.write(buff, ret);
    return (ret);
  }
};

#endif /* __MY_SOCKET_H__ */

