//
// MyThread.hpp for MyThread in /home/lequeu_m/
//
// Made by martin lequeux-gruninger
// Login   <lequeu_m@epitech.net>
//
// Started on  Fri Apr 12 15:15:11 2013 martin lequeux-gruninger
// Last update Tue Apr 16 11:22:11 2013 martin lequeux-gruninger
//

#ifndef		__MY_THREAD_HPP__
# define	__MY_THREAD_HPP__

# include	<signal.h>
# include	<pthread.h>

template<typename R, typename T, typename P>
void	*cLauncher(void *);

template<typename R, typename T, typename P>
class	MyThread
{
public:
  enum eState
    {
      INIT = 0,
      RUNNING,
      WAITING,
      ENDED
    };

private:
  typedef R	(T::*mPTR)(P);

  mPTR		_method;
  T		*_mInstance;
  P		_param;
  eState	_state;
  pthread_t	_thread;
  MyThread(const MyThread &);
  MyThread&			operator=(const MyThread &);

public:
  MyThread(mPTR method, T *mInstance, P param)
    : _method(method), _mInstance(mInstance), _param(param), _state(INIT)
  {  }

  virtual ~MyThread(void)
  {
  }

  bool		run()
  {
    if (pthread_create(&this->_thread, NULL, &cLauncher<R, T, P>, (void *)this) == 0)
      {
	this->_state = RUNNING;
	return (true);
      }
    return (false);
  }

  bool		join(void **retval)
  {
    if (pthread_join(this->_thread, retval) == 0)
      return (true);
    return (false);
  }

  bool		detach()
  {
    if (pthread_detach(this->_thread) == 0)
      return (true);
    return (false);
  }

  bool		kill()
  {
    pthread_exit(NULL);
    return (true);
  }

  eState	getState() const
  {
    return (this->_state);
  }

  void		getState(eState state)
  {
    this->_state = state;
  }

  pthread_t	getID() const
  {
    return (this->_thread);
  }


  //LAUNCHER CPP
  void	cppLauncher()
  {
    (this->_mInstance->*_method)(this->_param);
  }
};


//LAUNCHER C
template<typename R, typename T, typename P>
void	*cLauncher(void *instance)
{
  reinterpret_cast<MyThread<R, T, P>*>(instance)->cppLauncher();
  return (NULL);
}

#endif		// __MY_THREAD_HPP__
