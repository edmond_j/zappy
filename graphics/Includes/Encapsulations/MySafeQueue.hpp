
//
// SafeQueue.hpp for irc in /home/lequeu_m/
//
// Made by martin lequeux-gruninger
// Login   <lequeu_m@epitech.net>
//
// Started on  Thu Apr 25 18:38:07 2013 martin lequeux-gruninger
// Last update Sun Apr 28 17:03:19 2013 martin lequeux-gruninger
//

#ifndef		__MYSAFEQUEUE_HPP_
# define	__MYSAFEQUEUE_HPP_

#include	<queue>
#include	"MyMutex.hpp"
#include	"MyScopelock.hpp"

template<typename T>
class MySafeQueue
{
private:
  std::queue<T>			_queue;
  MyMutex			_mutex;

  MySafeQueue(MySafeQueue const & other);
  MySafeQueue & operator=(MySafeQueue const & other);
public:
  MySafeQueue()
    : _queue(), _mutex() {}
  ~MySafeQueue() {}

public:
  void			push(T value)
  {
    MyScopelock	locker(this->_mutex);
    this->_queue.push(value);
  }

  T			pop()
  {
    MyScopelock	locker(this->_mutex);
    T		ret;

    ret = this->_queue.front();
    this->_queue.pop();
    return (ret);
  }

  size_t		size()
  {
    MyScopelock	(this->_mutex);
    return (this->_queue.size());
  }

  bool			empty()
  {
    MyScopelock	(this->_mutex);
    return (this->_queue.empty());
  }
};

#endif
