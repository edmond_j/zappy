//
// Model3DS.hh for zappy in /home/crouze_t//Rendu/Tek2/SystemUnix/zappy-2016-lequeu_m
//
// Made by thomas crouzet
// Login   <crouze_t@epitech.net>
//
// Started on  Tue Jul  2 15:50:42 2013 thomas crouzet
// Last update Thu Jul  4 13:57:45 2013 thomas crouzet
//

#ifndef HEADER_3DS
# define HEADER_3DS

# include <lib3ds/file.h>
# include <lib3ds/node.h>
# include <lib3ds/mesh.h>
# include <lib3ds/vector.h>
# include <lib3ds/matrix.h>
# include <lib3ds/material.h>
# include <lib3ds/light.h>
# include <vector>
# include <string>
# include <GL/glut.h>

class Model
{
private:
  bool			_lightEnabled;
  int			_curFrame;
  GLuint		_lightListIndex;
  Lib3dsFile		*_file;
  const char		*_filename;
  std::vector<GLuint>	_textureIndices;
  std::vector<std::string>	_textureFilenames;

  void			ApplyTexture(Lib3dsMesh *mesh);
  void			CreateLightList();
  void			RenderNode(Lib3dsNode *node);
public:
  Model();
  ~Model();
  void			LoadFile(const char *name);
  void			EnableLights();
  void			DisableLights();
  void			RenderModel();
  Lib3dsFile		*Get3DSPointer();
  std::string		GetFilename();
};

#endif
