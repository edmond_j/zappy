//
// AObject.hh for gdl in /home/lequeu_m/
//
// Made by martin lequeux-gruninger
// Login   <lequeu_m@epitech.net>
//
// Started on  Mon May  6 17:46:09 2013 martin lequeux-gruninger
// Last update Thu Jul  4 10:21:35 2013 martin lequeux-gruninger
//

#ifndef			VECTOR_HH_
# define		VECTOR_HH_

# include		<math.h>

struct Vector3f
{
  float				x;
  float				y;
  float				z;

  Vector3f(void);
  Vector3f(float x, float y, float z);
  Vector3f(const Vector3f& v);
  Vector3f(const Vector3f& from, const Vector3f& to);
  void				setValues(double, double, double);
  Vector3f&			operator=(const Vector3f& v);
  bool      		operator==(const Vector3f& v) const;
  bool              operator<(const Vector3f& v) const;
  bool              operator>(const Vector3f& v) const;
  Vector3f&			operator+=(const Vector3f& v);
  Vector3f			operator+(const Vector3f& v) const;
  Vector3f&			operator-=(const Vector3f& v);
  Vector3f			operator-(const Vector3f& v) const;
  Vector3f&			operator*=(const double a);
  Vector3f			operator*(const double a) const;
  Vector3f&			operator/=(const double a);
  Vector3f			operator/(const double a) const;
  Vector3f			crossProduct(const Vector3f& v) const;
  double			dotProduct(const Vector3f& v) const;
  double			length() const;
  int				distance(const Vector3f& v) const;
  Vector3f&			normalize();
};

#endif			/* !VECTOR_HH_ */
