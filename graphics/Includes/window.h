/*
** window.h for window in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Wed Jun 12 19:27:27 2013 martin lequeux-gruninger
** Last update Thu Jul  4 10:21:08 2013 martin lequeux-gruninger
*/

#ifndef WINDOW_H
#define WINDOW_H

#include "abstractscene.h"
#include "comm.h"
#include <QWindow>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QWheelEvent>

class QOpenGLContext;

class Window : public QWindow
{
  Q_OBJECT
    public:
  explicit Window(int, int, Comm *, QScreen *screen = 0);
  ~Window();

 signals:

  public slots:

  protected slots:
  void		resizeGl();
  void		paintGl();
  void		updateScene();

 private:
  QOpenGLContext		*mContext;
  QScopedPointer<AbstractScene>         mScene;

  void				initializeGl();

 protected:
  void		keyPressEvent(QKeyEvent * ev);
  void		keyReleaseEvent(QKeyEvent * ev);
  void		mouseMoveEvent(QMouseEvent * ev);
  void		mousePressEvent(QMouseEvent * ev);
  void		wheelEvent(QWheelEvent * ev);
};

#endif // WINDOW_H
