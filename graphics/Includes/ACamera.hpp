//
// ACamera.hpp for bomberman in /home/olivie_d//depotsvn/bomberman-2016-lequeu_m/Includes/Graphics
//
// Made by david olivier
// Login   <olivie_d@epitech.net>
//
// Started on  Tue May 28 15:00:22 2013 david olivier
// Last update Thu Jul  4 10:17:12 2013 martin lequeux-gruninger
//

#ifndef						ACAMERA_HPP_
# define					ACAMERA_HPP_

# include					<GL/gl.h>
# include					<GL/glu.h>
# include					<map>
# include					<stdlib.h>
# include					<QKeyEvent>
# include					<QMouseEvent>
# include					<QPoint>

# include					"Vector3f.hh"
# include					"window.h"


# define					PI	3.14159265

class						ACamera
{
public:
  ACamera(const Vector3f& position = Vector3f(0.0f, 0.0f, 900.0f))
    : _position(position), _rotation(0.0f, 0.0f, 0.0f)
  {
    //HANDLE VERTICAL / HORIZONTAL MOUSE MOVE
    this->_position = Vector3f(0.0f, 0.0f, 0.0f);
    this->_theta = -446.5f;
    this->_phi = -5.0f;
    this->_len = 1.0;

    //Compute target
    this->vectorsFromAngles();

    //CAM SETTINGS
    this->_speed = 300.0f;
    this->_sensivity = 0.3f;

    this->_prevMousePos.setX(-1);
    this->_prevMousePos.setY(-1);
    //METHODPTR
    this->_keyStates[Qt::Key_Up] = &ACamera::keyEventForward;
    this->_keyStates[Qt::Key_Down] = &ACamera::keyEventBack;
    this->_keyStates[Qt::Key_Left] = &ACamera::keyEventLeft;
    this->_keyStates[Qt::Key_Right] = &ACamera::keyEventRight;
    this->_keyStates[Qt::Key_PageUp] = &ACamera::keyEventUp;
    this->_keyStates[Qt::Key_PageDown] = &ACamera::keyEventDown;
  }

  ACamera(const ACamera& copy)
    : _position(copy._position), _rotation(copy._rotation), _target(copy._target),
      _forward(copy._forward), _left(copy._left), _theta(copy._theta), _phi(copy._phi),
      _speed(copy._speed), _sensivity(copy._sensivity)
  {
    this->_keyStates[Qt::Key_Up] = &ACamera::keyEventForward;
    this->_keyStates[Qt::Key_Down] = &ACamera::keyEventBack;
    this->_keyStates[Qt::Key_Left] = &ACamera::keyEventLeft;
    this->_keyStates[Qt::Key_Right] = &ACamera::keyEventRight;
    this->_keyStates[Qt::Key_PageUp] = &ACamera::keyEventUp;
    this->_keyStates[Qt::Key_PageDown] = &ACamera::keyEventDown;
  }

  virtual ~ACamera()
  {
  }

  virtual ACamera&				operator=(const ACamera& other)
  {
    if (this != &other)
      {
	this->_position = other._position;
	this->_rotation = other._rotation;
	this->_target = other._target;
	this->_forward = other._forward;
	this->_left = other._left;
	this->_theta = other._theta;
	this->_phi = other._phi;
	this->_speed = other._speed;
	this->_sensivity = other._sensivity;
	this->_keyStates[Qt::Key_Up] = &ACamera::keyEventForward;
	this->_keyStates[Qt::Key_Down] = &ACamera::keyEventBack;
	this->_keyStates[Qt::Key_Left] = &ACamera::keyEventLeft;
	this->_keyStates[Qt::Key_Right] = &ACamera::keyEventRight;
	this->_keyStates[Qt::Key_PageUp] = &ACamera::keyEventUp;
	this->_keyStates[Qt::Key_PageDown] = &ACamera::keyEventDown;
      }
    return (*this);
  }

  virtual void				initialize(double width, double height)
  {
    //CAM INIT (FOV, PROJECTION, DEPTH TEST)
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(70.0f, width / height, 0.01f, 1000.0f);
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
  }

  virtual void				updateKey(float elapsed, const std::vector<Qt::Key> & pressed)
  {
    //HANDLE INPUT

    for (std::vector<Qt::Key>::const_iterator it = pressed.begin();
	 it != pressed.end();
	 ++it)
      {
        for (std::map<Qt::Key, PtrFuncEventKey>::iterator it2 = this->_keyStates.begin();
             it2 != this->_keyStates.end();
             it2++)
	  {
            if ((*it) == it2->first)
	      (this->*_keyStates[it2->first])(elapsed, false); // CHANGE THIS BOOST SHIT
	  }

      }
    //COMPUTE TARGET
    this->_target = this->_position + this->_forward;
  }

  virtual void				updateMouse(QMouseEvent * ev)
  {
    //COMPUTE PHI/THETA
    this->mouseChange(ev);
  }


  virtual void				look()
  {
    //LOOK
    gluLookAt(this->_position.x, this->_position.y, this->_position.z,
	      this->_target.x, this->_target.y, this->_target.z,
	      0.0f, 1.0f, 0.0f);
  }

  virtual void				setSpeed(double speed)
  {
    this->_speed = speed;
  }

  virtual void				setSensivity(double sensivity)
  {
    this->_sensivity = sensivity;
  }

  virtual void				setPosition(const Vector3f& position)
  {
    this->_position = position;

    //RECOMPUTE TARGET
    this->_target = this->_position + this->_forward;
  }

  virtual void				vectorsFromAngles()
  {
    //FUCK GDL AXES
    static const Vector3f		up(0.0f, 1.0f, 0.0f);
    double			       	r_temp;

    //AVOID ERRORS
    if (this->_phi > 89.0f)
      this->_phi = 89.0f;
    else if (this->_phi < -89.0f)
      this->_phi = -89.0f;

    //COMPUTE FORWARD VECTOR
    r_temp = cos(this->_phi * (PI / 180.0f));
    this->_forward.y = sin(this->_phi * (PI / 180.0f));
    this->_forward.x = r_temp * cos(this->_theta * (PI / 180.0f));
    this->_forward.z = r_temp * sin(this->_theta * (PI / 180.0f));

    //NORMALIZE + CROSS PRODUCT
    this->_left = up.crossProduct(this->_forward);
    this->_left.normalize();

    //COMPUTE TARGET
    this->_target = this->_position + this->_forward;
  }

  virtual const Vector3f &		getTarget() const
  {
    return (this->_target);
  }

  virtual const Vector3f &		getPosition() const
  {
    return (this->_position);
  }

  virtual bool				isFps() const
  {
    return (false);
  }

  virtual double				getTheta() const
  {
    return (0.0f);
  }

  virtual void				resetPrevPos()
  {
    this->_prevMousePos.setX(-1);
    this->_prevMousePos.setY(-1);
  }

  virtual void				updateLen(float val)
  {
    if (this->_len + val > 0)
      this->_len += val;
  }

protected:
  typedef					void (ACamera::*PtrFuncEventKey)(float elapsed, bool boost);
  Vector3f				_position;
  Vector3f				_rotation;
  Vector3f				_target;
  Vector3f				_forward;
  Vector3f				_left;
  double				_theta;
  double				_phi;
  double				_speed;
  double				_len;
  double				_sensivity;
  QPoint				_prevMousePos;
  std::map<Qt::Key, PtrFuncEventKey>	_keyStates;

public:
  virtual void				update() = 0;
  virtual void				mouseChange(QMouseEvent * ev) = 0;
  virtual void				keyEventForward(float elapsed, bool boost) = 0;
  virtual void				keyEventBack(float elapsed, bool boost) = 0;
  virtual void				keyEventLeft(float elapsed, bool boost) = 0;
  virtual void				keyEventRight(float elapsed, bool boost) = 0;
  virtual void				keyEventUp(float elapsed, bool boost) = 0;
  virtual void				keyEventDown(float elapsed, bool boost) = 0;
};

#endif						/* !ACAMERA_HPP_ */
