/*
** splashscreen.h for zapppy in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Thu Jul  4 10:14:26 2013 martin lequeux-gruninger
** Last update Thu Jul  4 10:23:12 2013 martin lequeux-gruninger
*/

#ifndef SPLASHSCREEN_H
#define SPLASHSCREEN_H

# include       <QMainWindow>
# include       <QMessageBox>
# include       "comm.h"

namespace Ui
{
  class SplashScreen;
}

class SplashScreen : public QMainWindow
{
  Q_OBJECT

    public:
  explicit SplashScreen(QWidget *parent = 0);
  ~SplashScreen();

  private slots:
  void			on_connect_clicked();
  void			updateButton(const QString &);
  void			error(const QString &);
  void			on_debugSendButton_clicked();
  void			on_button3D_clicked();
  void          msz(int, int);

 private:
  Ui::SplashScreen	*ui;
  Comm              *_comm;

  void connectUser(const std::string &, const std::string &);
};

#endif // SPLASHSCREEN_H
