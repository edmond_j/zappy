/*
** comm.h for zappy in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Thu Jul  4 10:17:32 2013 martin lequeux-gruninger
** Last update Thu Jul  4 10:18:29 2013 martin lequeux-gruninger
*/

#ifndef COMM_H
# define COMM_H

# include	<QObject>
# include	<map>
# include	"MySocket.hpp"
# include	"MyThread.hpp"
# include	"MySafeQueue.hpp"

# define	CONNECT		"Connect"
# define	DISCONNECT	"Disconnect"
class Comm;

typedef void (Comm::*funcPtr)(const std::string &);

class Comm : public QObject, public MySocket
{
  Q_OBJECT
    private:
  MySafeQueue<std::string>		_commands;
  MyThread<int, Comm, void *>	_loopThread;
  bool                          _loopActive;
  std::map<std::string, funcPtr> _map;
  bool                          _ready;

  Comm(const Comm &);
  Comm			&operator=(const Comm &other);

  int				readHandler();
  int				reader(int);

  int				writer(int);
  int				writeHandler();

  int				eval(const std::string &);

 public:
  explicit Comm(const std::string & ip,
		const std::string & port,
		QObject *parent = 0);
  virtual ~Comm();

  void				launchLoop(void);
  int					serverLoop(void *);
  bool				getConnected() const;
  void				disconnect();
  void				addCommand(const std::string &);
  void              ready();

  ////
  void              BIENVENUE(const std::string &);
  void              msz(const std::string &);
  void              bct(const std::string &);
  void              tnaf(const std::string &);
  void              pnw(const std::string &);
  void              ppo(const std::string &);
  void              plv(const std::string &);
  void              pin(const std::string &);
  void              pex(const std::string &);
  void              pbc(const std::string &);
  void              pic(const std::string &);
  void              pie(const std::string &);
  void              pfk(const std::string &);
  void              pdr(const std::string &);
  void              pgt(const std::string &);
  void              pdi(const std::string &);
  void              enw(const std::string &);
  void              eht(const std::string &);
  void              ebo(const std::string &);
  void              edi(const std::string &);
  void              sgt(const std::string &);
  void              seg(const std::string &);
  void              smg(const std::string &);
  void              suc(const std::string &);
  void              sbp(const std::string &);


  ////




 signals:
  void				updateButton(const QString &);
  void				error(const QString &);

  // SERVER MESSAGES
  void              msz(int, int);
  void              bct(int, int, int, int, int, int, int, int, int);
  void              tna(const std::string &);
  void              pnw(int, int, int, int, int, const std::string &);
  void              ppo(int, int, int, int);
  void              plv(int, int);
  void              pin(int, int, int, int, int, int, int, int, int, int);
  void              pex(int);
  void              pbc(int,  const std::string &);
  void              pic(int, int, int, const std::vector<int> &);
  void              pie(int, int, int);
  void              pfk(int);
  void              pdr(int, int);
  void              pgt(int, int);
  void              pdi(int);
  void              enw(int, int, int, int);
  void              eht(int);
  void              ebo(int);
  void              edi(int);
  void              sgt(int);
  void              seg(int);
  void              smg(int);
  void              suc(void);
  void              sbp(void);
};


#endif // COMM_H
