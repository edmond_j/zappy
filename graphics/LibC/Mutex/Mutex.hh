//
// Mutex.hh for plazza in /home/grange_s//rendulocal/tek2/plazza/libc/Mutex
//
// Made by steve granger
// Login   <grange_s@epitech.net>
//
// Started on  Sun Apr  7 21:14:38 2013 steve granger
// Last update Fri Jun  7 17:45:24 2013 julien edmond
//

#ifndef			MUTEX_HH__
# define		MUTEX_HH__

# include		<pthread.h>

namespace		LibC
{
  class			Mutex
  {
    friend class	CondVar;

  private:
    pthread_mutex_t	_mutex;

  public:
    Mutex();
    Mutex(const Mutex&);
    Mutex& operator=(const Mutex&);
    ~Mutex();

    void			lock();
    void			unlock();
    bool			tryLock();
    const pthread_mutex_t	&getMutex();
  };

  class			ScopedLock
  {
  public:
    explicit ScopedLock(Mutex& m);
    ~ScopedLock();

  private:
    ScopedLock();
    ScopedLock(const ScopedLock&);
    ScopedLock&	operator=(const ScopedLock&);

    Mutex&	_mutex;
  };
}

#endif		// MUTEX_HH__
