//
// Mutex.cpp for plazza in /home/grange_s//rendulocal/tek2/plazza/libc/Mutex
//
// Made by steve granger
// Login   <grange_s@epitech.net>
//
// Started on  Sun Apr  7 21:23:16 2013 steve granger
// Last update Sun Jun  9 14:05:23 2013 julien edmond
//

#include	<iostream>
#include	<cerrno>
#include	"Mutex.hh"
#include	"CException/CException.hh"
#include	"Errno/Errno.hh"

namespace	LibC
{
  Mutex::Mutex()
  {
    int	ret;

    if ((ret = pthread_mutex_init(&this->_mutex, NULL)) != 0)
      throw CException("Mutex", ret);
  }

  Mutex::Mutex(const Mutex&)
  {
    int	ret;

    if ((ret = pthread_mutex_init(&this->_mutex, NULL)) != 0)
      throw CException("Mutex", ret);
  }

  Mutex&	Mutex::operator=(const Mutex&)
  {
    return (*this);
  }

  Mutex::~Mutex()
  {
    int ret;

    if ((ret = pthread_mutex_destroy(&this->_mutex)) != 0)
      std::cerr << "Mutex: pthread_mutex_destroy: "
		<< Errno::getStrError(ret) << std::endl;
  }

  void	Mutex::lock()
  {
    int ret;

    if ((ret = pthread_mutex_lock(&this->_mutex)) != 0)
      throw CException("Mutex lock", ret);
  }

  void	Mutex::unlock()
  {
    int ret;

    if ((ret = pthread_mutex_unlock(&this->_mutex)) != 0)
      throw CException("Mutex unlock", ret);
  }

  bool	Mutex::tryLock()
  {
    int ret;

    if ((ret = pthread_mutex_trylock(&this->_mutex)) == EBUSY)
      return (false);
    else if (ret != 0)
      throw CException("Mutex trylock", ret);
    return (true);
  }

  const pthread_mutex_t&	Mutex::getMutex()
  {
    return (this->_mutex);
  }

  ScopedLock::ScopedLock(Mutex& m)
    : _mutex(m)
  {
    _mutex.lock();
  }

  ScopedLock::~ScopedLock()
  {
    _mutex.unlock();
  }
}
