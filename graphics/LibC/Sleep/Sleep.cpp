//
// Sleep.cpp for LibC in /home/edmond_j//rendu/B4/C++/Plazza/rendu/LibC/Sleep
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Tue Apr  9 11:21:55 2013 julien edmond
// Last update Sat Apr 13 15:31:46 2013 julien edmond
//

#include	<unistd.h>
#include	"Sleep.hh"
#include	"CException/CException.hh"

namespace	LibC
{

  unsigned	Sleep::sleep(unsigned sec)
  {
    return (::sleep(sec));
  }

  unsigned	Sleep::msleep(unsigned msec)
  {
    return (::usleep(msec * 1000));
  }

  unsigned	Sleep::usleep(unsigned usec)
  {
    return (::usleep(usec));
  }

  unsigned		Sleep::nsleep(unsigned nsec)
  {
    struct timespec	t;

    t.tv_sec = 0;
    t.tv_nsec = nsec;
    if (::nanosleep(&t, &t))
      throw CException("Sleep: nsleep");
    return (t.tv_nsec);
  }

}
