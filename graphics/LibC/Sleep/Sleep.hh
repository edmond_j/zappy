//
// Sleep.hh for libc in /home/edmond_j//Usefull/Sleep
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sun Mar 31 12:23:18 2013 julien edmond
// Last update Tue Apr  9 11:29:04 2013 julien edmond
//

#ifndef		SLEEP_HH__
# define	SLEEP_HH__

namespace	LibC
{

  class			Sleep
  {
  public:
    static unsigned	sleep(unsigned sec);
    static unsigned	msleep(unsigned msec);
    static unsigned	usleep(unsigned usec);
    static unsigned	nsleep(unsigned nsec);

  private:
    Sleep();
    ~Sleep();
    Sleep(const Sleep &);
    Sleep&	operator=(const Sleep&);
  };

}

#endif		// SLEEP_HH__
