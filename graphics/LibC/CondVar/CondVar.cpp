//
// CondVar.cpp for LibC in /home/edmond_j//rendu/B4/C++/Plazza/rendu/LibC/CondVar
//
// Made by julien edmond
// Login   <edmond_j@epitech.net>
//
// Started on  Sun Apr  7 21:22:33 2013 julien edmond
// Last update Sat Jun  8 14:12:14 2013 julien edmond
//

#include	<iostream>
#include	"CondVar.hh"
#include	"CException/CException.hh"
#include	"Errno/Errno.hh"

namespace	LibC
{

  CondVar::CondVar(Mutex &m)
    : _mutex(m)
  {
    int	ret;

    if ((ret = pthread_cond_init(&_cond, NULL)))
      throw CException("CondVar: pthread_cond_init", ret);
  }

  CondVar::~CondVar()
  {
    int	ret;

    this->broadcast();
    if ((ret = pthread_cond_destroy(&_cond)))
      std::cerr << "CondVar: pthread_cond_destroy: "
		<< Errno::getStrError(ret) << std::endl;
  }

  void	CondVar::wait()
  {
    int	ret;

    if ((ret = pthread_cond_wait(&_cond, &_mutex._mutex)))
      throw CException("CondVar: wait", ret);
  }

  void	CondVar::signal()
  {
    int	ret;

    if ((ret = pthread_cond_signal(&_cond)))
      throw CException("CondVar: signal", ret);
  }

  void	CondVar::broadcast()
  {
    int	ret;

    if ((ret = pthread_cond_broadcast(&_cond)))
      throw CException("CondVar: broadcast", ret);
  }

  const pthread_cond_t	&CondVar::getCond() const
  {
    return (_cond);
  }

}
