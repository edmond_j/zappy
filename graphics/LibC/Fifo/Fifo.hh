//
// IOPipe.hh for LibC in /home/edmond_j//rendu/B4/C++/Plazza/rendu/LibC/IOPipe
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sun Apr  7 19:40:44 2013 julien edmond
// Last update Fri Apr 19 15:11:44 2013 julien edmond
//

#ifndef		FIFO_HH__
# define	FIFO_HH__

# include	<fstream>
# include	<string>
# include	"Process/Process.hh"

namespace	LibC
{
  class	oFifo;

  typedef oFifo&	(*fifoFunc)(oFifo&);

  class Fifo
  {
  public:
    explicit Fifo(const std::string& name);
    ~Fifo();

    const std::string&	getName() const;

    static oFifo&	endl(oFifo&);
    static oFifo&	flush(oFifo&);

  private:
    Fifo();
    Fifo(const Fifo&);
    Fifo&	operator=(const Fifo&);

    std::string		_name;
  };

  class iFifo : public std::ifstream
  {
  public:
    explicit iFifo(Fifo& fifo, const Process& proc);
    ~iFifo();

    template<typename T>
    iFifo&	operator>>(T& value);

  private:
    iFifo&	operator=(const iFifo&);
    const Process&	_proc;
  };

  class oFifo : public std::ofstream
  {
  public:

    explicit oFifo(Fifo& fifo, const Process& proc);
    ~oFifo();

    oFifo&	operator<<(fifoFunc func);
    template<typename T>
    oFifo&	operator<<(const T& value);
    oFifo&	flush();

  private:
    oFifo&	operator=(const oFifo&);
    const Process&	_proc;
  };

  template<typename T>
  iFifo&	iFifo::operator>>(T& value)
  {
    if (*this && _proc.isChildAlive())
      this->read(reinterpret_cast<char *>(&value), sizeof(T));
    return (*this);
  }

  template<typename T>
  oFifo&	oFifo::operator<<(const T& value)
  {
    if (*this && _proc.isChildAlive())
      this->write(reinterpret_cast<const char *>(&value), sizeof(T));
    return (*this);
  }

}

#endif		// FIFO_HH__
