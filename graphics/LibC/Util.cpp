//
// Util.cpp for Util in /home/taiebi_e//workdir/plazza/plazza-epitech-2016/taiebi_e
//
// Made by eddy taiebi
// Login   <taiebi_e@epitech.net>
//
// Started on  Thu Apr 18 03:49:28 2013 eddy taiebi
// Last update Mon Jun  3 21:09:11 2013 eddy taiebi
//

#include	"Util.hpp"

namespace	Util
{
  std::string&	replace(std::string& str, const std::string& s1, const std::string& s2)
  {
    size_t	pos;

    pos = str.find(s1);
    while (pos != std::string::npos)
      {
	str = str.replace(pos, s1.length(), s2);
	pos = str.find(s1);
      }
    return (str);
  }

  bool		isEqual(const std::string& s, const std::string& str)
  {
    size_t	pos;

    pos = str.find("|");
    if (pos == std::string::npos)
      return (s == str);
    if (s == str.substr(0, pos))
      return (true);
    return (isEqual(s, str.substr(pos + 1)));
  }

  std::string	getBetween(const std::string& str, const std::string& s1, const std::string& s2)
  {
    size_t	i;
    size_t	pos;

    i = str.find(s1);
    pos = str.find(s2);
    if (i == std::string::npos && pos == std::string::npos)
      return (str);
    else if (i == std::string::npos)
      return (str.substr(0, pos));
    else if (pos == std::string::npos)
      return (str.substr(i + s1.length()));
    return (str.substr(i + s1.length(), pos - (i + s1.length())));
  }

  std::string	getNext(std::string& str, const std::string& delim)
  {
    std::string	line;
    size_t	i;
    size_t	j;

    i = 0;
    while (i < str.length())
      {
	j = 0;
	while (j < delim.length())
	  {
	    if (delim[j] == str[i])
	      {
		line = str.substr(0, i);
		str = str.substr(i + 1);
		return (line);
	      }
	    ++j;
	  }
	++i;
      }
    line = str;
    str = "";
    return (line);
  }
};
