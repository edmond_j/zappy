//
// Random.hh for libc in /home/edmond_j//Usefull/Random
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sat Mar 30 14:16:27 2013 julien edmond
// Last update Sat Mar 30 14:32:52 2013 julien edmond
//

#ifndef		RANDOM_HH__
# define	RANDOM_HH__

namespace	LibC
{

  class		Random
  {
  public:
    static int	randVal();
    static int	randVal(unsigned max);
    static int	randVal(int min, int max);

  private:

    static bool	isInit;
    static void	initialize();

    Random();
    Random(const Random&);
    ~Random();
    Random	&operator=(const Random&);
  };

}

#endif		// RANDOM_HH__
