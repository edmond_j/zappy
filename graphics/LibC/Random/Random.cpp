//
// Random.cpp for libc in /home/edmond_j//Usefull/Random
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sat Mar 30 14:25:32 2013 julien edmond
// Last update Sat Apr 13 14:30:37 2013 julien edmond
//

#include	<cstdlib>
#include	"Random.hh"
#include	"Timeval/Timeval.hh"

namespace	LibC
{

  int	Random::randVal()
  {
    if (!isInit)
      initialize();
    return (rand());
  }

  int	Random::randVal(unsigned max)
  {
    if (!isInit)
      initialize();
    return (rand() % max);
  }

  int	Random::randVal(int min, int max)
  {
    if (!isInit)
      initialize();
    return (rand() % (max - min) + min);
  }

  bool	Random::isInit = false;

  void	Random::initialize()
  {
    srand(Timeval().getUSec());
  }

}
