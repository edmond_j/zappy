//
// Thread.hpp for libc in /home/edmond_j//Usefull/C++/LibC/Thread
//
// Made by julien edmond
// Login   <edmond_j@epitech.net>
//
// Started on  Fri May 31 12:31:01 2013 julien edmond
// Last update Wed Jun  5 17:05:29 2013 julien edmond
//

#ifndef		THREAD_HPP__
# define	THREAD_HPP__

# include	<pthread.h>
# include	<signal.h>
# include	<cerrno>
# include	"CException/CException.hh"

//
//  Statement
//

namespace	LibC
{
  class	Thread
  {
  public:

    Thread();
    Thread(const Thread& t);
    Thread&	operator=(const Thread&);
    ~Thread();

  };

  template<typename Ret, typename Param>
  void	*starter(void *thread);

  template<typename Ret, typename Param, typename Obj>
  void	*starter(void *thread);
}

//
//  Implementation
//

// namespace LibC
// {
//   template<typename Ret, typename Param, typename Obj>
//   Thread<Ret, Param, Obj>::Thread()
//     : _startMthd(0), _obj(0), _start(0), _thread(-1)
//   {}

//   template<typename Ret, typename Param, typename Obj>
//   Thread<Ret, Param, Obj>::Thread(Ret (Obj::*start)(Param), Obj* obj,
// 				  Param param, Action act)
//     : _param(param), _startMthd(start), _obj(obj),
//       _start(static_cast<StartRoutine>(&starter)), _thread(-1)
//   {}

//   template<typename Ret, typename Param, typename Obj>
//   Thread<Ret, Param, Obj>::Thread(const Thread& t)
//     : _param(t._param), _startMthd(t._startMthd), _obj(t._obj),
//       _start(t._start), _thread(-1)
//   {}

//   template<typename Ret, typename Param, typename Obj>
//   Thread<Ret, Param, Obj>&
//   Thread<Ret, Param, Obj>::operator=(const Thread& t)
//   {
//     if (this != &t)
//       {
// 	this->_param = t._param;
// 	this->_startMthd = t._startMthd;
// 	this->_obj = t._obj;
// 	this->_start = t._start;
// 	this->destroy();
//       }
//     return (*this);
//   }

//   template<typename Ret, typename Param, typename Obj>
//   Thread<Ret, Param, Obj>::~Thread()
//   {
//     this->destroy();
//   }

//   template<typename Ret, typename Param, typename Obj>
//   void	Thread<Ret, Param, Obj>::setAct(Action act)
//   {
//     this->_act = act;
//   }

//   template<typename Ret, typename Param, typename Obj>
//   void	Thread<Ret, Param, Obj>::setParam(const Param& param)
//   {
//     this->_param = param;
//   }

//   template<typename Ret, typename Param, typename Obj>
//   void Thread<Ret, Param, Obj>::setObj(Obj* obj)
//   {
//     this->_obj = obj;
//   }

//   template<typename Ret, typename Param, typename Obj>
//   void Thread<Ret, Param, Obj>::setStart(Ret (Obj::*start)(Param))
//   {
//     this->_startMthd = start;
//   }

//   template<typename Ret, typename Param, typename Obj>
//   void Thread<Ret, Param, Obj>::setStart(StartRoutine start)
//   {
//     this->_start = start;
//     this->_startMthd = 0;
//   }

//   template<typename Ret, typename Param, typename Obj>
//   typename Thread<Ret, Param, Obj>::Action
//   Thread<Ret, Param, Obj>::getAct() const
//   {
//     return (this->_act);
//   }

//   template<typename Ret, typename Param, typename Obj>
//   const Param& Thread<Ret, Param, Obj>::getParam() const
//   {
//     return (this->_param);
//   }

//   template<typename Ret, typename Param, typename Obj>
//   const Obj*		Thread<Ret, Param, Obj>::getObj() const
//   {
//     return (this->_obj);
//   }

//   template<typename Ret, typename Param, typename Obj>
//   typename Thread<Ret, Param, Obj>::StartRoutine
//   Thread<Ret, Param, Obj>::getStart() const
//   {
//     return (this->_start);
//   }

//   template<typename Ret, typename Param, typename Obj>
//   bool	Thread<Ret, Param, Obj>::launch()
//   {
//     int	err;

//     if (this->_thread != -1)
//       {
// 	if (this->_startMthd)
// 	  err = pthread_create(&this->_thread, 0,
// 			       static_cast<void *(*)(void *)>(this->_start),
// 			       static_cast<void *>(this));
// 	else
// 	  err = pthread_create(&this->_thread, 0,
// 			       static_cast<void *(*)(void *)>(this->_start),
// 			       static_cast<void *>(this->_param));
// 	if (err)
// 	  {
// 	    this->_thread = -1;
// 	    throw CException("Thread::launch", err);
// 	  }
//       }
//     return (this->_thread != -1);
//   }

//   template<typename Ret, typename Param, typename Obj>
//   bool	Thread<Ret, Param, Obj>::kill()
//   {
//     int	err;

//     if (this->_thread != -1 && (err = pthread_cancel(this->_thread)))
//       throw CException("Thread::kill", err);
//     this->_thread = -1;
//     return (this->_thread == -1);
//   }

//   template<typename Ret, typename Param, typename Obj>
//   bool	Thread<Ret, Param, Obj>::join()
//   {
//     int	err;

//     if (this->_thread != -1 && (err = pthread_join(this->_thread, NULL)))
//       throw CException("Thread::join", err);
//     this->_thread = -1;
//     return (this->_thread == -1);
//   }

//   template<typename Ret, typename Param, typename Obj>
//   bool	Thread<Ret, Param, Obj>::join(Ret& ret)
//   {
//     int	err;

//     if (this->_thread != -1 && (err = pthread_join(this->_thread,
// 						   &ret)))
//       throw CException("Thread::join", err);
//     this->_thread = -1;
//     return (this->_thread == -1);
//   }

//   template<typename Ret, typename Param, typename Obj>
//   bool	Thread<Ret, Param, Obj>::tryJoin()
//   {
//     int	err(0);

//     if (this->_thread != -1 && (err = pthread_tryjoin_np(this->_thread, 0))
// 	&& err != EBUSY)
//       throw CException("Thread::tryJoin", err);
//     else if (err && err != EBUSY)
//       this->_thread = -1;
//     return (this->_thread == -1);
//   }

//   template<typename Ret, typename Param, typename Obj>
//   bool		Thread<Ret, Param, Obj>::tryJoin(Ret& ret)
//   {
//     int	err(0);

//     if (this->_thread != -1 && (err = pthread_tryjoin_np(this->_thread, &ret))
// 	&& err != EBUSY)
//       throw CException("Thread::tryJoin", err);
//     else if (err && err != EBUSY)
//       this->_thread = -1;
//     return (this->_thread == -1);
//   }

//   template<typename Ret, typename Param, typename Obj>
//   void		Thread<Ret, Param, Obj>::destroy()
//   {
//     if (this->_act == Kill)
//       this->kill();
//     else if (this->_act == Join)
//       this->join();
//     else
//       this->_thread = -1;
//   }

//   template<typename Ret, typename Param, typename Obj>
//   Ret	Thread<Ret, Param, Obj>::starter(Thread* t)
//   {
//     return ((t->_obj->*t->_startMthd)(t->_param));
//   }

// }

#endif		// THREAD_HPP__
