//
// Process.cpp for LibC in /home/edmond_j//rendu/B4/C++/Plazza/rendu/LibC/Process
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sun Apr  7 19:26:41 2013 julien edmond
// Last update Thu Apr 18 23:11:36 2013 julien edmond
//

#include	<cstdlib>
#include	<iostream>
#include	<sys/wait.h>
#include	<unistd.h>
#include	<csignal>
#include	"Process.hh"
#include	"CException/CException.hh"
#include	"Errno/Errno.hh"

int	LibC::Process::sigKiller = SIGTERM;

LibC::Process::Process(Action a)
  : _action(a), _pid(fork())
{
  if (_pid == -1)
    throw CException("Process: fork");
}

LibC::Process::~Process()
{
  if (this->isParent())
    {
      if (_action == Kill)
	{
	  if (kill(_pid, sigKiller) == -1)
	    Errno::printError("Process: kill");
	}
      else if (_action == Wait && isChildAlive())
	{
	  if (waitpid(_pid, NULL, 0) == -1)
	    Errno::printError("Process: waitpid");
	}
    }
}

bool	LibC::Process::isChild() const
{
  return (!_pid);
}

bool	LibC::Process::isParent() const
{
  return (_pid);
}

bool	LibC::Process::isChildAlive() const
{
  bool	ret;

  ret = isChild() || !waitpid(_pid, NULL, WNOHANG);
  return (ret);
}

void	LibC::Process::setSigKiller(int sigNb)
{
  sigKiller = sigNb;
}

pid_t	LibC::Process::getPid()
{
  return (getpid());
}

void	LibC::Process::exit(int status)
{
  ::exit(status);
}
