//
// Process.hh for LibC in /home/edmond_j//rendu/B4/C++/Plazza/rendu/LibC/Process
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sun Apr  7 19:17:30 2013 julien edmond
// Last update Thu Apr 18 23:10:48 2013 julien edmond
//

#ifndef		PROCESS_HH__
# define	PROCESS_HH__

# include	<unistd.h>

namespace	LibC
{

  class	Process
  {
  public:
    enum Action
      {
	Kill,
	Wait,
	Leave
      };

    explicit Process(Action a = Leave);
    ~Process();

    bool	isChild() const;
    bool	isParent() const;
    bool	isChildAlive() const;

    static void		setSigKiller(int sigNum);
    static pid_t	getPid();
    static void		exit(int status);

  private:

    static int	sigKiller;

    Process(const Process&);
    Process&	operator=(const Process&);

    Action	_action;
    pid_t	_pid;
  };

}

#endif		// PROCESS_HH__
