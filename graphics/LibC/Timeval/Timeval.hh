//
// Timeval.hh for Nibbler in /home/edmond_j//rendu/B4/C++/Nibbler/nibbler_snake_eater/libs/ncurses
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sat Mar 16 14:06:43 2013 julien edmond
// Last update Thu Apr 18 23:34:24 2013 julien edmond
//

#ifndef		TIMEVAL_HH__
# define	TIMEVAL_HH__

# include	<string>
# include	<stdexcept>
# include	<sys/time.h>

namespace LibC
{

  class		Timeval
  {
  public:
    class	Exception: public std::runtime_error
    {
    public:
      explicit Exception(const std::string& func, int errcode) throw();

      virtual ~Exception() throw();
    };

  public:
    Timeval();
    Timeval(const Timeval &);
    Timeval(double time);
    Timeval	&operator=(const Timeval &);
    ~Timeval();

    operator	double() const;

    time_t	getSec() const;
    suseconds_t	getUSec() const;

    void		now();

    Timeval	&operator+=(double time);
    Timeval	operator+(double time) const;

    Timeval	&operator-=(double time);
    Timeval	operator-(double time) const;

    bool		operator==(const Timeval &time) const;
    bool		operator!=(const Timeval &time) const;

    static const char*	strTime();

  private:
    struct timeval	_tv;
  };

}

#endif		// TIMEVAL_HH__
