//
// Timeval.cpp for Nibbler in /home/edmond_j//rendu/B4/C++/Nibbler/nibbler_snake_eater/libs/ncurses
// 
// Made by julien edmond
// Login   <edmond_j@epitech.net>
// 
// Started on  Sat Mar 16 14:15:57 2013 julien edmond
// Last update Thu Apr 18 23:57:16 2013 julien edmond
//

#include	<cerrno>
#include	<cstring>
#include	"Timeval.hh"
#include	"CException/CException.hh"

namespace LibC
{

  Timeval::Timeval()
  {
    this->now();
  }

  Timeval::Timeval(const Timeval &t)
    : _tv(t._tv)
  {}

  Timeval::Timeval(double time)
  {
    this->_tv.tv_sec = (int)time;
    this->_tv.tv_usec = (time - (int)time) * 1000000.0;
  }

  Timeval	&Timeval::operator=(const Timeval &t)
  {
    if (this != &t)
      this->_tv = t._tv;
    return (*this);
  }

  Timeval::~Timeval()
  {}

  Timeval::operator	double() const
  {
    return ((double)this->_tv.tv_sec + (double)this->_tv.tv_usec / 1000000.0);
  }

  time_t	Timeval::getSec() const
  {
    return (this->_tv.tv_sec);
  }

  suseconds_t	Timeval::getUSec() const
  {
    return (this->_tv.tv_usec);
  }

  void	Timeval::now()
  {
    if (gettimeofday(&this->_tv, NULL) == -1)
      throw CException("Timeval: gettimeofday");
  }

  Timeval	&Timeval::operator+=(double time)
  {
    this->_tv.tv_sec += (int)time;
    this->_tv.tv_usec += (double)(time - (int)time) * 1000000;
    this->_tv.tv_sec += this->_tv.tv_usec / 1000000;
    this->_tv.tv_usec %= 1000000;
    return (*this);
  }

  Timeval	Timeval::operator+(double time) const
  {
    return (Timeval((double)*this + time));
  }

  Timeval	&Timeval::operator-=(double time)
  {
    double	tmp;

    tmp = (double)*this - time;
    this->_tv.tv_sec = (int)tmp;
    this->_tv.tv_usec = (double)(tmp - (int)tmp) * 1000000;
    return (*this);
  }

  Timeval	Timeval::operator-(double time) const
  {
    return (Timeval((double)*this - time));
  }

  bool	Timeval::operator==(const Timeval &time) const
  {
    return (this->_tv.tv_sec == time._tv.tv_sec
	    && this->_tv.tv_usec == time._tv.tv_usec);
  }

  bool	Timeval::operator!=(const Timeval &t) const
  {
    return (!(*this == t));
  }

  const char*	Timeval::strTime()
  {
    time_t	stamp(time(NULL));
    char	*sent;

    sent = ctime(&stamp);
    sent[strlen(sent)] = 0;
    return (sent);
  }

}
