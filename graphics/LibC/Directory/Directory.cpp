//
// Directory.cpp for LibC in /home/edmond_j//rendu/B4/C++/Bomberman/bomberman-2016-lequeu_m/LibC/Directory
//
// Made by julien edmond
// Login   <edmond_j@epitech.net>
//
// Started on  Fri Jun  7 11:46:35 2013 julien edmond
// Last update Fri Jun  7 11:55:26 2013 julien edmond
//

#include	<sys/types.h>
#include	<dirent.h>
#include	"Directory/Directory.hh"
#include	"CException/CException.hh"

namespace	LibC
{

  Directory::Directory()
  {}

  Directory::Directory(const char *dir_name)
  {
    this->loadDir(dir_name);
  }

  Directory::Directory(const Directory& d)
    : std::map<std::string, Stat>(d)
  {}

  Directory&	Directory::operator=(const Directory& d)
  {
    if (this != &d)
      this->std::map<std::string, Stat>::operator=(d);
    return (*this);
  }

  Directory::~Directory()
  {}

  void			Directory::loadDir(const char *dir_name)
  {
    DIR*		dir;
    struct dirent*	ent;

    this->clear();
    if (!(dir = opendir(dir_name)))
      throw CException("Directory::loadDir");
    while ((ent = readdir(dir)))
      (*this)[ent->d_name]
	= Stat((std::string(dir_name) + '/' + ent->d_name).c_str());;
  }

}
