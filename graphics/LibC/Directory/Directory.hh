//
// Directory.hh for LibC in /home/edmond_j//rendu/B4/C++/Bomberman/bomberman-2016-lequeu_m/LibC/Directory
//
// Made by julien edmond
// Login   <edmond_j@epitech.net>
//
// Started on  Fri Jun  7 11:41:31 2013 julien edmond
// Last update Fri Jun  7 11:50:09 2013 julien edmond
//

#ifndef		DIRECTORY_HH__
# define	DIRECTORY_HH__

# include	<map>
# include	<string>
# include	"Stat/Stat.hh"

namespace	LibC
{

  class		Directory : public std::map<std::string, Stat>
  {
  public:
    Directory();
    Directory(const char *dir_name);
    Directory(const Directory&);
    Directory&	operator=(const Directory&);
    ~Directory();

    void	loadDir(const char *dir_name);
  };

}

#endif		// DIRECTORY_HH__
